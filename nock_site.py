from twisted.application import internet, service
from twisted.web import server, wsgi
from twisted.internet import reactor
from twisted.python.logfile import DailyLogFile
from twisted.python.log import ILogObserver, FileLogObserver
import django

PORT = 8000

django.setup()
from config.wsgi import application as djapp

# Twisted Application Framework setup:
application = service.Application('twisted-django')

root = wsgi.WSGIResource(reactor, reactor.getThreadPool(), djapp)
logfile = DailyLogFile("gui.log", "/logs")
application.setComponent(ILogObserver, FileLogObserver(logfile).emit)
# Serve it up:
main_site = server.Site(root)
internet.TCPServer(PORT, main_site).setServiceParent(application)
reactor.suggestThreadPoolSize(100)
