[![pipeline status](https://gitlab.com/reloded/nock/badges/test-ci-cd/pipeline.svg)](https://gitlab.com/reloded/nock/commits/test-ci-cd)
[![coverage report](https://gitlab.com/reloded/nock/badges/test-ci-cd/coverage.svg)](https://gitlab.com/reloded/nock/commits/test-ci-cd)
## Usage
API docs can be accessed on [NOCC API Docs](https://nocc-staging.herokuapp.com/api/v1/docs/).
## Admin Site
Admin site can be accessed [here](https://nocc-staging.herokuapp.com/admin/)
- username: gasyetu
- password: gasyetu2018