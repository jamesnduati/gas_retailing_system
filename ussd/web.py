from twisted.web import server, resource
from twisted.internet import reactor#, threads, defer
from ussd import server_handler
from ussd.location import UserLocation

#### WEB ###
class TestService(resource.Resource):
    def __init__(self):
        resource.Resource.__init__(self)
        #self.putChild('vendor', VendorHandler())
    def getChild(self, path, request):
        """ enables dynamic url eg /sms/46 /sms/50 etc """
        return self

    def render_GET(self, request):
        #print request.__dict__
        return "{'resp':'api test'}"
    def render_POST(self, request):
        #print request.__dict__
        return "{'resp':'api test'}"

class Factory(resource.Resource):
    def __init__(self):
       resource.Resource.__init__(self)
       self.putChild(b'', TestService())# for monit
       self.putChild(b'ussd', server_handler.ConsumerService())
       self.putChild(b'retailer', server_handler.RetailerService())
       reactor.callLater(1, UserLocation().set_locations)
