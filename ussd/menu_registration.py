import json
import logging
from datetime import datetime

import redis
import requests
from twisted.internet import reactor

from django.core import exceptions
from django.contrib.auth.password_validation import validate_password
from apps.accounts.models import User
from apps.consumers.models import Consumer
from apps.core.models import (Region,SubRegion,Location,Village)
from ussd import conf
from ussd.redis_wrapper import RedisWrapper


class registration_home():
    def __init__(self):
        pass
    def processAction(self, resources):
        req=resources['parameters']['request'].lower()
        if req == '1':
            resources['currentMenu']='registration_first_name'
            return resources        
        return resources
    def start(self, resources):        
        resources['parameters']['response']='Welcome to Gas Yetu program\n\n1.Register'
        resources['hasResponse'] = False            
        return resources
    

class registration_id():
    def __init__(self):
        pass
    def processAction(self, resources):
        req=resources['parameters']['request'].lower()
        if len(req) <6 or not req.isdigit():
            resources['parameters']['response'] = 'Invalid input\n Enter National id.'

            resources['hasResponse'] = True
            return resources
        resources['parameters']['national_id']=req
        resources['currentMenu']='registration_region'
        return resources
    def start(self, resources):
        if not resources['hasResponse']:
           resources['parameters']['response']='Reply with your national Id'
        resources['hasResponse'] = False
        return resources

class registration_first_name():
    def __init__(self):
        pass
    def processAction(self, resources):
        req = resources['parameters']['request']
        if not req:
           return resources
        resources['parameters']['first_name']=req
        resources['currentMenu']='registration_last_name'
        return resources
    def start(self, resources):
        if not resources['hasResponse']:
            resources['parameters']['response']='Enter your First Name'
        resources['hasResponse']=False
        return resources

class registration_last_name():
    def __init__(self):
        pass
    def processAction(self, resources):
        req = resources['parameters']['request']
        if not req:
           return resources
        resources['parameters']['last_name']=req
        resources['currentMenu']='registration_id'
        return resources
    def start(self, resources):
        if not resources['hasResponse']:
            resources['parameters']['response']='Enter your Last Name'
        resources['hasResponse']=False
        return resources

class registration_region():
    def __init__(self):
        #self.regions = RedisWrapper().redis.hgetall(f'{conf.LOCATION_CACHE_PREFIX}')
        ## the below code overides the county list
        ## to just Nakuru for the pilot
        ###############

        self.regions = {'32'.encode('utf8'):'Nakuru'.encode('utf8')}
        ## end
        ################

    def processAction(self, resources):
        req = resources['parameters']['request']
        if not req:
           return resources
        if req == '98':
            return resources
        if req == '90':
            resources['limit'] -= conf.PAGE_SIZE*2
            if resources['limit']<0:
                resources['limit']=0
            return resources

        if not req.encode('utf8') in self.regions:
            return resources
        
        resources['parameters']['region'] = req
        resources['currentMenu']='registration_subregion'  
        resources['limit']=0     
        return resources
    def start(self, resources):
        if not 'limit' in resources:
            resources['limit']=0
        text = 'Select county:\n\n'
        print(f'regions::{self.regions}')
        for regionid in list(self.regions.keys())[resources['limit']:resources['limit']+conf.PAGE_SIZE]:            
            text = f'{text}{regionid.decode("utf8")}.{self.regions[regionid].decode("utf8")}\n'
        resources['limit'] += conf.PAGE_SIZE
        more_text = '98. next\n'
        if resources['limit'] >= len(self.regions):
            more_text = '90. previous'
            
            resources['limit'] -= conf.PAGE_SIZE
            if resources['limit']<0:
                resources['limit']=0
        text = f'{text}{more_text}\n#.Home'        
        resources['parameters']['response']= text
        return resources

class registration_subregion():
    def __init__(self):
        pass
        
    def processAction(self, resources):
        req = resources['parameters']['request']
        if not req:
           return resources
        if req=='00':
            resources['currentMenu']='registration_region' 
            return resources
        if req == '98':
            return resources
        if req == '90':
            resources['limit'] -= conf.PAGE_SIZE*2
            if resources['limit']<0:
                resources['limit']=0
            return resources

        region = resources['parameters']['region']
        subregions = RedisWrapper().redis.hgetall(f'{conf.LOCATION_CACHE_PREFIX}:{region}')
        if not req.encode('utf8') in subregions:
            return resources
        resources['parameters']['subregion'] = req
        
        # check if there is a location and a vilage for this sub-region
        if not RedisWrapper().redis.hgetall(f'{conf.LOCATION_CACHE_PREFIX}:{region}:{req}'):
           resources['parameters']['location'] = ''
           resources['parameters']['village'] = ''
           resources['currentMenu']='registration_pin'  
           return resources
            
        resources['currentMenu']='registration_location'  
        resources['limit']=0     
        return resources
    def start(self, resources):         
        if not 'limit' in resources:
            resources['limit']=0
        text = 'select constituency:\n\n'       
        region = resources['parameters']['region']
        subregions = RedisWrapper().redis.hgetall(f'{conf.LOCATION_CACHE_PREFIX}:{region}')
        for regionid in list(subregions.keys())[resources['limit']:resources['limit']+conf.PAGE_SIZE]:            
            text = f'{text}{regionid.decode("utf8")}.{subregions[regionid].decode("utf8")}\n'
        resources['limit'] += conf.PAGE_SIZE
        more_text = '98. next\n'
        if resources['limit'] >= len(subregions):
            more_text = '90. previous'
            
            resources['limit'] -= conf.PAGE_SIZE
            if resources['limit']<0:
                resources['limit']=0
        text = f'{text}{more_text}\n00.back\n#.Home'        
        resources['parameters']['response']= text
        return resources

class registration_location():
    def __init__(self):
        pass
        
    def processAction(self, resources):
        req = resources['parameters']['request']
        if not req:
           return resources
        if req=='00':
            resources['currentMenu']='registration_region' 
            return resources
        if req == '98':
            return resources
        if req == '90':
            resources['limit'] -= conf.PAGE_SIZE*2
            if resources['limit']<0:
                resources['limit']=0
            return resources

        region = resources['parameters']['region']
        subregion = resources['parameters']['subregion']
        locations = RedisWrapper().redis.hgetall(f'{conf.LOCATION_CACHE_PREFIX}:{region}:{subregion}')
        if not req.encode('utf8') in locations:
            return resources
        
        
        resources['parameters']['location'] = req
         # check if there is a village for this location
        if not RedisWrapper().redis.hgetall(f'{conf.LOCATION_CACHE_PREFIX}:{region}:{subregion}:{req}'):           
           resources['parameters']['village'] = ''
           resources['currentMenu']='registration_pin'  
           return resources
        resources['currentMenu']='registration_village'  
        resources['limit']=0
        return resources
    def start(self, resources):
        if not 'limit' in resources:
            resources['limit']=0
        text = 'Select location:\n\n'
        region = resources['parameters']['region']
        subregion = resources['parameters']['subregion']
        locations = RedisWrapper().redis.hgetall(f'{conf.LOCATION_CACHE_PREFIX}:{region}:{subregion}')
        for regionid in list(locations.keys())[resources['limit']:resources['limit']+conf.PAGE_SIZE]:            
            text = f'{text}{regionid.decode("utf8")}.{locations[regionid].decode("utf8")}\n'
        resources['limit'] += conf.PAGE_SIZE
        more_text = '98. next\n'
        if resources['limit'] >= len(locations):
            more_text = '90. previous'
            resources['limit'] -= conf.PAGE_SIZE
            if resources['limit']<0:
                resources['limit']=0
        text = f'{text}{more_text}\n00.back\n#.Home'
        resources['parameters']['response']= text

        return resources

class registration_village():
    def __init__(self):
        pass
 
    def processAction(self, resources):
        req = resources['parameters']['request']
        if not req:
           return resources
        if req=='00':
            resources['currentMenu']='registration_region' 
            return resources
        if req == '98':
            return resources
        if req == '90':
            resources['limit'] -= conf.PAGE_SIZE*2
            if resources['limit']<0:
                resources['limit']=0
            return resources

        region = resources['parameters']['region']
        subregion = resources['parameters']['subregion']
        location = resources['parameters']['location']
        locations = RedisWrapper().redis.hgetall(f'{conf.LOCATION_CACHE_PREFIX}:{region}:{subregion}:{location}')
        if not req.encode('utf8') in locations:
            return resources

        resources['parameters']['village'] = req
        resources['currentMenu']='registration_pin'
        resources['limit']=0
        return resources
    def start(self, resources):
        if not 'limit' in resources:
            resources['limit']=0
        text = 'Select sub location:\n\n'
        region = resources['parameters']['region']
        subregion = resources['parameters']['subregion']
        location = resources['parameters']['location']
        locations = RedisWrapper().redis.hgetall(f'{conf.LOCATION_CACHE_PREFIX}:{region}:{subregion}:{location}')
        for regionid in list(locations.keys())[resources['limit']:resources['limit']+conf.PAGE_SIZE]:
            text = f'{text}{regionid.decode("utf8")}.{locations[regionid].decode("utf8")}\n'
        resources['limit'] += conf.PAGE_SIZE
        more_text = '98. next\n'
        if resources['limit'] >= len(locations):
            more_text = '90. previous'

            resources['limit'] -= conf.PAGE_SIZE
            if resources['limit']<0:
                resources['limit']=0
        text = f'{text}{more_text}\n00.back\n#.Home'
        resources['parameters']['response']= text
        return resources


class registration_gas_yetu():
    def __init__(self):
        pass
    def processAction(self, resources):
        req = resources['parameters']['request']
        if not req:
           return resources
        if 'intent' in resources:
            if resources['intent'] == 'jp_wallet_pin':
                resources['parameters']['reset_pin']=False
                if req =='1':
                    resources['parameters']['reset_pin']=True
        else:
           resources['parameters']['gas_yetu']=0
           if req == '2':
               resources['parameters']['gas_yetu'] = 1
        reactor.callInThread(self.user_registration, resources)

        #ret=self.user_registration(resources)
        resources['currentMenu']='wait'
        resources['action']='end'
        resources['parameters']['response']=''
        resources['parameters']['response']=conf.MSG_REGISTRATION_SUPA_GAS
        if resources['parameters']['gas_yetu'] == 1:
           resources['parameters']['response']=conf.MSG_REGISTARTION_GAS_OFFER
        resources['hasResponse']=True
        return resources

    def start(self, resources):
        if not resources['hasResponse']:
            resources['parameters']['response']= "Do you have Gas?\n1.Yes\n2.No"
        resources['hasResponse']=False
        return resources

    def user_registration(self, resources):
        ret = 1
        try:
            phone = resources['parameters']['phone']
            first_name = resources['parameters']['first_name']
            last_name = resources['parameters']['last_name']
            national_id = resources['parameters']['national_id']
            last_name = resources['parameters']['last_name']
            region = resources['parameters']['region']
            if region:
                region=Region.objects.get(code=region).id
            subregion = resources['parameters']['subregion']
            if subregion:
                subregion=SubRegion.objects.get(region__id=region,code=subregion).id
            location = resources['parameters']['location']
            if location:
                location=Location.objects.get(subregion__id=subregion,code=location).id
            village = resources['parameters']['village']
            if village:
                village=Village.objects.get(location__id=location,code=village).id
            pin = resources['parameters']['pin']
            request={
                "username": phone,
                "first_name": first_name,
                "last_name": last_name,
                "email": f"{phone}@jambopay.com",
                "phone": f'+{phone}',
                "password": pin,
                "region": region,
                "sub_region": subregion,
                "location": location,
                "village": village,
                "identity_number":national_id,
                "reset_pin":'',
                "from_enumerator":False,
                "survey_requested":False,
                "ussd":True
            }
            if 'reset_pin' in resources['parameters']:
                request['reset_pin']=resources['parameters']['reset_pin']
            if resources['parameters']['gas_yetu'] == 1:
                request["survey_requested"]=True

            r =requests.post(f"{conf.API_BASE_URL}/account/register/",json=request)
            if r.status_code==requests.codes.ok or r.status_code == requests.codes.created:
                # success
                print(r.text)
                reactor.callLater(1, self.post_register, resources)
                return 0
            elif r.status_code==requests.codes.bad:
                resp=r.json()
                if 'msg' in resp and 'already exists' in resp['msg']:
                   return 2#has jp wallet
                return 1
            else:#FIXME what to do if API is down
                print(r.text)
                return 1

        except Exception as e:
            logging.exception(e)
        return ret
    def post_register(self, resources):
        try:            
            consumer = Consumer.objects.filter(user__username=resources['parameters']['phone']).first()#  User.objects.filter(username=resources['parameters']['phone']).first()
            
            if consumer:
                if resources['parameters']['gas_yetu'] == 1:
                    consumer.survey_requested=True
                    consumer.save()
                    
               

        except Exception as e:
            logging.exception(e)   
class registration_pin():
    def __init__(self):
        pass
    def processAction(self, resources):
        req = resources['parameters']['request'].strip()
        if not req:
           return resources
        if not req.isdigit() or len(req) > conf.PIN_LENGTH or len(req)< conf.PIN_LENGTH:
            resources['parameters']['response']='Pin must be 4 digits'
            return resources
        try:             
             validate_password(password=req)        
        except exceptions.ValidationError as e:
            resources['parameters']['response']= '{}\n\nEnter new pin'.format(' '.join(list(e.messages)))
            resources['hasResponse'] = True
            return resources

        
        resources['parameters']['pin'] = req
        resources['currentMenu']='registration_pin_confirm'
        return resources
    def start(self, resources):
        if not resources['hasResponse']:
            resources['parameters']['response'] = 'Enter 4 digit pin.'
            resources['hasResponse'] = False
        return resources

class registration_pin_confirm():
    def __init__(self):
        pass
    def processAction(self, resources):
        req = resources['parameters']['request']
        if not req:
           return resources
        if req == '0':
           resources['currentMenu']='registration_pin'
           return resources
        if not req.isdigit() or len(req) > conf.PIN_LENGTH or len(req)< conf.PIN_LENGTH:
            resources['parameters']['response']='Pin must be 4 digits'
            return resources
        if not req== resources['parameters']['pin']:
            resources['parameters']['response']='Pin does not match, enter new pin'
            resources['currentMenu']='registration_pin'
            resources['hasResponse']=True
            return resources
        # register this person
       
        
        resources['currentMenu']='registration_gas_yetu'

        return resources
    def start(self, resources):
        #get counties list
        resources['parameters']['response']= 'Enter the pin again to confirm.\nEnter 0 to re-start pin entry'
        resources['hasResponse']=False
        return resources
    
class registration_supa_gas():
    def __init__(self):
        pass
    def processAction(self, resources):
        req = resources['parameters']['request']
        if req == '1':
             resources['currentMenu']='home'
        return resources

    def start(self, resources): 
        if not resources['hasResponse']:
            resources['parameters']['response']= 'Success!'
            resources['action'] = 'end'
        resources['hasResponse'] = False
        return resources
