#!/usr/bin/env python
from ussd import conf
from ussd.menus import (wait, pin)
from ussd.menu_purchase import purchase_business_code
from ussd.menu_business import (business_distributor,
                                business_distributor_detail)


class home():
    def __init__(self):
        pass

    def processAction(self, resources):
        if not resources['user']:
            return resources
        req = resources['parameters']['request'].strip()
        if req == '1':
            resources['currentMenu'] = 'fulfill_voucher'
        elif req == '2':
            resources['currentMenu'] = 'purchase_service'
        elif req == '3':
            resources['currentMenu'] = 'business_distributor'
        elif req == '4':
            resources['currentMenu'] = 'profile_home'
        return resources

    def start(self, resources):
        if resources['user']:
            resources['action'] = 'con'
            resources['parameters']['response'] = 'Reply with:\n1.Order Refills'\
                '\n2.Order Management\n3.Locate Distributor\n4.Manage Profile'
        else:
            resources['action'] = 'end'
            resources['parameters']['response'] = f'{resources["parameters"]["phone"]} is not a registered Retailer.'
        return resources


retailer_menus = {'home': home, 'purchase_business_code': purchase_business_code,
                  'business_distributor': business_distributor, 'business_distributor_detail': business_distributor_detail,
                  'wait': wait, 'pin': pin,
                  }

from ussd import retailer_order_fulfill
for _cls in [i for i in dir(retailer_order_fulfill)]:
    if _cls.startswith('fulfill_'):
        retailer_menus[_cls] = eval(f'retailer_order_fulfill.{_cls}')

from ussd import retailer_profile
for _cls in [i for i in dir(retailer_profile)]:
    if _cls.startswith('profile_'):
        retailer_menus[_cls] = eval(f'retailer_profile.{_cls}')

from ussd import retailer_purchase
for _cls in [i for i in dir(retailer_purchase)]:
    if _cls.startswith('purchase_'):
        retailer_menus[_cls] = eval(f'retailer_purchase.{_cls}')
