
SERVICE_PURCHASE='purchase'
SERVICE_REFILL='refill'
PRODUCT_TYPE_GASYETU='gasyetu'
PRODUCT_TYPE_SUPAGAS='supagas'
SESSION_KEY = 'nock_'
RETAILER_SESSION_KEY = 'nockr_'
API_BASE_URL = 'http://localhost:8000/api/v1'
API_AUTH_KEY = ''
PIN_LENGTH = 4 # for USSD authentication
PAGE_SIZE=4
PAYMENT_MODE = 'Wallet' # or Cash
GAS_YETU_CYLINDER = '6kg' # default gas yetu cylinder
USSD_API_TOKEN='eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxLCJ1c2VybmFtZSI6ImFkbWluIiwiZmlyc3RfbmFtZSI6IiIsImxhc3RfbmFtZSI6IiIsImV4cCI6MTUyNTM0OTcxNCwicGVybWlzc2lvbnMiOnt9LCJyb2xlcyI6W10sImVtYWlsIjoiZS5naWNodWhpQGphbWJvcGF5LmNvbSIsInBob25lIjpudWxsfQ.w0Lxxpx51Vo736j499H8vEuPWmAUENsNUTRVJ5x5-1M'
ERROR = 'Service momentarily unavailable'
MAXIMUM_QUANTITY =100000

PURCHASE_CYLINDER="purchase_cylinder"
PURCHASE_REFILL="purchase_refill"

LOCATION_CACHE_PREFIX = "loc"

DISTRIBUTOR='nock.distributor'
RETAILER='nock.retailer'
MPESA_STK_URL=''
MPESA_ACCOUNT_PRFIX='NOC'
MPESA_DEV_KEY = 'DEF041F5-7944-4FB9-B43F-D545BB224DD6'
MPESA_PAYBILL = '530100'
# redis
REDIS_HOST = '127.0.0.1'
RESIS_PORT = 6379
REDIS_DB = 0
# messages
MSG_HOME=''
MSG_TRANSACTION_WAIT = 'Please wait while we process your transaction.'
# Registration
MSG_REGISTRATION_SUPA_GAS='Registration was successful for Supa Gas you can proceed to purchase Supa Gas Cylinder or refill.'

MSG_REGISTARTION_GAS_OFFER='Registration was successful for Gas Yetu. Please await visit by our enumerator to administer the questionnaire within 1 week.'
MSG_PURCHASE_HOME='Reply with:\n1.Gas Yetu\n2.Supa Gas\n3.Other Products'
MSG_PURCHASE_GAS_OFFER_CYLINDER='''Cylinder size: 6kg\nUnits: 1\nCynlider Price: Ksh {amount:,}\n\nEnter your PIN to complete purchase'''
MSG_PURCHASE_GAS_OFFER_CYLINDER_SUCCESS='''Success!\nCollect your gas at {business} store.\nVoucher number is:{voucher}'''
MSG_PURCHASE_GAS_OFFER_REFILL='''Cylinder size: 6kg\nUnits: 1\nCynlider Price: Ksh {amount:,}\n\nEnter your PIN to complete purchase'''
MSG_PURCHASE_GAS_OFFER_REFILL_SUCCESS='''Successful!\nCollect your gas refill at {business} store.\nVoucher number is:{voucher}'''
MSG_PURCHASE_SUPA_GAS = '''Select Cylinder size:\n1.3kg\n2.6.kg\n3.13kg\n4.50kg'''
MSG_PURCHASE_SUPA_GAS_CYLINDER_SUCCESS='''Success!\nCollect your gas cylider(s) at {business} store.\nVoucher number is:{voucher}'''
MSG_PURCHASE_SUPA_GAS_REFILL_SUCCESS='''Success!\nCollect your cylider refill at {business} store.\nVoucher number is:{voucher}'''
MSG_PURCHASE_QUANTITY = '''Enter number of cylinders\n0.back'''
MSG_PURCHASE_PAY = '''1.Pay now\n2.Add another purchase'''
MSG_BUSINESS_MENU = 'Reply with:\n1.Distributors\n2.Retailers'
MSG_BUSINESS_MENU_DISTRIBUTORS = 'Select:\n1.Dist1\n2.Dist2\n3.Dist3'
MSG_BUSINESS_MENU_RETAILERS = 'Select:\n1.Retailer1\n2.Retailer2\n3.Retailer3'

# Profile menus
MSG_PROFILE_HOME='Reply with:\n1.Balance\n2.My Statements\n3.Change Pin\n4.Top up\n5.Vouchers'
MSG_PROFILE_PINCHANGE_SUCCESS='Your Pin has been Changed successfully'
MSG_PROFILE_TOPUP_MENU='Wallet Top-up\n\n1.Auto Top-up(M-PESA)\n2.Instructions'
MSG_PROFILE_TOPUP_INSTRUCTIONS='Go to Paybill (XXXXXX) enter Phone number as Account Number,amount and complete topup.'



