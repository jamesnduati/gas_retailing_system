import logging
from twisted.internet import reactor

from django.contrib.auth.password_validation import validate_password

from apps.accounts.models import User
from apps.orders.models import Order
from apps.orders.choices import ORDER_STATUS
from apps.consumers.choices import IPRS_STATUS
from apps.accounts.utils import (Wallet, send_sms)

from ussd import utils
from ussd import conf


class profile_home():
    def __init__(self):
        pass

    def processAction(self, resources):
        req = resources['parameters']['request'].lower()
        if req == '1':
            resources['parameters']['response'] = 'Enter pin to view balance'
            resources['hasResponse'] = True
            resources['currentMenu'] = 'pin'
            resources['nextMenu'] = 'profile_balance'
        elif req == '2':
            resources['parameters']['response'] = 'Enter pin to view statement'
            resources['hasResponse'] = True
            resources['currentMenu'] = 'pin'
            resources['nextMenu'] = 'profile_statement'
        elif req == '3':
            resources['currentMenu'] = 'profile_change_pin'
        elif req == '4':
            resources['currentMenu'] = 'profile_topup'
        elif req == '5':
            resources['currentMenu'] = 'profile_voucher'
        elif req == '6':
            resources['currentMenu'] = 'regedit_id'
        return resources

    def start(self, resources):
        resources['parameters']['response'] = conf.MSG_PROFILE_HOME
        if resources['user']['iprs_status'] == IPRS_STATUS.PendingChange:
            resources['parameters']['response'] = f'{conf.MSG_PROFILE_HOME}\n6.Edit'
        return resources


class profile_balance():
    def __init__(self):
        pass

    def processAction(self, resources):
        resources['currentMenu'] = 'home'
        return resources

    def start(self, resources):
        balance = self.get_balance(resources)
        if balance:
            resources['parameters']['response'] = f'Balance ksh.{balance}\n0.Home'
        else:
            resources['parameters']['response'] = 'Balance could not be obtained at the moment. Please try again later'
            resources['action'] = 'end'
        return resources

    def get_balance(self, resources):
        balance = None
        try:
            user = User.objects.get(username=resources['parameters']['phone'])
            wallet = Wallet(user=user, password=resources['password'])
            del resources['password']
            if wallet:
                resp = wallet.get_wallet_statement()
                if 'code' in resp and resp['code'] == 0:
                    balance = resp['data']['balance']
        except Exception as e:
            logging.exception(e)
        return balance


class profile_statement():
    def __init__(self):
        pass

    def processAction(self, resources):
        resources['currentMenu'] = 'home'
        return resources

    def start(self, resources):
        statement = self.get_statement(resources)
        if statement:
            resources['parameters']['response'] = f'{statement}\n0.Home'
            reactor.callInThread(
                send_sms, resources['parameters']['phone'], statement)
        else:
            resources['parameters']['response'] = 'Statement could not be obtained at the moment. Please try again later'
            resources['action'] = 'end'
        return resources

    def get_statement(self, resources):
        statement = ''
        try:
            user = User.objects.get(username=resources['parameters']['phone'])
            wallet = Wallet(user=user, password=resources['password'])
            del resources['password']
            if wallet:
                resp = wallet.get_wallet_statement()
                if 'code' in resp and resp['code'] == 0:
                    statement_data = resp['data']['statement']
                    if len(statement_data) > 0:
                        statement_data = statement_data[:5]
                        for item in statement_data:
                            statement = f'{statement}\n{item["date"]} {item["item"]} {item["amount"]}'
                    else:
                        statement = 'No records found'
        except Exception as e:
            logging.exception(e)
        return statement


class profile_change_pin():
    def __init__(self):
        pass

    def processAction(self, resources):
        req = resources['parameters']['request'].strip()
        if not req:
            return resources
        if not req.isdigit() or len(req) > conf.PIN_LENGTH or len(req) < conf.PIN_LENGTH:
            resources['parameters']['response'] = 'Pin must be 4 digits'
            return resources

        resources = utils.authenticate(resources, req)
        if not 'token' in resources:
            resources['parameters']['response'] = 'Wrong pin\nEnter current pin.'
            resources['hasResponse'] = True
            return resources
        resources['parameters']['current_pin'] = req
        resources['currentMenu'] = 'profile_new_pin'
        return resources

    def start(self, resources):
        if not resources['hasResponse']:
            resources['parameters']['response'] = 'Enter current pin.'
        resources['hasResponse'] = False
        return resources


class profile_new_pin():
    def __init__(self):
        pass

    def processAction(self, resources):
        req = resources['parameters']['request']
        if not req:
            return resources
        if not req.isdigit() or len(req) > conf.PIN_LENGTH or len(req) < conf.PIN_LENGTH:
            resources['parameters']['response'] = 'Pin must be 4 digits'
            resources['hasResponse'] = True
            return resources
        try:
            validate_password(password=req)
        except exceptions.ValidationError as e:
            resources['parameters']['response'] = '{}\n\nEnter new pin'.format(
                ' '.join(list(e.messages)))
            resources['hasResponse'] = True
            return resources
        resources['parameters']['pin'] = req
        resources['currentMenu'] = 'profile_pin_confirm'
        return resources

    def start(self, resources):
        if not resources['hasResponse']:
            resources['parameters']['response'] = 'Enter a new pin.'
        resources['hasResponse'] = False
        return resources


class profile_pin_confirm():
    def __init__(self):
        pass

    def processAction(self, resources):
        req = resources['parameters']['request'].strip()
        if not req:
            return resources
        if req == '0':
            resources['currentMenu'] = 'profile_new_pin'
            return resources

        if not req == resources['parameters']['pin']:
            resources['parameters']['response'] = 'Confirmation pin does not match, enter new pin'
            resources['currentMenu'] = 'profile_new_pin'
            resources['hasResponse'] = True
            return resources
        # TODO preferably call change_pin REST method if there are other settings to be made
        user = User.objects.get(username=resources['parameters']['phone'])
        user.set_password(req)
        user.save()
        resources['parameters']['response'] = conf.MSG_PROFILE_PINCHANGE_SUCCESS
        resources['currentMenu'] = 'wait'
        resources['hasResponse'] = True

        return resources

    def start(self, resources):
        resources['parameters']['response'] = 'Confirm new pin.\nEnter 0 to re-start pin entry'
        return resources


class profile_topup():
    def __init__(self):
        pass

    def processAction(self, resources):
        req = resources['parameters']['request']
        if not req:
            return resources
        if req == '1':
            resources['currentMenu'] = 'profile_topup_amount'

            return resources
        elif req == '2':
            resources['currentMenu'] = 'profile_instructions'

        return resources

    def start(self, resources):
        resources['parameters']['response'] = conf.MSG_PROFILE_TOPUP_MENU
        return resources


class profile_topup_amount():
    def __init__(self):
        pass

    def processAction(self, resources):
        req = resources['parameters']['request'].strip()
        if not req:
            return resources
        if req.isdigit():  # Initiate MPESA STK menu
            resources['parameters']['amount'] = req
            resources['currentMenu'] = 'wait'
            resources['hasResponse'] = True
            resources['parameters']['response'] = 'Please wait for mpesa menu'
            resources['action'] = 'end'
            reactor.callLater(1, utils.initiate_mpesa_stk, resources)
            return resources
        elif req == '2':
            resources['currentMenu'] = 'profile_instructions'

        return resources

    def start(self, resources):
        resources['parameters']['response'] = 'Enter Amount to load'
        return resources


class profile_instructions():
    def __init__(self):
        pass

    def processAction(self, resources):
        return resources

    def start(self, resources):
        resources['parameters']['response'] = conf.MSG_PROFILE_TOPUP_INSTRUCTIONS
        return resources


class profile_voucher():
    def __init__(self):
        pass

    def processAction(self, resources):
        return resources

    def start(self, resources):
        resources['action'] = 'end'
        reactor.callInThread(self.get_vouchers, resources)
        resources['parameters']['response'] = 'Recent orders sent via SMS'
        return resources

    def get_vouchers(self, resources):
        try:
            sms = ''
            orders = Order.objects.filter(
                buyer_id=resources['user']['tenant_id']).order_by('-order_date')[:5]
            if orders:
                for order in orders:
                    # get the first type of the order detail
                    title = order.order_items.all()[0].listing.product.brand.title
                    # get price
                    amount=0
                    for item in order.order_items.all():
                         amount +=item.quantity*item.listing.list_price
                    sms = f'{sms}{order.voucher_number}: {amount} {order.order_date:%Y-%m-%d} {title} {ORDER_STATUS[order.order_status]}\n'

            else:
                sms = 'No recent orders found'
            if sms:
                send_sms(resources['parameters']['phone'], sms)
        except Exception as e:
            logging.exception(e)
