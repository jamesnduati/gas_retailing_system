#!/bin/bash
echo "-----Starting ussd ---"
echo `pwd`
path=`pwd`
export PYTHONPATH=$PATH:$path

export DJANGO_SETTINGS_MODULE=config.settings
twistd -y ussd/site_ussd.py
exit 0