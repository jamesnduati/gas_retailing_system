#!/usr/bin/env python
from twisted.application import internet, service
from twisted.web import server,resource
from twisted.internet import reactor
from twisted.python.log import ILogObserver, FileLogObserver
from twisted.python.logfile import DailyLogFile

from ussd.web import Factory

PORT=9090
factory = server.Site(Factory())
application = service.Application('mobility-service')
apiService = internet.TCPServer(PORT, factory)
apiService.setName('mobility')
apiService.setServiceParent(application)
#logging
logfile = DailyLogFile("ussd.log", "/tmp")
application.setComponent(ILogObserver, FileLogObserver(logfile).emit)
reactor.suggestThreadPoolSize(200)
