import logging
import base64
from datetime import datetime
import requests

from apps.distribution.models import (Distributor, Retailer)
from ussd import conf


def get_business_from_code(agent_number, is_retailer=False):
    resp = {"business": None, "business_id": None, "business_title": None}
    if Distributor.objects.select_related('tenant').filter(agent_number=agent_number).exists():
        resp["business"] = conf.DISTRIBUTOR
        distributor = Distributor.objects.get(agent_number=agent_number)
        resp["business_id"] = distributor.distributor_tenant.id
        resp["business_title"] = distributor.distributor_tenant.title
    elif Retailer.objects.filter(enabled=True, agent_number=agent_number).exists() and not is_retailer:
        resp["business"] = conf.RETAILER
        retailer = Retailer.objects.get(agent_number=agent_number)
        resp["business_id"] = retailer.retailer_tenant.id
        resp["business_title"] = retailer.trading_name
    return resp


def authenticate(resources, pin):
    try:
        request = {
            "password": pin,
            "username": resources['parameters']['phone']
        }
        r = requests.post(f"{conf.API_BASE_URL}/account/login/", json=request)
        if r.status_code == requests.codes.ok:
            resp = r.json()
            if 'token' in resp:
                resources['token'] = resp['token']
    except Exception as e:
        logging.exception(e)
    return resources


def initiate_mpesa_stk(resources):
    try:
        phone = resources['parameters']['phone']
        account = phone[len(phone)-9:]
        _datetime = datetime.now()
        time_stamp = f'{_datetime:%Y%m%d%H%M%S}'
        key_sting = conf.MPESA_PAYBILL + \
        conf.MPESA_DEV_KEY + time_stamp
        auth_key = base64.b64encode(key_sting.encode('utf8'))
        request = {
            "DevKey": conf.MPESA_DEV_KEY,
            "Timestamp": time_stamp,
            "Paybill": conf.MPESA_PAYBILL,
            "PhoneNumber": resources['parameters']['phone'],
            "Amount": resources['parameters']['amount'],
            "Account": f"{conf.MPESA_ACCOUNT_PRFIX}{account}"
        }
        headers = {"Content-Type": "application/json",
                   "Authorization": auth_key}
        print(request)
        r = requests.post("http://192.168.11.95/STK/api/STK",
                          headers=headers, json=request)
        if r.status_code == requests.codes.ok or r.status_code == requests.codes.created:
            print(r.text)
        else:  # failed
            print(r.text)
    except Exception as e:
        logging.exception(e)
