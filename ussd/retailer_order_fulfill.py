'''
Retailer order rifill fulfilment
'''
import logging
from datetime import datetime
import requests
import json
from twisted.internet import reactor

from apps.accounts.utils import send_sms
from apps.tenants.models import Tenant
from apps.orders.models import Order
from apps.orders.models import ProductItem
from apps.orders.choices import ORDER_STATUS

from ussd import conf
from ussd.menus import pin
from ussd import utils


class fulfill_voucher():
    def __init__(self):
        pass

    def processAction(self, resources):
        req = resources['parameters']['request'].strip()
        order = Order.objects.filter(voucher_number__iexact=req,
                                     seller__id=resources['user']['tenant_id']
                                     ).first()
        if order:
            resources['currentMenu'] = 'fulfill_order'
            resources['order_number'] = order.order_number
        else:
            resources['parameters']['response'] = f'{req} not found'
            resources['hasResponse'] = True
            resources['action'] = 'end'
        return resources

    def start(self, resources):
        if not resources['hasResponse']:
            resources['parameters']['response'] = 'Enter voucher number'
        resources['hasResponse'] = False
        return resources


class fulfill_order():
    def __init__(self):
        pass

    def processAction(self, resources):
        return resources

    def start(self, resources):
        order = Order.objects.filter(order_number=resources['order_number']).prefetch_related('buyer__consumer_set',
                                                                                              'buyer__consumer_set__user',
                                                                                              'order_items__listing__product__cylinder').first()
        consumer = order.buyer._prefetched_objects_cache['consumer'].all()[0]
        text = f'order:{order.voucher_number}\nName:{order.buyer.title}\nID:{consumer.user.identity_number}'
        cylinders = 0
        if len(order.order_items.all()) > 1:
            for item in order.order_items.all():
                text = f'{text}\n{item.quantity} {item.listing.product.brand.title} {item.listing.product.cylinder.title}'
                cylinders += item.quantity

        else:
            order_item = order.order_items.all()[0]
            text = f'{text}\n{order_item.listing.product.brand.title}:{order_item.listing.product.cylinder.title}'
            cylinders = order_item.quantity

        text = f'{text}\nAmount:Ksh{order.total_cost:,}'
        text = f'{text}\nTime:{order.created_on:%y-%m-%d:%I:%M%P}'
        text = f'{text}\n{ORDER_STATUS[order.order_status]}'
        resources['parameters']['total_quantity'] = cylinders
        if order.order_status == ORDER_STATUS.New:
            text = f'{text}\nEnter pin to confirm'
            resources['currentMenu'] = 'pin'
            resources['nextMenu'] = 'fulfill_serial'
            resources['parameters']['listing_id'] = order.order_items.all()[
                0].listing.id
        else:
            resources['action'] = 'end'
        resources['parameters']['response'] = text
        resources['hasResponse'] = False
        return resources


class fulfill_serial():
    def __init__(self):
        pass

    def processAction(self, resources):
        req = resources['parameters']['request'].strip()
        if not req:
            return resources
        resources['parameters']['serial'] = req
        _serials = req.split(',')
        if len(_serials) < resources['parameters']['total_quantity']:
            return resources
        resources['parameters']['serials'] = _serials
        reactor.callInThread(self.fulfill_order, resources)  # call in thread
        resources['parameters']['response'] = 'Please wait for a confirmation SMS'
        resources['action'] = 'end'
        resources['currentMenu'] = 'wait'

        return resources

    def start(self, resources):
        resources['parameters']['response'] = f'Enter {resources["parameters"]["total_quantity"]} cylinder serial number'
        if resources["parameters"]["total_quantity"] > 1:
            resources['parameters']['response'] = f"{resources['parameters']['response']} separated"\
                "by a comma.\neg. serial1,seral2"
        return resources

    def fulfill_order(self, resources):
        try:
            serials = []
            # validate serials
            for serial in resources['parameters']['serials']:
                # with the current design, this might take several minutes
                # go bake a cookie or something..
                # there should be a better way to validate serials
                product_item = ProductItem.objects.filter(serial_no__iexact=serial)
                product_item = product_item.prefetch_related(
                    'product__listing_set').first()
                if product_item:
                    serials.append(
                        {'serial_no': serial, 'listing_id': product_item.product.listing_set.all()[0].id})
                else:
                    sms = f"serial number {serial} cound not be found in your stock"
                    print(f'Failed::{sms}')
                    send_sms(resources['parameters']['phone'], sms)
                    break
            else:
                request = {"assigned_cylinder_serials": serials,
                           "password": resources['password']
                           }
                headers = {'Authorization': f'JWT {resources["token"]}'}
                r = requests.post(f'{conf.API_BASE_URL}/orders/{resources["order_number"]}/fulfill/',
                                  headers=headers, json=request)
                if r.status_code == requests.codes.ok:
                    resp = r.json()
                    print('RESP>>', resp)
                    return True
                else:
                    print("RESP>>", t.text())

        except Exception as e:
            logging.exception(e)
        return False
