import logging
import redis

from ussd import conf


class RedisWrapper(object):
    __monostate = None

    def __init__(self):
        if not RedisWrapper.__monostate:
            RedisWrapper.__monostate = self.__dict__
            self.redis_connection_pool = redis.ConnectionPool(
                host=conf.REDIS_HOST, port=conf.RESIS_PORT, db=conf.REDIS_DB)
            self.redis = redis.StrictRedis(
                connection_pool=self.redis_connection_pool)
        else:
            self.__dict__ = RedisWrapper.__monostate
