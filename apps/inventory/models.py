from django.db import models
from django.db.models import Sum

from apps.inventory.choices import DIRECTION, MOVEMENT_TYPE
from apps.products.models import Product
from apps.tenants.models import Tenant, TenantBaseModel
from apps.consumers.models import Consumer


class Reconciliation(TenantBaseModel):
    """
    Inventory reconciliation
    """
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    reconciliation_date = models.DateTimeField()
    item_count = models.IntegerField()

    def __str__(self):
        return f'{self.product} - {self.reconciliation_date} - {self.item_count}'

    def save(self, *args, **kwargs):
        if not self.pk:
            stock_movement = StockMovement(tenant=self.tenant, product=self.product,
                                           movement_type=MOVEMENT_TYPE.Reconciliation, direction=DIRECTION.In,
                                           movement_date=self.reconciliation_date, no_of=self.item_count,
                                           transaction_entity=self.tenant)
            stock_movement.save()
            super(Reconciliation, self).save(*args, **kwargs)
        else:
            super(Reconciliation, self).save(*args, **kwargs)


class CurrentStock(TenantBaseModel):
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    no_of = models.IntegerField(default=0)

    class Meta:
        unique_together = ('product', 'tenant')

    def __str__(self):
        return f'{self.tenant} - {self.product} - {self.no_of}'


class StockMovement(TenantBaseModel):
    """
    Stock Movement
    """
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    movement_type = models.PositiveSmallIntegerField(choices=MOVEMENT_TYPE)
    direction = models.PositiveSmallIntegerField(choices=DIRECTION, default=1)
    movement_date = models.DateTimeField(auto_now_add=True, blank=True)
    no_of = models.IntegerField(help_text='Number of items moved')
    transaction_entity = models.ForeignKey(
        Tenant, on_delete=models.CASCADE, related_name="stock_movement_transaction_entity", null=True, blank=True)
    customer = models.ForeignKey(
        Consumer, on_delete=models.CASCADE, null=True, blank=True)

    def save(self, *args, **kwargs):
        """
        Compute current stock with the last current stock for speed
        """
        current, _ = CurrentStock.objects.get_or_create(
            tenant=self.tenant, product=self.product)
        if self.direction == DIRECTION.In:
            current.no_of = current.no_of + self.no_of
        elif self.direction == DIRECTION.Out:
            current.no_of = current.no_of - self.no_of
        current.save()

        super(StockMovement, self).save(*args, **kwargs)

    def __str__(self):
        return f'{self.product} - {self.movement_type} - {self.no_of}'

    @property
    def stock_level(self):
        current = CurrentStock.objects.get_or_create(
            tenant=self.tenant, product=self.product)
        return current.no_of

    @property
    def complete_stock_level(self):
        """
        Property to give the current stock level, for auditing purposes
        """
        stock_in = StockMovement.objects.filter(tenant_id=self.tenant_id, product_id=self.product_id,
                                                direction=DIRECTION.In).aggregate(stock_count=Sum('no_of'))

        stock_out = StockMovement.objects.filter(tenant_id=self.tenant_id, product_id=self.product_id,
                                                 direction=DIRECTION.Out).aggregate(stock_count=Sum('no_of'))

        if not stock_in['stock_count']:
            if not stock_out['stock_count']:
                return 0
        else:
            if not stock_out['stock_count']:
                return stock_in['stock_count']

        return stock_in['stock_count'] - stock_out['stock_count']
