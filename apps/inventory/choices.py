from django.utils.translation import ugettext_lazy as _

from model_utils import Choices

MOVEMENT_TYPE = Choices(
    (1, 'Orders', _('Orders')),
    (2, 'Reconciliation', _('Reconciliation')),
    (3, 'Sales', _('Sales')),
    (4, 'Import', _('Import')),
    (5, 'Removal', _('Removal')),
)

DIRECTION = Choices(
    (1, 'In', _('Stock In')),
    (2, 'Out', _('Stock Leaving')),
    (3, 'Returned_Empty', _('Stock Empty Returned')),
    (4, 'Received_Empty', _('Stock Empty Received')),
)
