import datetime

from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.views.generic import CreateView, ListView, View
from rolepermissions.mixins import HasRoleMixin

from apps.products.models import Product
from apps.orders.models import ProductMovement, ProductItem
from apps.tenants.models import Tenant

from ..forms import StockReconciliationForm
from ..models import CurrentStock, Reconciliation, StockMovement

from apps.orders.choices import SALE_STATUS, WITH_STATUS, PRODUCT_STATUS

from apps.distribution.models import GasImporter, Retailer
from apps.sales.models import Sale


class CurrentStockListView(HasRoleMixin, LoginRequiredMixin, ListView):
    model = CurrentStock
    template_name = 'inventory/current_stock.html'
    context_object_name = 'stocks'
    allowed_roles = ['system_admin', 'inventory_manager', 'retailer_admin']

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['display'] = 'Current Stock'
        return context

    def get_queryset(self):
        tenant = self.request.user.employee.tenant
        return CurrentStock.objects.filter(tenant=tenant)


class StockMovementListView(HasRoleMixin, LoginRequiredMixin, ListView):
    model = StockMovement
    template_name = 'inventory/stock_movement.html'
    context_object_name = 'stocks'
    allowed_roles = ['system_admin', 'inventory_manager', 'retailer_admin']

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['display'] = 'Stock Movement'
        return context

    def get_queryset(self):
        tenant = self.request.user.employee.tenant
        return StockMovement.objects.filter(tenant=tenant)


class StockReconciliationCreateView(HasRoleMixin, CreateView):
    model = Reconciliation
    template_name = 'inventory/reconciliation_create.html'
    form_class = StockReconciliationForm
    success_url = reverse_lazy('stock_movement')
    allowed_roles = ['system_admin', 'inventory_manager', 'retailer_admin']

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['display'] = 'Reconcile stock by setting a value for current product (+ or -)'
        return context

    def form_valid(self, form):
        tenant = self.request.user.employee.tenant
        reconciliation = form.instance
        reconciliation.tenant = tenant
        reconciliation.reconciliation_date = datetime.date.today().strftime('%Y-%m-%d')
        reconciliation.save()
        return super(StockReconciliationCreateView, self).form_valid(form)


class StockProductView(HasRoleMixin, ListView):
    model = StockMovement
    template_name = 'inventory/product_view.html'
    allowed_roles = ['system_admin', 'inventory_manager', 'retailer_admin']

    def get_context_data(self, **kwargs):
        """
        product
        serial
        """
        context = super().get_context_data(**kwargs)
        tenant = self.request.tenant
        pk = self.kwargs['pk']
        product = Product.objects.filter(pk=pk).first()
        tenant = Tenant.objects.filter(title=tenant.title).first()
        if GasImporter.objects.filter(gas_importer_tenant=self.request.tenant):
            product_movements = ProductMovement.objects.filter(destination=tenant,
                                                               product_item__product=product,
                                                               product_item__sale_status=SALE_STATUS.Not_Sold,
                                                               product_item__with_status=WITH_STATUS.GasImporter,
                                                               product_item__product_status=PRODUCT_STATUS.Full,
                                                               ).all()
        if Retailer.objects.filter(retailer_tenant=self.request.tenant):
            product_movements = ProductMovement.objects.filter(destination=tenant,
                                                               product_item__product=product,
                                                               product_item__sale_status=SALE_STATUS.Sold,
                                                               product_item__with_status=WITH_STATUS.Retailer,
                                                               product_item__product_status=PRODUCT_STATUS.Full,
                                                               ).all()
        context['stocks'] = product_movements
        return context


class StockProductEmptyView(HasRoleMixin, ListView):
    model = StockMovement
    template_name = 'inventory/product_empty_view.html'
    allowed_roles = ['system_admin', 'inventory_manager', 'retailer_admin']

    def get_context_data(self, **kwargs):
        """
        product
        serial
        """
        context = super().get_context_data(**kwargs)
        tenant = self.request.tenant
        tenant = Tenant.objects.filter(title=tenant.title).first()
        if GasImporter.objects.filter(gas_importer_tenant=self.request.tenant):
            product_movements = ProductMovement.objects.filter(destination=tenant,
                                                               product_item__sale_status=SALE_STATUS.Not_Sold,
                                                               product_item__with_status=WITH_STATUS.GasImporter,
                                                               product_item__product_status=PRODUCT_STATUS.Empty,
                                                               ).all()
        if Retailer.objects.filter(retailer_tenant=self.request.tenant):
            sales = [s.get_product_item_returned for s in Sale.objects.filter(seller=tenant).all()
                     if s.get_product_item_returned.with_status == WITH_STATUS.Retailer
                     and s.get_product_item_returned.product_status == PRODUCT_STATUS.Empty
                     and s.get_product_item_returned.sale_status == SALE_STATUS.Not_Sold]
            product_movements = sales
        context['stocks'] = product_movements
        return context
