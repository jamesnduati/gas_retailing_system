from django.db.models import Sum
from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated
from rolepermissions.mixins import HasRoleMixin

from apps.accounts.choices import EMPLOYEE
from apps.core.viewsets import ReadOnlyViewSet
from apps.distribution.models import Distributor, Retailer
from apps.inventory.models import CurrentStock, Reconciliation, StockMovement
from apps.inventory.serializers import (CurrentStockSerializer,
                                        ReconciliationSerializer,
                                        StockMovementSerializer)


class StockViewSet(HasRoleMixin, viewsets.ModelViewSet):
    """
    Manage stocks
    """
    queryset = StockMovement.objects.filter()
    serializer_class = StockMovementSerializer
    permission_classes = (IsAuthenticated,)
    filter_fields = ('id', 'tenant', 'product', 'movement_type', 'direction')
    allowed_roles = ['distributor_admin', 'retailer_admin']

    def get_queryset(self):
        queryset = super(StockViewSet, self).get_queryset()
        if self.request.user.profile_type == EMPLOYEE:
            employee = self.request.user.employee
            qs = StockMovement.objects.values('product').annotate(
                Sum('no_of')).filter(tenant=employee.tenant)
            return qs
        return queryset.none()


class CurrentStockViewSet(HasRoleMixin, viewsets.ModelViewSet):
    """
    Manage stocks
    """
    queryset = CurrentStock.objects.all()
    serializer_class = CurrentStockSerializer
    permission_classes = (IsAuthenticated,)
    filter_fields = ('id', 'tenant', 'product', 'no_of')
    allowed_roles = ['distributor_admin', 'retailer_admin']

    def get_queryset(self):
        queryset = super(CurrentStockViewSet, self).get_queryset()
        if self.request.user.profile_type == EMPLOYEE:
            employee = self.request.user.employee
            queryset = queryset.filter(tenant=employee.tenant)
            return queryset
        return queryset.none()


class ReconciliationViewSet(HasRoleMixin, viewsets.ModelViewSet):
    """
    Manage stocks
    """
    queryset = Reconciliation.objects.filter()
    serializer_class = ReconciliationSerializer
    permission_classes = (IsAuthenticated)
    filter_fields = ('id', 'tenant', 'product')
    allowed_roles = ['distributor_admin', 'retailer_admin']


class RetailerDistributorStockViewset(HasRoleMixin, ReadOnlyViewSet):
    queryset = CurrentStock.objects.all()
    serializer_class = CurrentStockSerializer
    allowed_roles = ['distributor_admin', 'retailer_admin']

    def get_queryset(self):
        queryset = super(RetailerDistributorStockViewset, self).get_queryset()

        parent_tenant = self.request.user.profile.tenant

        if 'distributors_pk' in self.kwargs:
            distributor = Distributor.objects.filter(enabled=True, pk=self.kwargs['distributors_pk'],
                                                     gas_importer=parent_tenant).first()
            if distributor:
                return queryset.filter(tenant=distributor.distributor_tenant)
            else:
                return queryset.none()
        elif 'retailers_pk' in self.kwargs:
            retailer = Retailer.objects.filter(pk=self.kwargs['retailers_pk'],
                                               enabled=True,
                                               distributor__distributor_tenant=parent_tenant).first()
            if retailer:
                return queryset.filter(tenant=retailer.retailer_tenant)
            else:
                return queryset.none()
        else:
            return queryset.none()
