from django.conf.urls import url

from ..views.default import (
    CurrentStockListView, StockMovementListView, StockReconciliationCreateView, StockProductView, StockProductEmptyView
)

urlpatterns = [
    url(r'^current-stock/$', CurrentStockListView.as_view(), name='current_stock'),
    url(r'^stock-movement/$', StockMovementListView.as_view(), name='stock_movement'),
    url(r'^stock-reconciliation/$', StockReconciliationCreateView.as_view(), name='stock_reconciliation'),
    url(r'^(?P<pk>[0-9a-f-]+)/view/$', StockProductView.as_view(), name="stock_product_view"),
    url(r'^view-empty-stock/$', StockProductEmptyView.as_view(), name="view_empty_stock"),
]
