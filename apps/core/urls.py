from django.conf.urls import url
from apps.core.views import default


urlpatterns = [
    # country urls
    url(r'^countries/$', default.CountriesView.as_view(), name='countries'),
    url(r'^countries/(?P<pk>[0-9]+)/$', default.CountryUpdateView.as_view(),
        name='country-update'),
    url(r'^countries/(?P<pk>[0-9]+)/delete$',
        default.CountryDeleteView.as_view(), name='country-delete'),
    url(r'^countries/new/$', default.CountryCreateView.as_view(),
        name="country-create"),
    # region urls
    url(r'^regions/$', default.RegionsView.as_view(), name='regions'),
    url(r'^regions/(?P<pk>[0-9]+)/$', default.RegionUpdateView.as_view(),
        name='region-update'),
    url(r'^regions/(?P<pk>[0-9]+)/delete$',
        default.RegionDeleteView.as_view(), name='region-delete'),
    url(r'^regions/new/$', default.RegionCreateView.as_view(),
        name="region-create"),

    # sub region urls
    url(r'^subregions/$', default.SubRegionsView.as_view(), name='sub-regions'),
    url(r'^subregions/(?P<pk>[0-9]+)/$', default.SubRegionUpdateView.as_view(),
        name='sub-region-update'),
    url(r'^subregions/(?P<pk>[0-9]+)/delete$',
        default.SubRegionDeleteView.as_view(), name='sub-region-delete'),
    url(r'^subregions/new/$', default.SubRegionCreateView.as_view(),
        name="sub-region-create"),

    # location urls
    url(r'^locations/$', default.LocationsView.as_view(), name='locations'),
    url(r'^locations/(?P<pk>[0-9]+)/$', default.LocationUpdateView.as_view(),
        name='location-update'),
    url(r'^locations/(?P<pk>[0-9]+)/delete$',
        default.LocationDeleteView.as_view(), name='location-delete'),
    url(r'^locations/new/$', default.LocationCreateView.as_view(),
        name="location-create"),

    # village urls
    url(r'^villages/$', default.VillagesView.as_view(), name='villages'),
    url(r'^villages/(?P<pk>[0-9]+)/$', default.VillageUpdateView.as_view(),
        name='village-update'),
    url(r'^villages/(?P<pk>[0-9]+)/delete$',
        default.VillageDeleteView.as_view(), name='village-delete'),
    url(r'^villages/new/$', default.VillageCreateView.as_view(),
        name="village-create"),




    url(r'^country-autocomplete/$',
        default.CountryAutocompleteView.as_view(), name='country-autocomplete'),
    url(r'^region-autocomplete/$',
        default.RegionAutocompleteView.as_view(), name='region-autocomplete'),
    url(r'^sub-region-autocomplete/$',
        default.SubRegionAutocompleteView.as_view(), name='sub-region-autocomplete'),
    url(r'^location-autocomplete/$',
        default.LocationAutocompleteView.as_view(), name='location-autocomplete'),
    url(r'^village-autocomplete/$',
        default.VillageAutocompleteView.as_view(), name='village-autocomplete'),
]
