from dal import autocomplete
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponseRedirect, JsonResponse
from django.shortcuts import redirect, render
from django.urls import reverse_lazy
from django.views.generic import (CreateView, DeleteView, ListView,
                                  TemplateView, UpdateView)
from rest_framework.request import Request
from rest_framework_jwt.authentication import JSONWebTokenAuthentication

from apps.core.forms import (CountryForm, LocationForm, RegionForm,
                             SubRegionForm, VillageForm)
from apps.core.models import Country, Location, Region, SubRegion, Village

from ..exceptions import PermissionDenied
from allauth.account.forms import LoginForm


class CountriesView(LoginRequiredMixin, ListView):
    model = Country
    template_name = 'countries.html'
    context_object_name = 'countries'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['display'] = 'Countries'
        return context


class CountryDeleteView(LoginRequiredMixin, DeleteView):
    model = Country
    template_name = 'country_delete.html'
    success_url = reverse_lazy('countries')

    def delete(self, request, *args, **kwargs):
        """
        Calls the delete() method on the fetched object and then
        redirects to the success URL.
        """
        return HttpResponseRedirect(self.get_success_url())

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['display'] = 'Confirm Delete'
        return context


class CountryCreateView(LoginRequiredMixin, CreateView):
    model = Country
    template_name = 'country.html'
    form_class = CountryForm

    def forms_valid(self, forms):
        # creating the country record
        country = forms.instance.save(commit=False)
        country.save()
        return super(CountryCreateView, self).forms_valid(forms)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['display'] = 'Add New Country'
        return context


class CountryUpdateView(LoginRequiredMixin, UpdateView):
    model = Country
    template_name = 'country_update.html'
    form_class = CountryForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['display'] = 'Update Country'
        return context


class RegionsView(LoginRequiredMixin, ListView):
    model = Region
    template_name = 'regions.html'
    context_object_name = 'regions'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['display'] = 'regions'
        return context


class RegionDeleteView(LoginRequiredMixin, DeleteView):
    model = Region
    template_name = 'region_delete.html'
    success_url = reverse_lazy('regions')

    def delete(self, request, *args, **kwargs):
        """
        Calls the delete() method on the fetched object and then
        redirects to the success URL.
        """
        return HttpResponseRedirect(self.get_success_url())

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['display'] = 'Confirm Delete'
        return context


class RegionCreateView(LoginRequiredMixin, CreateView):
    model = Region
    template_name = 'region.html'
    form_class = RegionForm

    def forms_valid(self, forms):
        # creating the country record
        region = forms.instance.save(commit=False)
        region.save()
        return super(RegionCreateView, self).forms_valid(forms)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['display'] = 'Add New County'
        return context


class RegionUpdateView(LoginRequiredMixin, UpdateView):
    model = Region
    template_name = 'region_update.html'
    form_class = RegionForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['display'] = 'Update County'
        return context


class SubRegionsView(LoginRequiredMixin, ListView):
    model = SubRegion
    template_name = 'sub_regions.html'
    context_object_name = 'sub_regions'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['display'] = 'sub_regions'
        return context


class SubRegionDeleteView(LoginRequiredMixin, DeleteView):
    model = SubRegion
    template_name = 'sub_region_delete.html'
    success_url = reverse_lazy('sub-regions')

    def delete(self, request, *args, **kwargs):
        """
        Calls the delete() method on the fetched object and then
        redirects to the success URL.
        """
        return HttpResponseRedirect(self.get_success_url())

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['display'] = 'Confirm Delete'
        return context


class SubRegionCreateView(LoginRequiredMixin, CreateView):
    model = SubRegion
    template_name = 'sub_region.html'
    form_class = SubRegionForm

    def forms_valid(self, forms):
        # creating the country record
        region = forms.instance.save(commit=False)
        region.save()
        return super(SubRegionCreateView, self).forms_valid(forms)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['display'] = 'Add New Sub County'
        return context


class SubRegionUpdateView(LoginRequiredMixin, UpdateView):
    model = SubRegion
    template_name = 'sub_region_update.html'
    form_class = SubRegionForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['display'] = 'Update Sub County'
        return context


class LocationsView(LoginRequiredMixin, ListView):
    model = Location
    template_name = 'locations.html'
    context_object_name = 'locations'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['display'] = 'locations'
        return context


class LocationDeleteView(LoginRequiredMixin, DeleteView):
    model = SubRegion
    template_name = 'location_delete.html'
    success_url = reverse_lazy('locations')

    def delete(self, request, *args, **kwargs):
        """
        Calls the delete() method on the fetched object and then
        redirects to the success URL.
        """
        return HttpResponseRedirect(self.get_success_url())

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['display'] = 'Confirm Delete'
        return context


class LocationCreateView(LoginRequiredMixin, CreateView):
    model = Location
    template_name = 'location.html'
    form_class = LocationForm

    def forms_valid(self, forms):
        # creating the country record
        location = forms.instance.save(commit=False)
        location.save()
        return super(LocationCreateView, self).forms_valid(forms)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['display'] = 'Add New Location'
        return context


class LocationUpdateView(LoginRequiredMixin, UpdateView):
    model = Location
    template_name = 'location_update.html'
    form_class = LocationForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['display'] = 'Update Location'
        return context


class VillagesView(LoginRequiredMixin, ListView):
    model = Village
    template_name = 'villages.html'
    context_object_name = 'villages'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['display'] = 'villages'
        return context


class VillageDeleteView(LoginRequiredMixin, DeleteView):
    model = Village
    template_name = 'village_delete.html'
    success_url = reverse_lazy('villages')

    def delete(self, request, *args, **kwargs):
        """
        Calls the delete() method on the fetched object and then
        redirects to the success URL.
        """
        return HttpResponseRedirect(self.get_success_url())

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['display'] = 'Confirm Delete'
        return context


class VillageCreateView(LoginRequiredMixin, CreateView):
    model = Village
    template_name = 'village.html'
    form_class = VillageForm

    def forms_valid(self, forms):
        # creating the country record
        village = forms.instance.save(commit=False)
        village.save()
        return super(VillageCreateView, self).forms_valid(forms)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['display'] = 'Add New Village'
        return context


class VillageUpdateView(LoginRequiredMixin, UpdateView):
    model = Village
    template_name = 'village_update.html'
    form_class = VillageForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['display'] = 'Update Village'
        return context


class CountryAutocompleteView(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = Country.objects.all()

        if self.q:
            qs = qs.filter(title__istartswith=self.q)

        return qs


class RegionAutocompleteView(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = Region.objects.all()

        if self.q:
            qs = qs.filter(title__istartswith=self.q)

        return qs


class SubRegionAutocompleteView(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = SubRegion.objects.all()

        region = self.forwarded.get('region', None)

        if region:
            qs = qs.filter(region=region)

        if self.q:
            qs = qs.filter(title__istartswith=self.q)

        return qs


class LocationAutocompleteView(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = Location.objects.all()

        sub_region = self.forwarded.get('sub_region', None)

        if sub_region:
            qs = qs.filter(subregion=sub_region)

        if self.q:
            qs = qs.filter(title__istartswith=self.q)

        return qs


class VillageAutocompleteView(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = Village.objects.all()

        location = self.forwarded.get('location', None)

        if location:
            qs = qs.filter(location=location)

        if self.q:
            qs = qs.filter(title__istartswith=self.q)

        return qs


class IndexPage(TemplateView):
    template_name = 'webpage.html'

    # def get(self, request, *args, **kwargs):
    #     return redirect('account:account')

    def get_context_data(self, **kwargs):
        context = super(IndexPage, self).get_context_data(**kwargs)
        form = LoginForm(self.request.POST or None)  # instance= None
        context["form"] = form
        return context


def get_index(request):
    return redirect('accounts:login')


def is_request_rest(request):
    try:
        user_jwt = JSONWebTokenAuthentication().authenticate(Request(request))
        if user_jwt is not None:
            return True
    except:
        pass
    return False


def handler403(request, exception):
    if is_request_rest(request):
        return JsonResponse({'msg': 'Permission Denied!'}, status=403)
    return render(request, 'errors/403.html', status=403)


def handler404(request):
    return render(request, 'errors/404.html', status=404)


def handler500(request):
    return render(request, 'errors/500.html', status=500)
