from django.utils.translation import ugettext as _

SANITIZE_ERROR = _("Input can not contain HTML tags, emojis or start with any "
                   "of + - = and @.")
first_time_password = _(
    "Welcome to Gas yetu. Your username is {username} and one time login password is {password}.")
