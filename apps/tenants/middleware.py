from django.conf import settings
from django.contrib import auth
from django.contrib.auth.models import AnonymousUser
from django.contrib.sites.shortcuts import get_current_site
from django.utils.functional import SimpleLazyObject
from rest_framework.request import Request
from rest_framework_jwt.authentication import JSONWebTokenAuthentication

from apps.distribution.models import GasImporter, Retailer

from .choices import TENANT_TYPE


def get_user_jwt(request):
    """
    Replacement for django session auth get_user & auth.get_user
    JSON Web Token authentication. Inspects the token for the user_id,
    attempts to get that user from the DB & assigns the user on the
    request object. Otherwise it defaults to AnonymousUser.

    Returns: instance of user object or AnonymousUser object
    """
    user = None
    try:
        user_jwt = JSONWebTokenAuthentication().authenticate(Request(request))
        if user_jwt is not None:
            user = user_jwt[0]
    except:
        pass

    return user or AnonymousUser()


class TenantMiddleware(object):
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        if request.user.is_anonymous:
            request.user = SimpleLazyObject(lambda: get_user_jwt(request))
        if request.user.is_authenticated and not request.user.is_superuser:
            tenant = request.user.profile.tenant
            if tenant.tenant_type == TENANT_TYPE.Retailer:
                retailer = tenant.retailer
                gas_importer = retailer.gas_importer
                request.tenant_phone = retailer.contact_phone
                request.tenant_seller = gas_importer.gas_importer_tenant
                request.tenant_seller_code = gas_importer.agent_number
            elif tenant.tenant_type == TENANT_TYPE.GasImporter:
                gas_importer = tenant.gas_importer
                request.tenant_phone = gas_importer.contact_phone
                request.tenant_agent_email = ''
                request.tenant_seller = ''
                request.tenant_seller_code = ''
            request.tenant = tenant
        response = self.get_response(request)
        return response


class SuperUserMiddleware(object):
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        response = self.get_response(request)
        url = request.get_full_path()
        # if request.user.is_authenticated and request.user.is_superuser and not url.startswith("/admin"):
        #     auth.logout(request)
        return response
