from django.utils.translation import ugettext_lazy as _

from model_utils import Choices

JAMBO_PAY = 1
GAS_IMPORTER = 2
RETAILER = 3

TENANT_TYPE = Choices(
    (1, 'JamboPay', _('JamboPay')),
    (2, 'GasImporter', _('Gas Importer')),
    (3, 'Retailer', _('Retailer')),
)

CYLINDER_SERVICE_REFILL = 1
CYLINDER_SERVICE_PURCHASE = 2

CYLINDER_SERVICE_CHOICES = Choices(
    (CYLINDER_SERVICE_REFILL, 'Cylinder Refill', _('Cylinder Refill')),
    (CYLINDER_SERVICE_PURCHASE, 'Cylinder Purchase', _('Cylinder Refill')),
)
