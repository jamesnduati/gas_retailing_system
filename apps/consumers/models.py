from django.db import models

from apps.core.models import BaseModel


class ConsumerManager(models.Manager):
    def create_consumer(self, user, title, **kwargs):
        consumer = self.create(
                               phone=kwargs['phone'],
                               name=kwargs['name'])
        return consumer


class Consumer(BaseModel):
    """
    Consumers registered in the system
    """

    phone = models.CharField(
        'mobile number', max_length=16, null=True, blank=True, default=None, unique=True)
    name = models.CharField(
        max_length=150, blank=True, help_text='Customer Name')

    objects = ConsumerManager()

    def __str__(self):
        return self.name + '-' + self.phone
