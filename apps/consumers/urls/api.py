from django.conf.urls import include, url
from rest_framework.routers import DefaultRouter

from apps.consumers.views import api

router = DefaultRouter()
router.register(r'beneficiaries', api.BeneficiaryViewset)
router.register(r'consumers', api.ConsumerViewset)


urlpatterns = [
    url(r'^', include(router.urls))
]
