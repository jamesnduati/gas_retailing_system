from django.conf.urls import url
import allauth.account.views as allauth_views
from apps.consumers.views import reports

urlpatterns = [
    url(r'^reports/consumers/$', reports.get_consumers, name='consumers'),
    url(r'^reports/generate/pdf/$', reports.generate_pdf, name='generate_pdf'),
    url(r'^reports/generate/excel/$', reports.generate_excel, name='generate_excel'),
]
