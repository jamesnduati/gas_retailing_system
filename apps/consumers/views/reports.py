import xlsxwriter
import tempfile
from io import BytesIO
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render, reverse
from django.urls import reverse_lazy
from django.views.generic import (CreateView, DeleteView, DetailView, ListView,
                                  RedirectView, UpdateView, View)
from django.template.loader import render_to_string
from weasyprint import HTML
from apps.tenants.models import Tenant
from apps.consumers.models import Consumer
from apps.core.models import Region, SubRegion, Location, Village
from datetime import date
from config.settings.base import STATIC_ROOT, STATIC_URL, BASE_DIR
static_path = BASE_DIR + STATIC_URL

regions = Region.objects.all()
sub_regions = SubRegion.objects.all()
locations = Location.objects.all()
villages = Village.objects.all()


@login_required
def get_consumers(request):
    if request.method == 'GET':
        report_data = []
        consumers = Consumer.objects.all()
        for consumer in consumers:
            data={}
            data['name'] = consumer.user.first_name+' '+consumer.user.last_name
            data['email'] = consumer.user.email
            data['phone'] = consumer.user.phone
            data['id_no'] = consumer.user.identity_number
            data['status'] = consumer.user.is_active
            data['date_joined'] = consumer.user.date_joined
            data['survey'] = consumer.user.survey_requested
            if Beneficiary.objects.filter(consumer_ptr_id=consumer.id):
                data['product'] = 'Gas Yetu'
            else:
                data['product'] = 'Super Gas'

            for region in regions:
                if region.id ==consumer.user.region_id:
                    data['region'] = region.title
                else:
                    data['region'] = None
 
            for sub_region in sub_regions:
                if sub_region.id == consumer.user.sub_region_id:
                    data['sub_region'] = sub_region.title
                else:
                    data['sub_region'] = None

            for location in locations:
                if location.id == consumer.user.location_id:
                    data['location'] = location.title
                else:
                    data['location'] = None

            for village in villages:
                if village.id == consumer.user.village_id:
                    data['village'] = village.title
                else:
                    data['village'] = None

            report_data.append(data)
        return render(request, 'reports/consumers.html', {'reports': report_data})
    else:
        min_value = request.POST['min-date']
        max_value = request.POST['max-date']
        report_data = []
        consumers = Consumer.objects.all()
        for consumer in consumers:
            if consumer.user.date_joined.strftime('%d-%m-%Y') >= min_value and consumer.user.date_joined.strftime('%d-%m-%Y') <= max_value:
                data = {}
                data['name'] = consumer.user.first_name+' '+consumer.user.last_name
                data['email'] = consumer.user.email
                data['phone'] = consumer.user.phone
                data['id_no'] = consumer.user.identity_number
                data['status'] = consumer.user.is_active
                data['date_joined'] = consumer.user.date_joined
                data['survey'] = consumer.user.survey_requested
                if Beneficiary.objects.filter(consumer_ptr_id=consumer.id):
                    data['product'] = 'Gas Yetu'
                else:
                    data['product'] = 'Super Gas'

                for region in regions:
                    if region.id == consumer.user.region_id:
                        data['region'] = region.title
                    else:
                        data['region'] = None

                for sub_region in sub_regions:
                    if sub_region.id == consumer.user.sub_region_id:
                        data['sub_region'] = sub_region.title
                    else:
                        data['sub_region'] = None

                for location in locations:
                    if location.id == consumer.user.location_id:
                        data['location'] = location.title
                    else:
                        data['location'] = None

                for village in villages:
                    if village.id == consumer.user.village_id:
                        data['village'] = village.title
                    else:
                        data['village'] = None

                report_data.append(data)
        return render(request, 'reports/consumers.html', {'reports':report_data})

@login_required
def generate_pdf(request):
    tenant = request.tenant
    max_value = request.GET['max_date']
    min_value = request.GET['min_date']
    report_data = []
    consumers = Consumer.objects.all()
    for consumer in consumers:
        data = {}
        if consumer.user.date_joined.strftime('%d-%m-%Y') >= min_value and consumer.user.date_joined.strftime('%d-%m-%Y') <= max_value:
            data['name'] = consumer.user.first_name +' '+consumer.user.last_name
            data['email'] = consumer.user.email
            data['phone'] = consumer.user.phone
            data['id_no'] = consumer.user.identity_number
            data['status'] = consumer.user.is_active
            data['date_joined'] = consumer.user.date_joined
            data['survey'] = consumer.user.survey_requested
            if Beneficiary.objects.filter(consumer_ptr_id=consumer.id):
                data['product'] = 'Gas Yetu'
            else:
                data['product'] = 'Super Gas'

            for region in regions:
                if region.id == consumer.user.region_id:
                    data['region'] = region.title
                else:
                    data['region'] = None

            for sub_region in sub_regions:
                if sub_region.id == consumer.user.sub_region_id:
                    data['sub_region'] = sub_region.title
                else:
                    data['sub_region'] = None

            for location in locations:
                if location.id == consumer.user.location_id:
                    data['location'] = location.title
                else:
                    data['location'] = None

            for village in villages:
                if village.id == consumer.user.village_id:
                    data['village'] = village.title
                else:
                    data['village'] = None
            report_data.append(data)
        else:
            data['name'] = consumer.user.first_name +' '+consumer.user.last_name
            data['email'] = consumer.user.email
            data['phone'] = consumer.user.phone
            data['id_no'] = consumer.user.identity_number
            data['status'] = consumer.user.is_active
            data['date_joined'] = consumer.user.date_joined
            data['survey'] = consumer.user.survey_requested
            if Beneficiary.objects.filter(consumer_ptr_id=consumer.id):
                data['product'] = 'Gas Yetu'
            else:
                data['product'] = 'Super Gas'

            for region in regions:
                if region.id == consumer.user.region_id:
                    data['region'] = region.title
                else:
                    data['region'] = None

            for sub_region in sub_regions:
                if sub_region.id == consumer.user.sub_region_id:
                    data['sub_region'] = sub_region.title
                else:
                    data['sub_region'] = None

            for location in locations:
                if location.id == consumer.user.location_id:
                    data['location'] = location.title
                else:
                    data['location'] = None

            for village in villages:
                if village.id == consumer.user.village_id:
                    data['village'] = village.title
                else:
                    data['village'] = None

            report_data.append(data)
    # Rendered
    html_string = render_to_string(
        'reports/consumer_report.html', {
            'reports': report_data, 
            'date': date.today(),
            'static':static_path,
            'logo': static_path+'images/logo.png',
            'tenant': tenant
            }
        )
    html = HTML(string=html_string).write_pdf()
    # Creating http response
    response = HttpResponse(html, content_type='application/pdf')
    response['Content-Disposition'] = 'filename=consumer_report.pdf'
    return response


@login_required
def generate_excel(request):
    output = BytesIO()
    tenant = request.tenant
    max_value = request.GET['max_date']
    min_value = request.GET['min_date']
    report_data = []
    consumers = Consumer.objects.all()
    for consumer in consumers:
        data = {}
        if consumer.user.date_joined.strftime('%d-%m-%Y') >= min_value and consumer.user.date_joined.strftime('%d-%m-%Y') <= max_value:
            data['name'] = consumer.user.first_name +' '+consumer.user.last_name
            data['email'] = consumer.user.email
            data['phone'] = consumer.user.phone
            data['id_no'] = consumer.user.identity_number
            if consumer.user.is_active == 1:
                data['status'] = "True"
            else:
                data['status'] = "False"
            data['date_joined'] = consumer.user.date_joined.strftime('%d-%m-%Y')
            if consumer.user.survey_requested == 1:
                data['survey'] = "True"
            else:
                data['survey'] = "False"
            if Beneficiary.objects.filter(consumer_ptr_id=consumer.id):
                data['product'] = 'Gas Yetu'
            else:
                data['product'] = 'Super Gas'

            for region in regions:
                if region.id == consumer.user.region_id:
                    data['region'] = region.title
                else:
                    data['region'] = None

            for sub_region in sub_regions:
                if sub_region.id == consumer.user.sub_region_id:
                    data['sub_region'] = sub_region.title
                else:
                    data['sub_region'] = None

            for location in locations:
                if location.id == consumer.user.location_id:
                    data['location'] = location.title
                else:
                    data['location'] = None

            for village in villages:
                if village.id == consumer.user.village_id:
                    data['village'] = village.title
                else:
                    data['village'] = None

            report_data.append(data)
        else:
            data['name'] = consumer.user.first_name +' '+consumer.user.last_name
            data['email'] = consumer.user.email
            data['phone'] = consumer.user.phone
            data['id_no'] = consumer.user.identity_number
            if consumer.user.is_active == 1:
                data['status'] = "True"
            else:
                data['status'] = "False"
            data['date_joined'] = consumer.user.date_joined.strftime('%d-%m-%Y')
            if consumer.user.survey_requested == 1:
                data['survey'] = "True"
            else:
                data['survey'] = "False"
            if Beneficiary.objects.filter(consumer_ptr_id=consumer.id):
                data['product'] = 'Gas Yetu'
            else:
                data['product'] = 'Super Gas'

            for region in regions:
                if region.id == consumer.user.region_id:
                    data['region'] = region.title
                else:
                    data['region'] = None

            for sub_region in sub_regions:
                if sub_region.id == consumer.user.sub_region_id:
                    data['sub_region'] = sub_region.title
                else:
                    data['sub_region'] = None

            for location in locations:
                if location.id == consumer.user.location_id:
                    data['location'] = location.title
                else:
                    data['location'] = None

            for village in villages:
                if village.id == consumer.user.village_id:
                    data['village'] = village.title
                else:
                    data['village'] = None

            report_data.append(data)
    # Create a workbook and add a worksheet.
    workbook = xlsxwriter.Workbook(output, {'in_memory': True})
    worksheet = workbook.add_worksheet()

    # merge format for the cells
    merge_format = workbook.add_format(
                        {
                            'bold': 1,
                            'align': 'center',
                            'valign': 'vcenter'
                        }
                    )

    # merge logo cells
    # Merge 3 cells.
    worksheet.merge_range('F1:G4','', merge_format)

    # setting the header with a logo
    image_width = 128.0
    image_height = 40.0

    cell_width = 64.0
    cell_height = 20.0

    x_scale = cell_width/image_width
    y_scale = cell_height/image_height

    logo = static_path+'images/logo.png'
    worksheet.insert_image('F1', logo,
                           {'x_scale': x_scale, 'y_scale': y_scale}
                           )
    # Add a bold format to use to highlight cells.
    bold = workbook.add_format({'bold': True})

    # adding the header under the logo
    worksheet.merge_range('F5:G5', '', merge_format)
    worksheet.write('F5', 'Consumers Report', bold)
    worksheet.merge_range('A1:B2', '', merge_format)
    worksheet.write(
        'A1', 'Printed by: '+str(tenant)+'\nDate: '+str(date.today()))


    worksheet.merge_range('I1:L4', '', merge_format)
    worksheet.write('I1', 'National Oil Corporation of Kenya \nKAWI house – South C Redcross Road, \nOff Popo Road \nTelephone: +254-20-695 2000')


    # Write some data headers.
    worksheet.write('A6', 'Name', bold)
    worksheet.write('B6', 'Email', bold)
    worksheet.write('C6', 'Phone No.', bold)
    worksheet.write('D6', 'ID No.', bold)
    worksheet.write('E6', 'Status', bold)
    worksheet.write('F6', 'Date Joined', bold)
    worksheet.write('G6', 'Survey', bold)
    worksheet.write('H6', 'Product', bold)
    worksheet.write('I6', 'Region', bold)
    worksheet.write('J6', 'Sub Region', bold)
    worksheet.write('K6', 'Location', bold)
    worksheet.write('L6', 'Village', bold)

    # Start from the first cell below the headers.
    row = 6
    col = 0

    # Iterate over the data and write it out row by row.
    rows = list(report_data[0].keys())
    for i, row in enumerate(report_data):
        i += 6
        for j, col in enumerate(rows):
            worksheet.write(i, j, row[col])

    workbook.close()
    output.seek(0)
    # response
    filename = 'consumers-'+str(date.today())+'.xls'
    response = HttpResponse(output.read(), content_type="application/ms-excel")
    response['Content-Disposition'] = "attachment; filename="+filename
    return response
