# Generated by Django 2.0.8 on 2020-05-25 11:19

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('consumers', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='beneficiary',
            name='consumer_ptr',
        ),
        migrations.RemoveField(
            model_name='consumer',
            name='iprs_change_counter',
        ),
        migrations.RemoveField(
            model_name='consumer',
            name='iprs_date_of_birth',
        ),
        migrations.RemoveField(
            model_name='consumer',
            name='iprs_first_name',
        ),
        migrations.RemoveField(
            model_name='consumer',
            name='iprs_last_name',
        ),
        migrations.RemoveField(
            model_name='consumer',
            name='iprs_other_name',
        ),
        migrations.RemoveField(
            model_name='consumer',
            name='iprs_status',
        ),
        migrations.RemoveField(
            model_name='consumer',
            name='iprs_verification_date',
        ),
        migrations.RemoveField(
            model_name='consumer',
            name='iprs_verified',
        ),
        migrations.RemoveField(
            model_name='consumer',
            name='similarity_score',
        ),
        migrations.RemoveField(
            model_name='consumer',
            name='spouse',
        ),
        migrations.RemoveField(
            model_name='consumer',
            name='survey_conducted',
        ),
        migrations.RemoveField(
            model_name='consumer',
            name='survey_requested',
        ),
        migrations.RemoveField(
            model_name='consumer',
            name='tenant',
        ),
        migrations.RemoveField(
            model_name='consumer',
            name='user',
        ),
        migrations.AddField(
            model_name='consumer',
            name='name',
            field=models.CharField(blank=True, help_text='Customer Name', max_length=150),
        ),
        migrations.AddField(
            model_name='consumer',
            name='phone',
            field=models.CharField(blank=True, default=None, max_length=16, null=True, unique=True, verbose_name='mobile number'),
        ),
        migrations.DeleteModel(
            name='Beneficiary',
        ),
    ]
