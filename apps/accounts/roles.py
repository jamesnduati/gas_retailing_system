from rolepermissions.roles import AbstractUserRole


class SystemAdmin(AbstractUserRole):
    available_permissions = {
        'can_manage_regions': True,
        'can_manage_products': True,
        'can_manage_users': True,
        'can_view_orders_report': True,
        'can_view_orders_report': True,
        'can_manage_inventory': True,
        'can_dispatch_delivery': True,
        'edit_serials': True,
    }


class FinanceManager(AbstractUserRole):
    available_permissions = {
        'can_fulfill_retailer_orders': True,
        'can_view_orders_report': True,
    }


class InventoryManager(AbstractUserRole):
    available_permissions = {
        'can_manage_inventory': True,
        'can_manage_products': True,
        'can_dispatch_delivery': True,
        'edit_serials': True,
    }


class GasImporterAdmin(AbstractUserRole):
    available_permissions = {
        'can_create_retailer': True,
        'can_settle_order': True,
        'can_fulfill_retailer_orders': True,
        'can_manage_retailers': True,
        'can_view_orders_report': True,
    }


class RetailerAdmin(AbstractUserRole):
    available_permissions = {
        'can_settle_order': True,
        'can_make_order': True,
    }
