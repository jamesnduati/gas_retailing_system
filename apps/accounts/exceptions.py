from django.utils.translation import ugettext as _


class EmailNotVerifiedError(Exception):

    def __init__(self, msg=None):
        if not msg:
            self.msg = _("The email has not been verified.")
        super().__init__(msg)


class PhoneNotVerifiedError(Exception):

    def __init__(self, msg=None):
        if not msg:
            self.msg = _("The phone has not been verified.")
        super().__init__(msg)


class AccountInactiveError(Exception):

    def __init__(self, msg=None):
        if not msg:
            self.msg = _("Both email and phone have not been verified.")
        super().__init__(msg)


class SMSException(Exception):

    def __init__(self, msg=None):
        if not msg:
            self.msg = _(
                "An error was encountered while trying to send the message. Try again later")
        super().__init__(msg)
