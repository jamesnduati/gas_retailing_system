from allauth.account.auth_backends import AuthenticationBackend as Backend
from django.contrib.auth.backends import ModelBackend

from apps.accounts.models import User


class AuthenticationBackend(Backend):
    def _authenticate_by_email(self, **credentials):
        # Allow login by both email and username
        email = credentials.get('email', credentials.get('username'))
        try:
            user = User.objects.get(email__iexact=email)
            if user.check_password(credentials["password"]):
                return user
        except User.DoesNotExist:
            pass

        return None


class PhoneAuthenticationBackend(ModelBackend):
    def authenticate(self, **credentials):
        username = credentials.get('username')
        try:
            user = User.objects.get(username__iexact=username)
            if user.check_password(credentials["password"]):
                return user
        except User.DoesNotExist:
            pass
        return None
