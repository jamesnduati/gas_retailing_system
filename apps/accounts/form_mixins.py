from allauth.account import forms as allauth_forms
from django import forms
from django.contrib.auth.password_validation import validate_password
from phonenumbers import parse as parse_phone

from apps.accounts.validators import phone_validator


class ChangePasswordMixin:

    def clean_password(self):
        password = self.cleaned_data['password']
        validate_password(password, user=self.user)
        return password

    def save(self):
        allauth_forms.get_adapter().set_password(
            self.user, self.cleaned_data['password'])
