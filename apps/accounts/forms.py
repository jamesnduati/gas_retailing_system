import cloudinary
from allauth.account import forms as allauth_forms
from allauth.account.models import EmailAddress
from dal import autocomplete
from django import forms
from django.conf import settings
from django.contrib.auth.password_validation import validate_password
from django.utils.translation import ugettext as _
from parsley.decorators import parsleyfy
from phonenumbers import NumberParseException
from phonenumbers import parse as parse_phone

from apps.accounts import messages, utils
from apps.accounts.form_mixins import ChangePasswordMixin
from apps.accounts.models import Invitations, OTPVerificationDevice, User
from apps.core.models import Location, Region, SubRegion, Village


@parsleyfy
class RegisterBasicInfoForm(forms.Form):
    email = forms.EmailField(required=False)
    phone = forms.RegexField(regex=r'^(07)[0-9]{8}$',
                             error_messages={'invalid': messages.phone_format},
                             required=True)
    password = forms.RegexField(regex=r'^[0-9]{4}$',
                                error_messages={
                                    'invalid': messages.password_format},
                                required=True, widget=forms.PasswordInput())
    first_name = forms.CharField(label=_("First Name"), max_length=100)
    last_name = forms.CharField(label=_("Last Name"), max_length=100)

    def clean(self):
        cleaned_data = super().clean()

        email = cleaned_data.get('email', None)
        if email:
            email = email.casefold()
            if EmailAddress.objects.filter(email=email).exists():
                raise forms.ValidationError(
                    _("User with this Email address already exists."))

        phone = cleaned_data.get('phone', None)
        if phone:
            phone = '254' + phone[-9:]
            if OTPVerificationDevice.objects.filter(
                    unverified_phone=phone).exists():
                raise forms.ValidationError(
                    _("User with this Phone number already exists."))
            if User.objects.filter(
                    username=phone).exists():
                raise forms.ValidationError(
                    _("User with this Phone number already exists."))
            if User.objects.filter(
                    phone=phone).exists():
                raise forms.ValidationError(
                    _("User with this Phone number already exists."))

        return cleaned_data


@parsleyfy
class RegisterOtherInfoForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ['identity_number', 'region',
                  'sub_region', 'location', 'village']
        widgets = {
            'region': autocomplete.ModelSelect2(url='region-autocomplete'),
            'sub_region': autocomplete.ModelSelect2(url='sub-region-autocomplete', forward=['region']),
            'location': autocomplete.ModelSelect2(url='location-autocomplete', forward=['sub_region']),
            'village': autocomplete.ModelSelect2(url='village-autocomplete', forward=['location']),
        }


@parsleyfy
class StaffRegisterForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput())

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['email'].widget.attrs['readonly'] = True

    class Meta:
        model = User
        fields = ['password', 'last_name', 'email', 'first_name']

    class Media:
        js = ('js/sanitize.js', )

    def clean_password(self):
        password = self.data.get('password')
        validate_password(password)

        return password

    def clean_email(self):
        email = self.data.get('email')
        if email:
            email = email.casefold()
            if EmailAddress.objects.filter(email=email).exists():
                raise forms.ValidationError(
                    _("User with this Email address already exists."))
            if not Invitations.objects.filter(email=email, accepted=False).exists():
                raise forms.ValidationError(
                    _("User with this Email address was not invited"))
        else:
            email = None
        return email

    def save(self, *args, **kwargs):
        user = super().save(commit=False)
        user.username = self.cleaned_data['email']
        user.set_password(self.cleaned_data['password'])
        user.email_verified = True
        user.save()
        return user


class UserInviteForm(forms.Form):
    email = forms.EmailField()
    roles = forms.ChoiceField(choices=utils.get_roles_tuple)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def clean_email(self):
        email = self.data.get('email')
        if email:
            if User.objects.filter(email=email).exists():
                raise forms.ValidationError(
                    _("User with this email already exists!"))
        else:
            email = None
        return email


class ResetPasswordForm(allauth_forms.ResetPasswordForm):
    email = forms.EmailField(required=False)

    phone = forms.RegexField(regex=r'^\+[0-9]{5,14}$',
                             error_messages={'invalid': messages.phone_format},
                             required=False)

    def clean(self):
        if not self.data.get('email') and not self.data.get('phone'):
            raise forms.ValidationError(_(
                "You cannot leave both phone and email empty."))

    def clean_email(self):
        email = self.cleaned_data.get('email')
        if email:
            email = email.casefold()
            self.users = User.objects.filter(email=email)
        else:
            email = None
        return email

    def clean_phone(self):
        phone = self.data.get('phone')
        if not phone:
            phone = None
        return phone

    def save(self, request, **kwargs):
        phone = self.data.get('phone')
        if phone:
            request.session['phone'] = phone
            try:
                user = User.objects.get(phone=phone)
                device = user.otpverificationdevice_set.get_or_create(
                    unverified_phone=phone, label='password_reset')
                device[0].generate_challenge()
            except User.DoesNotExist:
                pass
            return phone
        else:
            super().save(request, **kwargs)


class ProfileForm(forms.ModelForm):
    email = forms.EmailField(required=False)

    phone = forms.RegexField(regex=r'^\+[0-9]{5,14}$',
                             error_messages={'invalid': messages.phone_format},
                             required=False)
    password = forms.CharField(widget=forms.PasswordInput())

    class Meta:
        model = User
        fields = ['email', 'phone',
                  'first_name', 'last_name', 'image']

    class Media:
        js = ('js/sanitize.js', )

    def __init__(self, *args, **kwargs):
        self._send_confirmation = False
        self.request = kwargs.pop('request', None)
        super().__init__(*args, **kwargs)
        self.current_email = self.instance.email
        if self.current_email:
            self.fields['email'].required = True

        self.current_phone = self.instance.phone
        if self.current_phone:
            self.fields['phone'].required = True

        self.current_photo = self.instance.image

    def clean(self):
        if not self.instance.update_profile:
            raise forms.ValidationError(
                _("The profile for this user can not be updated."))

        return super().clean()

    def clean_password(self):
        if (self.fields['password'].required and
                not self.instance.check_password(self.data.get('password'))):
            raise forms.ValidationError(
                _("Please provide the correct password for your account."))

    def clean_phone(self):
        phone = self.data.get('phone')
        if phone:
            if (phone != self.current_phone and
                OTPVerificationDevice.objects.filter(unverified_phone=phone
                                                     ).exists()):
                raise forms.ValidationError(
                    _("User with this Phone number already exists."))
            try:
                parse_phone(phone)
            except NumberParseException:
                raise forms.ValidationError(
                    _("Please enter a valid country code."))

        else:
            phone = None

        return phone

    def clean_email(self):
        email = self.data.get('email')
        if email:
            email = email.casefold()
            if (email != self.current_email and
                    EmailAddress.objects.filter(email=email).exists()):
                raise forms.ValidationError(
                    _("User with this Email address already exists."))
        else:
            email = None

        return email

    def save(self, *args, **kwargs):
        user = super().save(commit=False, *args, **kwargs)

        if self.current_email != user.email:
            self.instance.emailaddress_set.all().delete()

            send_email_confirmation(self.request, user)
            utils.send_email_update_notification(self.current_email)
            user.email = self.current_email

        if self.current_phone != user.phone:
            self.instance.otpverificationdevice_set.all().delete()

            device = OTPVerificationDevice.objects.create(
                user=self.instance, unverified_phone=user.phone)
            device.generate_challenge()
            utils.send_sms(self.current_phone, messages.phone_change)
            user.phone = self.current_phone

        if self.current_photo:
            cloudinary.uploader.destroy(self.current_photo.public_id)

        user.save()
        return user


class ChangePasswordForm(ChangePasswordMixin,
                         allauth_forms.UserForm):

    oldpassword = allauth_forms.PasswordField(label=_("Current Password"))
    password = allauth_forms.SetPasswordField(label=_("New Password"))

    def clean_oldpassword(self):
        if not self.user.check_password(self.cleaned_data.get('oldpassword')):
            raise forms.ValidationError(_("Please type your current"
                                          " password."))
        return self.cleaned_data['oldpassword']


class TokenVerificationForm(forms.Form):
    token = forms.CharField(label=_("Token"), max_length=settings.TOTP_DIGITS)

    def __init__(self, *args, **kwargs):
        self.device = kwargs.pop('device', None)
        super().__init__(*args, **kwargs)

    def clean_token(self):
        token = self.data.get('token')
        device = self.device
        if not device:
            raise forms.ValidationError(
                _("The token could not be verified."
                    " Please click on 'here' to try again."))
        try:
            token = int(token)
        except ValueError:
            raise forms.ValidationError(_("Token must be a number."))

        if device.verify_token(token):
            return token
        elif device.verify_token(token=token, tolerance=5):
            raise forms.ValidationError(
                _("The token has expired."
                  " Please click on 'here' to receive the new token."))
        else:
            raise forms.ValidationError(
                "Invalid Token. Enter a valid token.")


class ResetPasswordKeyForm(ChangePasswordMixin,
                           forms.Form):

    password = allauth_forms.SetPasswordField(label=_("New Password"))

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user', None)
        self.temp_key = kwargs.pop('temp_key', None)
        super().__init__(*args, **kwargs)


class ResendTokenForm(forms.Form):
    email = forms.EmailField(required=False)
    phone = forms.RegexField(regex=r'^(07)[0-9]{8}$',
                             error_messages={'invalid': messages.phone_format},
                             required=False)

    def clean(self):
        if not self.data.get('email') and not self.data.get('phone'):
            raise forms.ValidationError(
                _("You cannot leave both phone and email empty."))


class TokenVerificationForm(forms.Form):
    token = forms.CharField(label=_("Token"), max_length=settings.TOTP_DIGITS)

    def __init__(self, *args, **kwargs):
        self.device = kwargs.pop('device', None)
        super().__init__(*args, **kwargs)

    def clean_token(self):
        token = self.data.get('token')
        device = self.device
        if not device:
            raise forms.ValidationError(
                _("The token could not be verified."
                    " Please click on 'here' to try again."))
        try:
            token = int(token)
        except ValueError:
            raise forms.ValidationError(_("Token must be a number."))

        if device.verify_token(token):
            return token
        elif device.verify_token(token=token, tolerance=5):
            raise forms.ValidationError(
                _("The token has expired."
                  " Please click on 'here' to receive the new token."))
        else:
            raise forms.ValidationError(
                "Invalid Token. Enter a valid token.")


class AccountStatementForm(forms.Form):
    password = forms.CharField(max_length=32, widget=forms.PasswordInput)


class AccountTopUpForm(forms.Form):
    phone = forms.RegexField(regex=r'^(07)[0-9]{8}$',
                             error_messages={'invalid': messages.phone_format},
                             required=False)
    amount = forms.DecimalField(max_digits=10, decimal_places=3)

    def __init__(self, user, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['phone'].initial = user.phone