# Generated by Django 2.0.8 on 2020-05-25 10:49

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0002_auto_20200525_1043'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='historicaluser',
            name='survey_requested',
        ),
        migrations.RemoveField(
            model_name='user',
            name='survey_requested',
        ),
    ]
