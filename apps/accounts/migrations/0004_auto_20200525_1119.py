# Generated by Django 2.0.8 on 2020-05-25 11:19

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0003_auto_20200525_1049'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='historicaluser',
            name='ussd',
        ),
        migrations.RemoveField(
            model_name='user',
            name='ussd',
        ),
    ]
