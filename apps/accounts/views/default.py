import base64
import datetime
import json
import logging
import os

import allauth.account.views as allauth_views
import requests
from allauth.account import signals
from allauth.account.forms import LoginForm
from allauth.account.models import EmailAddress
from allauth.account.utils import send_email_confirmation
from allauth.account.views import ConfirmEmailView, LoginView
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.api import get_messages
from django.db import transaction
from django.http import HttpResponse
from django.shortcuts import redirect, render
from django.template.loader import render_to_string
from django.urls import reverse, reverse_lazy
from django.utils.html import format_html
from django.utils.translation import ugettext_lazy as _
from django.views.generic import (CreateView, FormView, TemplateView,
                                  UpdateView, View)
from formtools.wizard.views import SessionWizardView
from rolepermissions.mixins import HasRoleMixin
from rolepermissions.roles import (assign_role, clear_roles, get_user_roles,
                                   remove_role)

from apps.consumers.models import Consumer
from apps.core.mixins import ExtraActionMixin
from apps.core.views.mixins import SuperUserCheckMixin
from apps.distribution.models import Retailer
from apps.employees.models import Employee
from apps.inventory.models import StockMovement, CurrentStock
from apps.orders import choices
from apps.orders.models import Order
from apps.tenants.choices import TENANT_TYPE
from apps.tenants.models import Tenant
from apps.sales.models import Sale

from .. import forms
from ..choices import PROFILE_TYPES
from ..exceptions import SMSException
from ..messages import account_inactive, unverified_identifier
from ..models import Invitations, OTPVerificationDevice, User
from ..utils import (Wallet, get_roles_tuple, send_email, send_sms,
                     to_snake_case)

logger = logging.getLogger(__name__)

FORMS = [("register_basic", forms.RegisterBasicInfoForm),
         ("register_final", forms.RegisterOtherInfoForm)]

TEMPLATES = {"register_basic": "accounts/register_basic.html",
             "register_final": "accounts/register_final.html", }


class AccountRegisterWizard(SessionWizardView):
    """Wizard View for consumer's account registration.

    Note:
        JamboPay wallet is created upon registration, if phone exists
        the password is reset with the inputted password.

    """

    def get_template_names(self):
        return [TEMPLATES[self.steps.current]]

    @transaction.atomic
    def done(self, form, **kwargs):
        """Creates account on JP and sends confirmation email
        and sms is email and phone exists respectively.

        Args:
            form: RegisterForm.

        Returns:
            Redirect to verify phone page if successful.

        """
        data = self.get_all_cleaned_data()
        password = data.pop('password')
        phone = '254' + data['phone'][-9:]
        data['phone'] = phone
        user = User.objects.create_user(
            username=phone, password=password, **data)

        assign_role(user, 'consumer')
        survey_requested = data['survey_requested']
        Consumer.objects.create_consumer(
            user, user.get_full_name(), survey_requested=survey_requested)

        message = "Registration was successful. Welcome to JamboPay Gas Retailing System."

        send_sms(phone_number=user.phone, sms_body=message)

        if user.phone:
            device = OTPVerificationDevice.objects.create(
                user=user, unverified_phone=user.phone)
            device.generate_challenge()

            message = _("Verification token sent to {phone}")
            message = message.format(phone=user.phone)
            messages.add_message(self.request, messages.INFO, message)

        if user.email:
            send_email_confirmation(self.request, user)

        self.request.session['phone_verify_id'] = user.id

        message = _("We have created your account. You should have "
                    "received an email or a text to verify your "
                    "account.")
        messages.add_message(self.request, messages.SUCCESS, message)
        return redirect(reverse_lazy('account:verify_phone'))


class PasswordChangeView(LoginRequiredMixin,
                         SuperUserCheckMixin,
                         allauth_views.PasswordChangeView):
    success_url = reverse_lazy('account:profile')
    form_class = forms.ChangePasswordForm


class PasswordResetView(SuperUserCheckMixin,
                        allauth_views.PasswordResetView):
    form_class = forms.ResetPasswordForm

    def form_valid(self, form):
        try:
            with transaction.atomic():
                return super().form_valid(form)
        except SMSException as e:
            error = e.msg
            if error:
                form.add_error('phone', error)
                return self.form_invalid(form)
            else:
                raise


class AccountLogin(LoginView):
    success_url = reverse_lazy('account:dashboard')

    def post(self, request, *args, **kwargs):
        """
        Handles POST requests, instantiating a form instance with the passed
        POST variables and then checked for validity.
        """
        login_form = self.get_form()
        updated_request = request.POST.copy()
        login = updated_request['login']
        if login.isdigit():
            login = '254' + login[-9:]
        updated_request.update({'login': login})
        login_form = LoginForm(updated_request)
        if login_form.is_valid():
            return self.form_valid(login_form)
        else:
            return self.form_invalid(login_form)

    def form_valid(self, form):
        login = form.cleaned_data['login']
        user = form.user

        next_url = self.request.POST.get('next', None)
        if user.is_superuser:
            if next_url == '/docs/':
                super().form_valid(form)
                return redirect(next_url)
            else:
                messages.add_message(
                    self.request, messages.ERROR, "User not allowed access")
                return redirect(reverse_lazy('account:login'))
        if (login == user.username and
                not user.phone_verified and
                not user.email_verified):
            user.is_active = False
            user.save()
            messages.add_message(
                self.request, messages.ERROR, account_inactive)
            return redirect(reverse_lazy('account:resend_token'))

        if(login == user.email and not user.email_verified or
                login == user.phone and not user.phone_verified):
            messages.add_message(
                self.request, messages.ERROR, unverified_identifier)
            return redirect(reverse_lazy('account:resend_token'))
        if next_url:
            super().form_valid(form)
            return redirect(next_url)
        else:
            return super().form_valid(form)


class AccountProfile(LoginRequiredMixin, UpdateView):
    model = User
    form_class = forms.ProfileForm
    template_name = 'account/profile.html'
    success_url = reverse_lazy('retailers')

    def get_object(self, *args, **kwargs):
        self.instance_phone = self.request.user.phone
        return self.request.user

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)

        emails_to_verify = EmailAddress.objects.filter(
            user=self.object, verified=False).exists()
        phones_to_verify = OTPVerificationDevice.objects.filter(
            user=self.object, verified=False).exists()

        context['emails_to_verify'] = emails_to_verify
        context['phones_to_verify'] = phones_to_verify
        return context

    def get_form_kwargs(self, *args, **kwargs):
        form_kwargs = super().get_form_kwargs(*args, **kwargs)
        form_kwargs['request'] = self.request
        return form_kwargs

    def form_valid(self, form):
        with transaction.atomic():
            phone = form.data.get('phone')
            messages.add_message(
                self.request, messages.SUCCESS,
                _("Successfully updated profile information"))

            if (phone != self.instance_phone and phone):
                message = _("Verification Token sent to {phone}")
                message = message.format(phone=phone)
                messages.add_message(self.request, messages.INFO, message)
                self.request.session['phone_verify_id'] = self.object.id
                self.success_url = reverse_lazy('account:verify_phone')

            return super().form_valid(form)

    def form_invalid(self, form):
        messages.add_message(self.request, messages.ERROR,
                             _("Failed to update profile information"))
        return super().form_invalid(form)


class PasswordResetDoneView(FormView, allauth_views.PasswordResetDoneView):
    """ If the user opts to reset password with phone, this view will display
     a form to verify the password reset token. """
    form_class = forms.TokenVerificationForm
    success_url = reverse_lazy('account:account_reset_password_from_phone')

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['phone'] = self.request.session.get('phone', None)
        return context

    def get_form_kwargs(self, *args, **kwargs):
        form_kwargs = super().get_form_kwargs(*args, **kwargs)
        phone = self.request.session.get('phone', None)
        try:
            form_kwargs['device'] = OTPVerificationDevice.objects.get(
                unverified_phone=phone, label='password_reset')
        except OTPVerificationDevice.DoesNotExist:
            pass
        return form_kwargs

    def form_valid(self, form):
        device = form.device
        message = _("Successfully Verified Token."
                    " You can now reset your password.")
        messages.add_message(self.request, messages.SUCCESS, message)
        self.request.session.pop('phone', None)
        self.request.session['password_reset_id'] = device.user_id
        device.delete()
        return super().form_valid(form)


class PasswordResetFromPhoneView(FormView, SuperUserCheckMixin):
    """ This view will allow user to reset password once a user has
     successfully verified the password reset token. """
    form_class = forms.ResetPasswordKeyForm
    template_name = 'account/password_reset_from_key.html'
    success_url = reverse_lazy("account:account_reset_password_from_key_done")

    def get_form_kwargs(self, *args, **kwargs):
        form_kwargs = super().get_form_kwargs(*args, **kwargs)
        try:
            user_id = self.request.session['password_reset_id']
            user = User.objects.get(id=user_id)
            form_kwargs['user'] = user
        except KeyError:
            message = _(
                "You must first verify your token before resetting password."
                " Click <a href='{url}'>here</a> to get the password reset"
                " verification token. ")
            message = format_html(message.format(
                url=reverse_lazy('account:account_reset_password')))
            messages.add_message(self.request, messages.ERROR, message)

        return form_kwargs

    def form_valid(self, form):
        form.save()
        self.request.session.pop('password_reset_id', None)
        signals.password_reset.send(sender=form.user.__class__,
                                    request=self.request,
                                    user=form.user)
        return super().form_valid(form)


class ResendTokenView(FormView):
    form_class = forms.ResendTokenForm
    template_name = 'account/resend_token_page.html'
    success_url = reverse_lazy('account:verify_phone')

    def form_valid(self, form):
        phone = form.data.get('phone')
        email = form.data.get('email')

        if phone:
            phone = '254' + phone[-9:]
            try:
                with transaction.atomic():
                    phone_device = OTPVerificationDevice.objects.get(
                        unverified_phone=phone, verified=False)
                    phone_device.generate_challenge()
                    self.request.session[
                        'phone_verify_id'] = phone_device.user_id

            except SMSException as e:
                error = e.msg
                if error:
                    form.add_error('phone', error)
                    return self.form_invalid(form)
                else:
                    raise

            except OTPVerificationDevice.DoesNotExist:
                pass

            message = _(
                "Your phone number has been submitted."
                " If it matches your account on Gas Yetu, you will"
                " receive a verification token to confirm your phone.")
            messages.add_message(self.request, messages.SUCCESS, message)

        if email:
            email = email.casefold()
            try:
                email_device = EmailAddress.objects.get(
                    email=email, verified=False)
                user = email_device.user
                if not user.email_verified:
                    user.email = email
                send_email_confirmation(self.request, user)
                self.request.session['phone_verify_id'] = email_device.user.id
            except EmailAddress.DoesNotExist:
                pass

            get_messages(self.request)._queued_messages = []

            message = _(
                "Your email address has been submitted."
                " If it matches your account on Gas Yetu, you will"
                " receive a verification link to confirm your email.")
            messages.add_message(self.request, messages.SUCCESS, message)

        return super().form_valid(form)


class ConfirmPhone(FormView):
    template_name = 'account/account_verification.html'
    form_class = forms.TokenVerificationForm
    success_url = reverse_lazy('account:login')

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        user_id = self.request.session.get('phone_verify_id', None)

        emails_to_verify = EmailAddress.objects.filter(
            user_id=user_id, verified=False).exists()
        phones_to_verify = OTPVerificationDevice.objects.filter(
            user_id=user_id, label='phone_verify').exists()
        context['phone'] = phones_to_verify
        context['email'] = emails_to_verify

        return context

    def get_form_kwargs(self, *args, **kwargs):
        form_kwargs = super().get_form_kwargs(*args, **kwargs)
        user_id = self.request.session.get('phone_verify_id', None)
        try:
            form_kwargs['device'] = OTPVerificationDevice.objects.get(
                user_id=user_id, label='phone_verify')
        except OTPVerificationDevice.DoesNotExist:
            pass
        return form_kwargs

    def form_valid(self, form):
        device = form.device
        user = device.user
        if user.phone != device.unverified_phone:
            user.phone = device.unverified_phone
        user.phone_verified = True
        user.is_active = True
        user.save()
        device.delete()
        message = _("Successfully verified {phone}")
        message = message.format(phone=user.phone)
        messages.add_message(self.request, messages.SUCCESS, message)
        self.request.session.pop('phone_verify_id', None)
        return super().form_valid(form)


class ConfirmEmail(ConfirmEmailView):

    def post(self, *args, **kwargs):
        response = super().post(*args, **kwargs)

        user = self.get_object().email_address.user
        user.email = self.get_object().email_address.email
        user.email_verified = True
        user.is_active = True
        user.save()

        return response


@login_required
def account_statement(request):
    statements = []
    balance = ""
    statement_form = forms.AccountStatementForm()
    if request.method == 'POST':
        statement_form = forms.AccountStatementForm(request.POST)
        if statement_form.is_valid():
            raw_password = statement_form.cleaned_data['password']
            w = Wallet(user=request.user, password=raw_password)
            if request.user.profile.tenant.tenant_type == TENANT_TYPE.Consumer:
                result = w.get_wallet_statement()
                if result['code'] == 0:
                    statements = result['data']['statement']
                    balance = result['data']['balance']
                else:
                    messages.add_message(
                        request, messages.ERROR, result['data'])
            elif request.user.profile.tenant.tenant_type in [TENANT_TYPE.Retailer]:
                # TODO: Quick hack, replace with datepicker
                one_year_ago = datetime.datetime.now() - datetime.timedelta(days=3 * 365)
                result = w.get_agent_statement(agent_code=request.tenant_agent_code,
                                               agent_key=request.tenant_agent_key,
                                               start_date=one_year_ago,
                                               end_date=datetime.datetime.now())
                if result['code'] == 0:
                    statements = result['data']['statement']
                    balance = result['data']['balance']
                else:
                    messages.add_message(
                        request, messages.ERROR, result['data'])
    context = {
        'statements': statements,
        'balance': balance,
        'statement_form': statement_form,
        'display': 'Wallet Statement',
    }
    return render(request, 'account/statement.html', context=context)


@login_required
def dashboard(request):
    retailer_count = []
    new_order = []
    completed_order = []
    completed_orders = []
    new_orders = []

    tenant_id = request.tenant

    current_stock = sum([item.no_of for item in CurrentStock.objects.filter(tenant_id=tenant_id).all()])
    if current_stock is not None:
        total_stock = current_stock
    else:
        total_stock = 0
    if request.tenant.tenant_type == TENANT_TYPE.GasImporter:
        gas_importer = request.tenant.gas_importer
        retailers = Retailer.objects.filter(enabled=True, gas_importer=gas_importer)
        retailer_count = len(retailers)

        orders = Order.objects.filter(seller=tenant_id)

        for order in orders:
            if order.order_status == choices.ORDER_STATUS_NEW:
                new_order.append(order)
            if order.order_status == choices.ORDER_STATUS_COMPLETED:
                completed_order.append(order.total_purchased_cylinders)
        new_orders = len(new_order)
        completed_orders = sum(completed_order)

    if request.tenant.tenant_type == TENANT_TYPE.Retailer:
        sales = Sale.objects.filter(seller=request.tenant).count()
        sales_count = sales
        completed_orders = sales_count
    sales = Sale.objects.filter(seller=request.tenant).all()
    return render(request, 'dashboard.html', {
        "orders": new_order,
        "total_stock": total_stock,
        "completed_orders": completed_orders,
        "retailer_count": retailer_count,
        "new_order_count": new_orders,
        "sales": sales
    })


class StaffAccountRegister(CreateView):
    model = User
    form_class = forms.StaffRegisterForm
    template_name = 'accounts/staff_register.html'
    success_url = reverse_lazy('accounts:login')

    def form_valid(self, form):
        with transaction.atomic():
            user = form.save(self.request)
            invite = Invitations.objects.filter(email=user.email)
            invite.update(accepted=True)

            role = self.request.session['invite_roles']
            assign_role(user, to_snake_case(role))
            user.profile_type = PROFILE_TYPES.Employee
            user.save()

            tenant = Tenant.objects.filter(
                tenant_type=TENANT_TYPE.GasImporter).first()
            Employee.objects.create(
                tenant=tenant,
                user=user,
            )

            message = _("Account successfully created, proceed to login.")
            messages.add_message(
                self.request, messages.SUCCESS, message)

            del self.request.session['invite_email']
            del self.request.session['invite_roles']
        return super().form_valid(form)

    def get_initial(self):
        initial = super(StaffAccountRegister, self).get_initial()
        initial['email'] = self.request.session['invite_email']
        initial['roles'] = self.request.session['invite_roles']
        return initial

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        return context

    def dispatch(self, request, *args, **kwargs):
        """
        Checks whether we are redirected from accept invite page
        """
        if 'invite_email' not in request.session:
            message = "Sorry, this page doesn't exist"
            return render(request, context={'message': message}, template_name='accounts/invalid.html')
        return super().dispatch(request, *args, **kwargs)


class AcceptInvite(View):
    def get(self, request, uidb64):
        try:
            data = self.decode_data(uidb64)
            email = data['email']
            role = data['roles']
            self.request.session[
                'invite_email'] = email
            self.request.session[
                'invite_roles'] = role
            return redirect(reverse_lazy('accounts:staff_register'))
        except (TypeError, ValueError, OverflowError, KeyError):
            message = "Sorry, the link you clicked is invalid 😕"
            return render(request, context={'message': message}, template_name='accounts/invalid.html')

        if not Invitations.objects.filter(email=email, accepted=False):
            message = "Sorry, the link you clicked has is invalid or has expired 😕"
            return render(request, context={'message': message}, template_name='accounts/invalid.html')

    def decode_data(self, data):
        return json.loads(base64.urlsafe_b64decode(data))


class InviteUser(HasRoleMixin, FormView):
    template_name = 'accounts/invite_user.html'
    form_class = forms.UserInviteForm
    success_url = reverse_lazy('account:invite_user')
    allowed_roles = ['system_admin']

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['display'] = 'Invite User'
        return context

    def form_valid(self, form):
        email = form.cleaned_data['email']
        encoded_data = self.encode_data(form.cleaned_data)

        invite_url = reverse('account:accept_invite',
                             kwargs={'uidb64': encoded_data})
        invite_url = self.request.build_absolute_uri(invite_url)

        """
        TODO: Write reusable email template
        """

        msg_body = render_to_string(
            'accounts/email/invitation.html', {'link': invite_url})
        send_email(
            _("Invitation to Gas Yetu"),
            msg_body,
            email)

        Invitations.objects.create(email=email)
        message = _(f'Invite sent to {email}')
        messages.add_message(self.request, messages.SUCCESS, message)
        return super().form_valid(form)

    def encode_data(self, data):
        """
        Encode data for use in url
        """
        return base64.urlsafe_b64encode(json.dumps(data).encode()).decode()


class ManageRoles(HasRoleMixin, LoginRequiredMixin, ExtraActionMixin, TemplateView):
    template_name = 'accounts/manage_roles.html'
    action = None
    allowed_roles = ['system_admin']

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)

        user_roles = []
        for user in User.objects.filter(employee__tenant__tenant_type=TENANT_TYPE.GasImporter):
            user_roles.append({
                'user': user,
                'roles': get_user_roles(user)
            })

        context.update({
            'user_roles': user_roles,
            'all_roles': get_roles_tuple(),
            'display':  'Manage Roles'
        })
        return render(self.request, self.template_name, context)

    def get_roles_data(self, request, *args, **kwargs):
        user_id = request.GET.get('user_id')
        roles = []
        if user_id:
            user = User.objects.get(pk=user_id)
            roles = [{'name': role.get_name().replace('_', ' ').title(
            ), 'value': role.get_name()} for role in get_user_roles(user)]

        response = {
            'roles': roles
        }

        return HttpResponse(json.dumps(response), content_type='application/json')

    def add_user_role(self, request, *args, **kwargs):
        user_id = request.GET.get('user_id')
        role = request.GET.get('role')
        try:
            if user_id and role:
                user = User.objects.get(pk=user_id)
                assign_role(user, to_snake_case(role))
                user.save()
                roles = [{'name': role.get_name().replace('_', ' ').title(
                ), 'value': role.get_name()} for role in get_user_roles(user)]

                response = {
                    'status': 'ok',
                    'roles': roles
                }
            else:
                response = {
                    'status': 'error'
                }
        except Exception as e:
            response = {
                'status': 'error'
            }
        return HttpResponse(json.dumps(response), content_type='application/json')

    def remove_user_role(self, request, *args, **kwargs):
        user_id = request.GET.get('user_id')
        role = request.GET.get('role')

        try:
            if user_id and role:
                user = User.objects.get(pk=user_id)
                remove_role(user, role)
                user.save()
                roles = [{'name': role.get_name().replace('_', ' ').title(
                ), 'value': role.get_name()} for role in get_user_roles(user)]

                response = {
                    'status': 'ok',
                    'roles': roles
                }
            else:
                response = {
                    'status': 'error'
                }
        except Exception as e:
            response = {
                'status': 'error'
            }

        return HttpResponse(json.dumps(response), content_type='application/json')

    def remove_all_user_role(self, request, *args, **kwargs):
        user_id = request.GET.get('user_id')

        try:
            if user_id:
                user = User.objects.get(pk=user_id)
                clear_roles(user)
                user.save()
                roles = [{'name': role.get_name().replace('_', ' ').title(
                ), 'value': role.get_name()} for role in get_user_roles(user)]

                response = {
                    'status': 'ok',
                    'roles': roles
                }
            else:
                response = {
                    'status': 'error'
                }
        except Exception as e:
            response = {
                'status': 'error'
            }

        return HttpResponse(json.dumps(response), content_type='application/json')


class AccountTopUp(HasRoleMixin, FormView):
    template_name = 'accounts/topup.html'
    form_class = forms.AccountTopUpForm
    success_url = reverse_lazy('account:account_topup')
    allowed_roles = ['consumer', 'beneficiary']

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['user'] = self.request.user
        return kwargs

    def form_valid(self, form):
        phone = '254' + form.cleaned_data['phone'][-9:]
        amount = form.cleaned_data['amount']

        _datetime = datetime.datetime.now()
        time_stamp = f'{_datetime:%Y%m%d%H%M%S}'
        key_sting = os.getenv("MPESA_PAYBILL") + \
            os.getenv("MPESA_DEV_KEY") + time_stamp
        auth_key = base64.b64encode(key_sting.encode('utf8'))
        account = 'EMB'
        request = {
            "DevKey": os.getenv("MPESA_DEV_KEY"),
            "Timestamp": time_stamp,
            "Paybill": os.getenv("MPESA_PAYBILL"),
            "PhoneNumber": phone,
            "Amount": amount,
            "Account": account
        }
        headers = {"Content-Type": "application/json",
                   "Authorization": auth_key}

        r = requests.post(os.getenv("MPESA_STK_URL"),
                          headers=headers, json=request)
        if r.status_code == requests.codes.ok or r.status_code == requests.codes.created:
            messages.add_message(self.request, messages.SUCCESS,
                                 "STK Sent. Enter MPESA PIN on your mobile handset!")
            return super().form_valid(form)
        else:
            r = json.dumps(r.json())
            data = json.loads(r)
            messages.add_message(self.request, messages.ERROR,
                                 "STK sending failed. Try again later!")
            return super().form_invalid(form)

        return super().form_valid(form)


def send_contact_email(request):
    """
    send email from the contact form
    """
    try: 
        email = request.POST['email']
        subject = request.POST['subject']
        msg_body = render_to_string(
            'accounts/email/contact_form.html', 
            {
                'name': request.POST['name'],
                'phone': request.POST['phone'],
                'subject':request.POST['subject'],
                'message':request.POST['message']
            }
        )
        send_email(subject, msg_body, email)
        response = {'status': 'Okay', 'message':'Email was sent successfully'}
        return HttpResponse(json.dumps(response), content_type='application/json')
    except Exception as e:
        response = {'status': 'Not Okay', 'message': str(e)}
        return HttpResponse(json.dumps(response), content_type='application/json')
