import base64
import logging
import os
from datetime import datetime

import requests
from allauth.account.signals import password_changed
from allauth.account.utils import send_email_confirmation
from django.db import IntegrityError, transaction
from django.utils.translation import ugettext as _
from djoser import signals
from djoser import views as djoser_views
from rest_framework import permissions, status, viewsets
from rest_framework.generics import GenericAPIView
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response
from rest_framework.serializers import ValidationError
from rest_framework.views import APIView
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from rest_framework_jwt.settings import api_settings
from rolepermissions.checkers import has_permission
from rolepermissions.roles import assign_role

from apps.consumers.models import Consumer
from apps.core.viewsets import ReadOnlyViewSet
from apps.tenants.models import TENANT_TYPE

from .. import exceptions, messages, serializers
from ..models import OTPVerificationDevice, User
from ..serializers import (AccountLoginSerializer, AccountStatementSerializer,
                           MpesaDepositSerializer, MpesaSTKPushSerializer,
                           PhoneVerificationSerializer, RegistrationSerializer)
from ..utils import Wallet, generate_jwt, send_sms


class AccountRegister(djoser_views.UserViewSet):
    serializer_class = RegistrationSerializer

    def create(self, request, *args, **kwargs):
        with transaction.atomic():
            return super().create(request, *args, **kwargs)

    def perform_create(self, serializer):
        user = serializer.save()
        signals.user_registered.send(sender=self.__class__, user=user,
                                     request=self.request)

        survey_requested = serializer.validated_data.get(
            'survey_requested', True)
        Consumer.objects.create_consumer(
            user, user.get_full_name(), survey_requested=survey_requested)

        ussd = serializer.validated_data.get('ussd', False)
        if survey_requested:
            message = "Registration was successful for Gas Yetu. Please await visit by one our officials for surveying."
        else:
            message = "Registration was successful.We have noted that you use Supa Gas,please proceed using Supa Gas."

        from_enumerator = serializer.validated_data.get(
            'from_enumerator', False)

        send_sms(phone_number=user.phone, sms_body=message)

        assign_role(user, 'consumer')
        if user.phone:
            if not ussd:
                if from_enumerator:
                    self.send_otp(user, verified=True)
                else:
                    self.send_otp(user)
            else:
                self.send_otp(user, verified=True)

    def send_otp(self, user, verified=False):
        verification_device = OTPVerificationDevice.objects.create(
            unverified_phone=user.phone,
            user=user,
            verified=verified)
        if not verified:
            verification_device.generate_challenge()
        user.is_active = verified
        user.phone_verified = verified
        user.save()


class AccountLogin(djoser_views.TokenCreateView):
    serializer_class = AccountLoginSerializer

    def post(self, request):
        serializer = AccountLoginSerializer(data=request.data)
        try:
            serializer.is_valid(raise_exception=True)
            token = generate_jwt(serializer.user)
            return Response(
                data={'token': token},
                status=status.HTTP_200_OK,
            )

        except ValidationError:
            error = serializer.errors

        except exceptions.AccountInactiveError as e:
            user = serializer.user
            user.is_active = False
            user.save()
            error = e.msg

        except exceptions.EmailNotVerifiedError as e:
            user = serializer.user
            error = e.msg
            # send_email_confirmation(self.request._request, user)

        except exceptions.PhoneNotVerifiedError as e:
            user = serializer.user
            error = e.msg
            device = user.otpverificationdevice_set.get(label='phone_verify')
            device.generate_challenge()

        return Response(
            data={'detail': error},
            status=status.HTTP_401_UNAUTHORIZED)


class AccountUser(djoser_views.UserViewSet):
    serializer_class = serializers.UserSerializer

    def update(self, request, *args, **kwargs):
        with transaction.atomic():
            return super().update(request, *args, **kwargs)

    def perform_update(self, serializer):
        instance = self.get_object()
        current_email, current_phone = instance.email, instance.phone
        new_email = serializer.validated_data.get('email', instance.email)
        new_phone = serializer.validated_data.get('phone', instance.phone)
        user = serializer.save()

        if current_email != new_email:
            if new_email:
                # send_email_confirmation(self.request._request, user)
                if current_email:
                    user.email = current_email
                    # send_email_update_notification(current_email)

        if current_phone != new_phone:
            instance.otpverificationdevice.delete()

            if new_phone:
                device = OTPVerificationDevice.objects.create(
                    user=instance,
                    unverified_phone=new_phone)
                device.generate_challenge()
                if current_phone:
                    user.phone = current_phone
                    send_sms(current_phone, messages.phone_change)

        user.save()


class ConfirmPhoneView(GenericAPIView):
    permission_classes = (AllowAny,)
    serializer_class = PhoneVerificationSerializer

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        try:
            serializer.is_valid(raise_exception=True)
        except ValidationError:
            return Response(
                data=serializer.errors,
                status=status.HTTP_400_BAD_REQUEST,
            )
        return Response(
            data={'detail': 'Phone successfully verified.'},
            status=status.HTTP_200_OK)


class SetPasswordView(djoser_views.UserViewSet):

    def _action(self, serializer):
        response = super()._action(serializer)
        password_changed.send(sender=self.request.user.__class__,
                              request=self.request._request,
                              user=self.request.user)
        return response


class AccountStatement(APIView):
    authentication_classes = (JSONWebTokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    renderer_classes = (JSONRenderer, )

    def get(self, request, format=None):
        """
        Returns the account balance and statement
        """
        password = request.META.get('HTTP_PASSWORD')
        if not password:
            content = {'error': 'Password is required'}
            return Response(content, status=status.HTTP_401_UNAUTHORIZED)
        wallet = Wallet(user=request.user,
                        password=password)
        if request.tenant.tenant_type in [TENANT_TYPE.Retailer, TENANT_TYPE.Distributor]:
            one_year_ago = datetime.datetime.now() - datetime.timedelta(days=3 * 365)
            result = w.get_agent_statement(agent_code=request.tenant_agent_code,
                                           agent_key=request.tenant_agent_key,
                                           start_date=one_year_ago,
                                           end_date=datetime.datetime.now())
        else:
            result = wallet.get_wallet_statement()
        if result['code'] not in [0, 200]:
            content = {'error': result['data']}
            return Response(content, status=status.HTTP_400_BAD_REQUEST)

        return Response(result['data'], status=status.HTTP_200_OK)


class MpesaDepositView(GenericAPIView):
    authentication_classes = (JSONWebTokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = MpesaDepositSerializer

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        _status = status.HTTP_400_BAD_REQUEST
        response = {'code': 1, 'data': 'failed request'}
        if serializer.is_valid(raise_exception=True):
            print(serializer.validated_data)
            amount = serializer.validated_data['amount']
            phone = serializer.validated_data['phone']
            mpesa_ref = serializer.validated_data['mpesareceipt']

            try:
                wallet = Wallet()
                response = wallet.make_deposit(
                    ref_no=mpesa_ref, amount=str(amount), phone=phone)
            except Exception as e:
                logging.exception(e)
            print(response)
            if 'code' in response:
                if response['code'] == 0:
                    _status = status.HTTP_200_OK

        return Response(response, status=_status)


class MpesaSTKPushView(GenericAPIView):
    authentication_classes = (JSONWebTokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = MpesaSTKPushSerializer

    def post(self, request):
        serializer = self.serializer_class(data=request.data)

        if serializer.is_valid(raise_exception=True):

            phone = request.user.phone
            phone = phone.split('+')[1]
            _datetime = datetime.now()
            time_stamp = f'{_datetime:%Y%m%d%H%M%S}'
            key_sting = os.getenv("MPESA_PAYBILL") + \
                os.getenv("MPESA_DEV_KEY")+time_stamp
            auth_key = base64.b64encode(key_sting.encode('utf8'))
            account = serializer.validated_data.get('account', None)
            if not account:
                account = 'EMBJF832F'
            request = {
                "DevKey": os.getenv("MPESA_DEV_KEY"),
                "Timestamp": time_stamp,
                "Paybill": os.getenv("MPESA_PAYBILL"),
                "PhoneNumber": phone,
                "Amount": serializer.validated_data.get('amount', 0),
                "Account": account
            }
            headers = {"Content-Type": "application/json",
                       "Authorization": auth_key}

            r = requests.post(os.getenv("MPESA_STK_URL"),
                              headers=headers, json=request)
            if r.status_code == requests.codes.ok or r.status_code == requests.codes.created:
                return Response(
                    data={'detail': 'Mpesa STK successfully sent.'},
                    status=status.HTTP_200_OK)
            else:
                return Response(
                    data={'detail': 'Sending Mpesa STK has failed.'},
                    status=status.HTTP_424_FAILED_DEPENDENCY)
        else:
            return Response(
                data={'detail': 'Sending Mpesa STK has failed.'},
                status=status.HTTP_400_BAD_REQUEST)
