from django.conf.urls import url, include
from rest_framework_nested import routers

from . import api

router = routers.SimpleRouter()
router.register(r'listings', api.ListingViewset, base_name='listing')

urlpatterns = [
    url(r'^', include(router.urls)),
]
