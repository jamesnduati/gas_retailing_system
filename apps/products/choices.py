from django.utils.translation import ugettext_lazy as _

from model_utils import Choices

PURCHASE_SERVICE = 'purchase'
REFILL_SERVICE = 'refill'

SERVICE_CHOICES = Choices(
    (PURCHASE_SERVICE, 'Purchase', _('Purchase Service')),
    (REFILL_SERVICE, 'Refill', _('Refill Service'))
)

BILLING_MODE = Choices(
    (1, 'Percentage', _('Percentage')),
    (2, 'Fixed', _('Fixed'))
)