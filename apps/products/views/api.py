from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated
from rolepermissions.mixins import HasRoleMixin

from apps.accounts.choices import BENEFICIARY, CONSUMER, EMPLOYEE
from apps.distribution.models import Distributor, Retailer
from apps.products.models import Brand, Cylinder, Discount, Listing, Product
from apps.products.serializers import (BrandSerializer, CylinderSerializer,
                                       DiscountSerializer, ListingSerializer,
                                       ProductSerializer)
from apps.tenants.choices import DISTRIBUTOR, RETAILER


class ProductViewSet(HasRoleMixin, viewsets.ModelViewSet):
    """
    Manage Products
    """
    queryset = Product.objects.filter()
    serializer_class = ProductSerializer
    permission_classes = (IsAuthenticated,)
    filter_fields = ('id', 'cylinder', 'brand')
    allowed_roles = ['distributor_admin',
                     'retailer_admin', 'beneficiary', 'consumer']


class ListingViewSet(HasRoleMixin, viewsets.ModelViewSet):
    """
    Manage Listings
    """
    queryset = Listing.objects.all()
    serializer_class = ListingSerializer
    permission_classes = (IsAuthenticated,)
    filter_fields = ('id', 'tenant')
    allowed_roles = ['distributor_admin',
                     'retailer_admin', 'beneficiary', 'consumer']

    def get_queryset(self):

        queryset = super(ListingViewSet, self).get_queryset()

        if self.request.user.profile_type == EMPLOYEE:
            employee = self.request.user.employee

            if employee.tenant.tenant_type == RETAILER:
                tenant = Retailer.objects.get(
                    retailer_tenant=employee.tenant).distributor.distributor_tenant
                queryset = queryset.filter(tenant=tenant)
                return queryset

            elif employee.tenant.tenant_type == DISTRIBUTOR:
                tenant = Distributor.objects.get(
                    distributor_tenant=employee.tenant).gas_importer.gas_importer_tenant
                queryset = queryset.filter(tenant=tenant)
                return queryset

        elif self.request.user.profile_type == CONSUMER:
            queryset = queryset.filter(
                for_consumer=True, product__gas_yetu=False)

        elif self.request.user.profile_type == BENEFICIARY:
            queryset = queryset.filter(
                for_consumer=True)

            if self.request.user.profile.has_benefited:
                queryset = queryset.exclude(product__gas_yetu=True)

        else:
            return queryset.none()

        agent_number = self.request.query_params.get('agent_number')

        if agent_number:
            distributor = Distributor.objects.filter(enabled=True,
                                                     agent_number=agent_number).first()

            if distributor:
                return queryset.filter(tenant=distributor.distributor_tenant)
            else:
                retailer = Retailer.objects.filter(
                    enabled=True,
                    agent_number=agent_number).first()
                if retailer:
                    return queryset.filter(tenant=retailer.retailer_tenant)
        else:
            return queryset

        return queryset.none()


class BrandViewSet(HasRoleMixin, viewsets.ModelViewSet):
    """
    Manage Brands
    """
    queryset = Brand.objects.filter()
    serializer_class = BrandSerializer
    permission_classes = (IsAuthenticated,)
    filter_fields = ('id', 'title')
    allowed_roles = ['distributor_admin',
                     'retailer_admin', 'beneficiary', 'consumer']


class CylindersViewSet(HasRoleMixin, viewsets.ModelViewSet):
    """
    Manage stocks
    """
    queryset = Cylinder.objects.filter()
    serializer_class = CylinderSerializer
    permission_classes = (IsAuthenticated,)
    filter_fields = ('id', 'title')
    allowed_roles = ['distributor_admin',
                     'retailer_admin', 'beneficiary', 'consumer']


class DiscountViewSet(HasRoleMixin, viewsets.ModelViewSet):
    """
    Manage Discount
    """
    queryset = Discount.objects.filter()
    serializer_class = DiscountSerializer
    permission_classes = (IsAuthenticated,)
    filter_fields = ('id', 'tenant', 'merchant', 'product', 'service', 'title',
                     'billing_mode', 'item_count', 'discount_rate',
                     'discount_type', 'start_date', 'end_date')
    allowed_roles = ['distributor_admin',
                     'retailer_admin', 'beneficiary', 'consumer']
