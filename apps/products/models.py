from django.db import models

from apps.products.choices import BILLING_MODE, SERVICE_CHOICES
from apps.tenants.models import TenantBaseModel, Tenant, BaseModel


class Cylinder(BaseModel):
    """
    List of Cylinder Sizes. E.g. 13 KG, 6Kg
    """
    title = models.CharField(max_length=100, unique=True)

    class Meta:
        unique_together = ('title',)

    def __str__(self):
        return self.title


class Brand(BaseModel):
    """
    List of Brands
    """
    title = models.CharField(max_length=100, unique=True)
    tenant = models.ForeignKey(
        Tenant, on_delete=models.CASCADE, related_name='brand_tenant', null=True, blank=True)

    class Meta:
        unique_together = (('title'),)

    def save(self, *args, **kwargs):
        self.title = self.title.upper()
        self.tenant = self.tenant
        return super().save(*args, **kwargs)

    def __str__(self):
        return self.title


class Product(BaseModel):
    """
    Product/Gas offered in the system. E.g. Gas-13KG, Gas-6KG
    """
    brand = models.ForeignKey(Brand, on_delete=models.CASCADE)
    cylinder = models.ForeignKey(Cylinder, on_delete=models.CASCADE)

    class Meta:
        unique_together = ('brand', 'cylinder',)

    @property
    def title(self):
        return f'{self.brand}-{self.cylinder}'

    def __str__(self):
        return self.title


class Listing(TenantBaseModel):
    """
    Listings offered by a company/tenant
    """
    service = models.CharField(choices=SERVICE_CHOICES, max_length=32)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    list_price = models.DecimalField(
        max_digits=10, decimal_places=2, help_text='List Price')
    is_active = models.BooleanField(default=True)

    class Meta:
        unique_together = (('tenant', 'service', 'product'),)

    def __str__(self):
        return '{} - {} - {} - {}'.format(self.tenant.title, self.service, self.product, self.list_price)


class Discount(TenantBaseModel):
    """
    Details of discounts offered
    """
    merchant = models.ForeignKey(
        Tenant, on_delete=models.CASCADE, related_name='merchant_discount_rule')
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    listing = models.ForeignKey(Listing, on_delete=models.CASCADE)
    title = models.CharField(max_length=100)
    billing_mode = models.PositiveSmallIntegerField(choices=BILLING_MODE)
    item_count = models.PositiveSmallIntegerField(null=True, blank=True,
                                                  help_text='value to be checked so as to qualify for the discount '
                                                            'E.g. 1 cylinder')
    discount_rate = models.DecimalField(max_digits=10, decimal_places=3,
                                        help_text='Percentage Value or Fixed amount value')
    discount_type = models.PositiveSmallIntegerField()
    start_date = models.DateField()
    end_date = models.DateField(null=True, blank=True)
    is_active = models.BooleanField(default=True)

    class Meta:
        unique_together = (('tenant', 'title'),
                           ('merchant', 'discount_type', 'billing_mode', 'item_count'))

    def __str__(self):
        return '{} ({} - {})'.format(self.title, self.discount_type, self.billing_mode)
