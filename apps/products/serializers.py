from rest_framework import serializers

from . import models


class CylinderSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Cylinder
        fields = ('title',)
        read_only = ('id',)


class BrandSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Brand
        fields = ('title',)
        read_only = ('id',)


class ProductSerializer(serializers.ModelSerializer):
    brand = BrandSerializer()
    cylinder = CylinderSerializer()
    title = models.Product.title

    class Meta:
        model = models.Product
        fields = ('brand', 'cylinder', 'title')
        read_only = ('id',)


class ListingSerializer(serializers.ModelSerializer):
    product = ProductSerializer()

    class Meta:
        model = models.Listing
        fields = ('id', 'list_price', 'service', 'product')
        read_only = ('id',)


class DiscountSerializer(serializers.ModelSerializer):
    listing = ListingSerializer()
    product = ProductSerializer()

    class Meta:
        model = models.Listing
        fields = ('title', 'listing', 'item_count', 'discount_rate')
        read_only = ('id',)
