import os
import requests
import string
import random
from apps.orders.models import Order


def get_voucher_number(size=6, chars=string.ascii_uppercase + string.digits):
    voucher_number = ''.join(random.choice(chars) for _ in range(size))
    order = Order.objects.filter(voucher_number=voucher_number).exists()
    if order:
        return get_voucher_number()
    return voucher_number


def process_order_express_input(order_oracle_items, customer_number, order_number):
    """
    oracle integration
    """
    url = os.getenv('ORACLE_URL')
    headers = {
	    'content-type': 'application/json',
	    'accept': 'application/json',
	    'content-language': 'en-US',
	    'authorization': os.getenv('ORACLE_PASS_KEY')
    }
    payload = {
        "PROCESS_ORDER_EXPRESS_Input": {
            "@xmlns": "http://xmlns.oracle.com/apps/ont/rest/ordint/process_order_express/",
            "RESTHeader": {
                "xmlns": "http://xmlns.oracle.com/apps/fnd/rest/header",
                "Responsibility": "NOC_JAMBOPAY_RESP",
                "RespApplication": "ONT",
                "SecurityGroup": "STANDARD",
                "NLSLanguage": "AMERICAN"
            },
            "InputParameters": {
                "P_HEADER_REC": {
                    "CUSTOMER_NUMBER": "26971",
                    "CUST_PO_NUMBER": "TEST",
                    "TRANSACTIONAL_CURR_CODE": "KES"
                },
                "P_LINE_TBL": {
                    "P_LINE_TBL_ITEM": [
                        {
                            "INVENTORY_ITEM": "AGO_NOC",
                            "SHIP_FROM_ORG": "530",
                            "ORDERED_QUANTITY": "6000"
                        },
                        {
                            "INVENTORY_ITEM": "PMS_NOC",
                            "SHIP_FROM_ORG": "530",
                            "ORDERED_QUANTITY": "7000"
                        }
                    ]
                }
            }
        }
    }
    payload['PROCESS_ORDER_EXPRESS_Input']['InputParameters']['P_HEADER_REC']['CUSTOMER_NUMBER'] = str(
        customer_number)
    payload['PROCESS_ORDER_EXPRESS_Input']['InputParameters']['P_HEADER_REC']['CUST_PO_NUMBER'] = str(
        order_number)
    payload['PROCESS_ORDER_EXPRESS_Input']['InputParameters']['P_LINE_TBL']['P_LINE_TBL_ITEM'] = order_oracle_items
    r = requests.post(url, json=payload, headers=headers)
    return r
