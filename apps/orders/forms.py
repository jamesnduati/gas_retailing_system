from django import forms
from django.forms import BaseFormSet, ModelForm
from django.utils.translation import ugettext_lazy as _
from extra_views import (CreateWithInlinesView, InlineFormSet,
                         ModelFormSetView, UpdateWithInlinesView)
from model_utils import Choices

from apps.accounts.choices import PROFILE_TYPES
from apps.distribution.models import Retailer
from apps.products.choices import SERVICE_CHOICES
from apps.tenants.choices import RETAILER

from .models import Order, ProductItem


class OrderForm(forms.ModelForm):
    service = forms.ChoiceField(choices=SERVICE_CHOICES)
    MODE_OF_PAYMENT_WALLET = Choices(
        ('Wallet', _('Wallet')),
    )

    MODE_OF_PAYMENT_CASH = Choices(
        ('Cash', _('Cash')),
    )

    class Meta:
        model = Order
        fields = ('seller_code', 'seller', 'service', 'mode_of_payment')
        widgets = {
            'seller': forms.HiddenInput()
        }

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user', None)
        self.disabled = kwargs.pop('disabled', False)
        self.agent_number = kwargs.pop('agent_number', None)
        super().__init__(*args, **kwargs)
        if self.agent_number:
            self.fields['seller_code'].initial = self.agent_number
        self.fields['seller_code'].required = False
        self.fields['seller_code'].disabled = self.disabled
        if self.user.profile_type == PROFILE_TYPES.Employee:
            tenant_type = self.user.profile.tenant.tenant_type
            if tenant_type == RETAILER:
                retailer = Retailer.objects.get(
                    retailer_tenant=self.user.profile.tenant)
                tenant = retailer.gas_importer.gas_importer_tenant
                self.fields['seller'].initial = tenant
                self.fields['mode_of_payment'].choices = self.MODE_OF_PAYMENT_CASH


class OrderPreviewForm(forms.Form):
    password = forms.CharField(max_length=32, widget=forms.PasswordInput)


class SerialsForm(forms.Form):
    issued_serials = forms.CharField(
        max_length=32, label="Enter Issued Serial Numbers", required=False)
    received_serials = forms.CharField(
        max_length=32, label="Enter Received Serial Numbers", required=False)

    def __init__(self, *args, service='purchase', **kwargs):
        super().__init__(*args, **kwargs)
        self.empty_permitted = False
        if service == 'purchase':
            del self.fields['received_serials']

    def clean_issued_serials(self):
        issued_serial = self.cleaned_data['issued_serials']
        if issued_serial:
            try:
                ProductItem.objects.get(serial_no=issued_serial)
            except(ProductItem.DoesNotExist):
                raise forms.ValidationError(
                    "This serial is not valid")
        return issued_serial


class BaseSerialsFormSet(BaseFormSet):
    def __init__(self, *args, **kwargs):
        super(BaseSerialsFormSet, self).__init__(*args, **kwargs)

    # TEMP FIX: should be optional
    # def clean(self):
    #     num_fields = self.total_form_count()
    #     for i in range(num_fields):
    #         if 'received_serials' in list(self.forms[i].fields.keys()):
    #             if not self.forms[i].cleaned_data.get('issued_serials') or not self.forms[i].cleaned_data.get('received_serials'):
    #                 raise forms.ValidationError(
    #                     "Please fill in all serials")
    #         else:
    #             if not self.forms[i].cleaned_data.get('issued_serials'):
    #                 raise forms.ValidationError(
    #                     "Please fill in all serials")
