# Generated by Django 2.0.8 on 2020-06-01 06:53

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0007_auto_20200529_1349'),
    ]

    operations = [
        migrations.AddField(
            model_name='productitem',
            name='product_status',
            field=models.CharField(choices=[('Empty', 'Empty'), ('Full', 'Full')], default='Full', max_length=20),
        ),
    ]
