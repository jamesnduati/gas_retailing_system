# Generated by Django 2.0.8 on 2020-06-04 06:09

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0012_remove_productmovement_sale_status'),
    ]

    operations = [
        migrations.AddField(
            model_name='productmovement',
            name='sale_status',
            field=models.CharField(choices=[('Sold', 'Sold'), ('Not_Sold', 'Not_Sold')], default='Not_Sold', max_length=30),
        ),
    ]
