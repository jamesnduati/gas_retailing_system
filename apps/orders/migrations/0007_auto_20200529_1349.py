# Generated by Django 2.0.8 on 2020-05-29 13:49

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0006_remove_productitemuser_consumer'),
    ]

    operations = [
        migrations.AlterField(
            model_name='productitemuser',
            name='product_item',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='product_item_user', to='orders.ProductItem'),
        ),
        migrations.AlterField(
            model_name='productitemuser',
            name='tenant',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='product_item_user', to='tenants.Tenant'),
        ),
    ]
