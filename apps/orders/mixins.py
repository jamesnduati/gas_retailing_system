from apps.tenants.choices import TENANT_TYPE


class OrderRecipientDetailsMixin(object):

    def get_context_data(self, **kwargs):
        order = self.object
        buyer = order.buyer
        seller = order.seller

        if buyer.tenant_type == TENANT_TYPE.Retailer:
            retailer = buyer.retailer
            self.buyer_names = retailer.trading_name
            self.buyer_phone = retailer.contact_phone
            self.buyer_email = retailer.contact_email
            self.buyer_county = retailer.sub_region.title
            self.buyer_sub_county = retailer.sub_region.title



        if seller.tenant_type == TENANT_TYPE.Retailer:
            retailer = seller.retailer
            self.seller_names = retailer.trading_name
            self.seller_phone = retailer.contact_phone
            self.seller_email = retailer.contact_email
            self.seller_county = retailer.sub_region.title
            self.seller_sub_county = retailer.sub_region.title

        elif seller.tenant_type == TENANT_TYPE.GasImporter:
            gas_importer = seller.gas_importer
            self.seller_names = gas_importer.trading_name
            self.seller_phone = gas_importer.contact_phone
            self.seller_email = gas_importer.contact_email
            self.seller_county = 'Nairobi'
            self.seller_sub_county = ''

        context = super().get_context_data(**kwargs)
        context['buyer_names'] = self.buyer_names
        context['buyer_phone'] = self.buyer_phone
        context['buyer_email'] = self.buyer_email
        context['buyer_county'] = self.buyer_county
        context['buyer_sub_county'] = self.buyer_sub_county
        context['seller_names'] = self.seller_names
        context['seller_phone'] = self.seller_phone
        context['seller_email'] = self.seller_email
        context['seller_county'] = self.seller_county
        context['seller_sub_county'] = self.seller_sub_county
        return context
