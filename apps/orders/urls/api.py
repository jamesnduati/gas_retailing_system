from django.conf.urls import include, url

from ..views import api
from rest_framework_nested import routers

router = routers.SimpleRouter()
router.register(r'orders', api.OrdersViewSet, basename='orders')
router.register(r'serials', api.SerialNumberViewSet, basename='serials')


details_router = routers.NestedSimpleRouter(router, r'orders', lookup='order')
details_router.register(r'items', api.OrderItemViewSet, basename='items')

details_router.register(r'delivery-note', api.DeliveryNoteViewSet, basename='delivery_note')


urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^', include(details_router.urls)),
]
