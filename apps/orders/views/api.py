from django.db.models import Q
from rest_framework import mixins, serializers, status, viewsets
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from rolepermissions.mixins import HasRoleMixin

from apps.accounts.utils import Wallet, send_sms
from apps.consumers.models import Beneficiary
from apps.core.exceptions import OrderEditForbidden
from apps.core.viewsets import ReadOnlyViewSet
from apps.distribution.models import Distributor, Retailer
from apps.orders.choices import (DELIVERY_NOTE_STATUS, MODE_OF_PAYMENT,
                                 ORDER_STATUS, ORDER_STATUS_APPROVED,
                                 ORDER_STATUS_CANCELLED,
                                 ORDER_STATUS_COMPLETED,
                                 ORDER_STATUS_DISPATCHED, ORDER_STATUS_DRAFT,
                                 ORDER_STATUS_NEW, ORDER_STATUS_REJECTED)
from apps.orders.models import (DeliveryNote, Order, OrderItem, Product,
                                ProductItem, ProductItemUser, ProductMovement)
from apps.orders.serializers import (DeliveryNoteSerializer,
                                     OrderFulfillmentSerializer,
                                     OrderItemSerializer,
                                     OrderReturnedSerializer, OrderSerializer,
                                     SerialNumberSerializer)
from apps.tenants.choices import CONSUMER, DISTRIBUTOR, RETAILER


class OrdersViewSet(HasRoleMixin, mixins.CreateModelMixin,
                    mixins.ListModelMixin, mixins.RetrieveModelMixin,
                    viewsets.GenericViewSet):
    permission_classes = (IsAuthenticated,)
    queryset = Order.objects.all()
    serializer_class = OrderSerializer
    filter_fields = ('voucher_number', 'order_status',)
    allowed_roles = ['distributor_admin',
                     'retailer_admin', 'beneficiary', 'consumer']

    def get_queryset(self):
        queryset = super(OrdersViewSet, self).get_queryset()
        voucher_number = self.request.query_params.get('voucher_number')
        if voucher_number:
            queryset = queryset.filter(voucher_number=voucher_number)
            return queryset
        queryset = queryset.filter(buyer=self.request.user.profile.tenant)
        return queryset

    def create(self, request, *args, **kwargs):
        """
        Handles Orders with the format
        {
            "order_items":
                [
                    {"listing" : "1", "quantity": "2"},
                    {"listing" : "2", "quantity": "3"}
                ],
            "returned_cylinder_serials": [{"serial_no": "1"}, {"serial_no": "2"}]
        }
        """

        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def perform_create(self, serializer):
        serializer.save(order_status=ORDER_STATUS_NEW)

    @action(methods=['get'], detail=False, url_path='seller', url_name='seller-order')
    def seller(self, request, *args, **kwargs):
        """
        Seller views orders to fulfill
        """
        queryset = self.filter_queryset(
            super(OrdersViewSet, self).get_queryset())
        order_status = self.request.query_params.get('status')
        queryset = queryset.filter(seller=request.user.profile.tenant)
        if order_status == '0':
            queryset = queryset.filter(Q(order_status=ORDER_STATUS_COMPLETED) |
                                       Q(order_status=ORDER_STATUS_REJECTED) |
                                       Q(order_status=ORDER_STATUS_DISPATCHED) |
                                       Q(order_status=ORDER_STATUS_APPROVED) |
                                       Q(order_status=ORDER_STATUS_CANCELLED))
        else:
            queryset = queryset.filter(order_status=ORDER_STATUS_NEW)
        page = self.paginate_queryset(queryset)

        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)

        return Response(serializer.data)

    def remove_duplicate_serials(self, serials_dict):
        cylinder_serials_list = []
        for serial in serials_dict:
            if not serial['serial_no'] in cylinder_serials_list:
                cylinder_serials_list.append(serial['serial_no'])
            else:  # duplicate
                del serials_dict[len(cylinder_serials_list)-1]
                continue
        return serials_dict

    @action(methods=['post'], detail=True, url_path='fulfill', url_name='fulfill-order')
    def fulfill(self, request, pk=None):
        """

        For seller(retailer/distributor) fulfilling the order

        orders/order_number/fulfill/

        {"assigned_cylinder_serials" : [
            {"serial_no": "1"}, {"serial_no": "2"}]}

        """

        serializer = OrderFulfillmentSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        queryset = self.filter_queryset(
            super(OrdersViewSet, self).get_queryset())
        order = queryset.get(pk=pk)

        buyer = order.buyer
        seller = order.seller
        if buyer.tenant_type == CONSUMER:
            consumer_user = buyer.consumer_set.first().user
            self.buyer_phone = consumer_user.phone

            if seller.tenant_type == RETAILER:
                retailer = seller.retailer
                self.agent_email = retailer.jp_agent_email

            elif seller.tenant_type == DISTRIBUTOR:
                distributor = seller.distributor
                self.agent_email = distributor.jp_agent_email

            if order.mode_of_payment == MODE_OF_PAYMENT.Cash:
                wallet = Wallet(password=serializer.data['password'])
                result = wallet.makeAgencyPayment(
                    agent_code=request.tenant_agent_code,
                    agent_key=request.tenant_agent_key,
                    order=order,
                    customer_phone=self.buyer_phone
                )
                if result['code'] != 0:
                    raise serializers.ValidationError({
                        'error': result['data']
                    })

        elif buyer.tenant_type == RETAILER:
            retailer = buyer.retailer
            self.buyer_phone = retailer.contact_phone

        elif buyer.tenant_type == DISTRIBUTOR:
            distributor = buyer.distributor
            self.buyer_phone = distributor.contact_phone
        serializer.data['assigned_cylinder_serials'] = self.remove_duplicate_serials(
            serializer.data['assigned_cylinder_serials'])
        if order.total_purchased_cylinders > len(serializer.data['assigned_cylinder_serials']):
            raise serializers.ValidationError({
                'error': 'Please enter all the assigned cylinder serials'
            })

        order_items = {}
        cylinder_items = {}
        for item in order.order_items.all():
            if item.listing.product.id not in order_items:
                order_items[item.listing.product.id] = item.quantity
            else:
                order_items[item.listing.product.id] += item.quantity
        for serial in serializer.data['assigned_cylinder_serials']:
            _product_id = ProductItem.objects.get(
                serial_no=serial['serial_no']).product.id
            if _product_id not in cylinder_items:
                cylinder_items[_product_id] = 1
            else:
                cylinder_items[_product_id] += 1
            product_item_user = ProductItemUser.objects.filter(
                product_item__serial_no=serial['serial_no']).first()
            if product_item_user:  # else it we will be created if it doesn't exist
                if not product_item_user.tenant == seller:
                    raise serializers.ValidationError({
                        'error': f'returned serial number {serial["serial_no"]} is not owned by {seller}'
                    })

        for product_id, quantity in order_items.items():
            if product_id in cylinder_items:
                if not cylinder_items[product_id] == quantity:
                    _product = Product.objects.get(id=product_id)
                    raise serializers.ValidationError({
                        'error': f'Issued cylinder quantity mismatch. expected ({_product}={quantity}) got ({_product}={cylinder_items[product_id]}'
                    })
            else:
                raise serializers.ValidationError({
                    'error': f'Issued cylinder serial is not in order items'
                })
        if 'returned_cylinder_serials' in serializer.data:
            serializer.data['returned_cylinder_serials'] = self.remove_duplicate_serials(
                serializer.data['returned_cylinder_serials'])
            for serial in serializer.data['returned_cylinder_serials']:
                _product = ProductItem.objects.get(
                    serial_no=serial['serial_no']).product
                if _product.id not in order_items:
                    raise serializers.ValidationError({
                        'error': f'Returned cylinder serial: {serial["serial_no"]} for {_product} is not in this order'
                    })
                product_item_user = ProductItemUser.objects.filter(
                    product_item__serial_no=serial['serial_no']).first()
                if product_item_user:  # else it we will be created if it doesn't exist
                    if not product_item_user.tenant == buyer:
                        raise serializers.ValidationError({
                            'error': f'returned serial number {serial["serial_no"]} is not owned by {buyer}'
                        })

        for serial in serializer.data['assigned_cylinder_serials']:
            if not ProductItemUser.objects.filter(product_item__serial_no=serial['serial_no']).exists():
                ProductItemUser.objects.create(product_item=ProductItem.objects.get(serial_no=serial['serial_no']),
                                               tenant=buyer)
            else:
                product_item_user = ProductItemUser.objects.get(
                    product_item__serial_no=serial['serial_no'])
                product_item_user.tenant = buyer
                product_item_user.save()

        for serial in serializer.data['assigned_cylinder_serials']:
            product_item = ProductItem.objects.get(
                serial_no=serial['serial_no'])
            if product_item.product.gas_yetu:
                beneficiary = Beneficiary.objects.get(tenant=order.buyer)
                beneficiary.has_benefited = True
                beneficiary.save()

        cylinder_serials = ",".join([list(serial.items())[0][1] for
                                     serial in serializer.data['assigned_cylinder_serials']])
        
        if 'returned_cylinder_serials' in serializer.data:
            for serial in serializer.data['returned_cylinder_serials']:
                if not ProductItemUser.objects.filter(product_item__serial_no=serial['serial_no']).exists():
                    ProductItemUser.objects.create(product_item=ProductItem.objects.get(serial_no=serial['serial_no']),
                                                   tenant=seller)
                else:
                    product_item_user = ProductItemUser.objects.get(
                        product_item__serial_no=serial['serial_no'])
                    product_item_user.tenant = seller
                    product_item_user.save()

            order.complete_order_returned_cylinder_serials(
                *serializer.data['returned_cylinder_serials'])
        #order.compute_profit(*serializer.data['assigned_cylinder_serials'])
        order.complete_issued_cylinder(
            *serializer.data['assigned_cylinder_serials'])
        order.compute_profit(*serializer.data['assigned_cylinder_serials'])

        services = list(order.order_items.all().values_list(
            'listing__service', flat=True))

        if 'purchase' in services and 'refill' in services:
            message = """Your order of cylinder refill and new cylinder purchase with serial numbers %s has been
                 fulfilled successfully. Thank you for Buying Gas Yetu."""

        elif 'purchase' in services:
            message = """Your order of New cylinder purchase with serial numbers %s has been fulfilled successfully.
                 Thank you for Buying Gas Yetu."""

        elif 'refill' in services:
            message = """Your order of cylinder refill with serial numbers %s has been fulfilled successfully.
                 Thank you for Buying Gas Yetu."""

        send_sms(phone_number=self.buyer_phone,
                 sms_body=message % cylinder_serials)
        serializer = self.get_serializer(order)
        return Response(serializer.data)

        for serial in serializer.data['assigned_cylinder_serials']:
            product_item = ProductItem.objects.get(
                serial_no=serial['serial_no'])
            if product_item.product.gas_yetu:
                beneficiary = Beneficiary.objects.get(tenant=order.buyer)
                beneficiary.has_benefited = True
                beneficiary.save()

    @action(methods=['post'], detail=True, url_path='received', url_name='received')
    def received(self, request, pk=None):
        """
        For tenants marking the cylinders they received in bulk from their retailer,
        entered at a later time than when the order occurred
        """

        serializer = OrderReturnedSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        order = self.get_object()
        order.complete_order_returned_cylinder_serials(
            *serializer.data['returned_cylinder_serials'])
        order.order_status = ORDER_STATUS_COMPLETED
        order.save()

        return Response(serializer.data)


class OrderItemViewSet(HasRoleMixin, viewsets.ModelViewSet):
    authentication_classes = (JSONWebTokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = OrderItemSerializer
    allowed_roles = ['distributor_admin',
                     'retailer_admin', 'beneficiary', 'consumer']

    def get_queryset(self):
        return OrderItem.objects.filter(order_id=self.kwargs['order_pk'])

    def perform_update(self, serializer):
        order_status = Order.objects.get(
            order_number=self.kwargs['order_pk']).order_status
        if order_status == ORDER_STATUS_DRAFT:
            serializer.save()
        else:
            raise OrderEditForbidden


class DeliveryNoteViewSet(HasRoleMixin, viewsets.ModelViewSet):
    authentication_classes = (JSONWebTokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = DeliveryNoteSerializer
    allowed_roles = ['distributor_admin',
                     'retailer_admin', 'beneficiary', 'consumer']

    def get_queryset(self):
        return DeliveryNote.objects.filter(order__order_id=self.kwargs['order_pk'])

    def perform_create(self, serializer):
        order = Order.objects.get(
            order_number=self.kwargs['order_pk'])
        if not order.order_status == ORDER_STATUS_COMPLETED:
            raise serializers.ValidationError({"code": "422",
                                               "error": "Incomplete Order"})
        if DeliveryNote.objects.filter(order=order).exists():
            raise serializers.ValidationError({"code": "409",
                                               "error": "Delivery note for this order has already been created"})
        serializer.save(
            order=order, delivery_note_status=DELIVERY_NOTE_STATUS.New)


class RetailerDistributorOrdersViewset(HasRoleMixin, ReadOnlyViewSet):
    authentication_classes = (JSONWebTokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    queryset = Order.objects.all()
    serializer_class = OrderSerializer
    allowed_roles = ['distributor_admin',
                     'retailer_admin', 'beneficiary', 'consumer']

    def get_queryset(self):
        queryset = super(RetailerDistributorOrdersViewset, self).get_queryset()

        parent_tenant = self.request.user.profile.tenant

        if 'distributors_pk' in self.kwargs:
            distributor = Distributor.objects.filter(pk=self.kwargs['distributors_pk'],
                                                     enabled=True,
                                                     gas_importer=parent_tenant).first()
            if distributor:
                return queryset.filter(seller=distributor.distributor_tenant)
            else:
                return queryset.none()
        elif 'retailers_pk' in self.kwargs:
            retailer = Retailer.objects.filter(enabled=True,
                                               pk=self.kwargs['retailers_pk'], distributor=parent_tenant).first()
            if retailer:
                return queryset.filter(seller=retailer.retailer_tenant)
            else:
                return queryset.none()
        else:
            return queryset.none()


class SerialNumberViewSet(HasRoleMixin, ReadOnlyViewSet):
    authentication_classes = (JSONWebTokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = SerialNumberSerializer
    allowed_roles = ['distributor_admin',
                     'retailer_admin', 'beneficiary', 'consumer']

    def get_queryset(self):
        tenant = self.request.user.profile.tenant
        queryset = ProductMovement.objects.filter(destination=tenant)
        product_id = self.request.query_params.get('productid', None)
        if product_id is not None:
            queryset = queryset.filter(product_item__product__id=product_id)
        return queryset
