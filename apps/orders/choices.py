from django.utils.translation import ugettext_lazy as _

from model_utils import Choices

ORDER_STATUS_DRAFT = 1
ORDER_STATUS_NEW = 2
ORDER_STATUS_APPROVED = 3
ORDER_STATUS_REJECTED = 4
ORDER_STATUS_CANCELLED = 5
ORDER_STATUS_DISPATCHED = 6
ORDER_STATUS_COMPLETED = 7

ORDER_STATUS = Choices(
    (ORDER_STATUS_DRAFT, 'Draft', _('Draft')),
    (ORDER_STATUS_NEW, 'New', _('New')),
    (ORDER_STATUS_APPROVED, 'Approved', _('Approved')),
    (ORDER_STATUS_REJECTED, 'Rejected', _('Rejected')),
    (ORDER_STATUS_CANCELLED, 'Cancelled', _('Cancelled')),
    (ORDER_STATUS_DISPATCHED, 'Dispatched', _('Dispatched')),
    (ORDER_STATUS_COMPLETED, 'Completed', _('Completed'))
)
DELIVERY_NOTE_STATUS = Choices(
    (1, 'New', _('New')),
    (2, 'Cancelled', _('Cancelled')),
    (3, 'Delivered', _('Delivered')),
)

MODE_OF_PAYMENT = Choices(
    ('Cash', _('Cash')),
    ('Wallet', _('Wallet')),
)

PRODUCT_STATUS = Choices(
    ('Empty', _('Empty')),
    ('Full', _('Full')),
)


SALE_STATUS = Choices(
    ('Sold', _('Sold')),
    ('Not_Sold', _('Not_Sold')),
)


WITH_STATUS = Choices(
    ('GasImporter', _('GasImporter')),
    ('Retailer', _('Retailer')),
    ('Consumer', _('Consumer')),
)