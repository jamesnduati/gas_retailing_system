import os
import requests
import string
import random
from apps.sales.models import Sale


def get_voucher_number(size=6, chars=string.ascii_uppercase + string.digits):
    voucher_number = ''.join(random.choice(chars) for _ in range(size))
    order = Sale.objects.filter(voucher_number=voucher_number).exists()
    if order:
        return get_voucher_number()
    return voucher_number