# Generated by Django 2.0.8 on 2020-05-29 17:39

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sales', '0008_auto_20200529_1735'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sale',
            name='voucher_number',
            field=models.CharField(blank=True, max_length=30, null=True, unique=True),
        ),
    ]
