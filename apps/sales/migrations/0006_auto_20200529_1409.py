# Generated by Django 2.0.8 on 2020-05-29 14:09

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0002_remove_product_gas_yetu'),
        ('sales', '0005_sale_sale_status'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='saleitem',
            name='listing',
        ),
        migrations.AddField(
            model_name='saleitem',
            name='product',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='products.Product', verbose_name='Product'),
        ),
        migrations.AlterField(
            model_name='saleitem',
            name='sale',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='sale_item', to='sales.Sale'),
        ),
    ]
