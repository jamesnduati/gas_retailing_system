# Generated by Django 2.0.8 on 2020-05-26 18:24

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0002_remove_product_gas_yetu'),
        ('sales', '0002_sale_listing'),
    ]

    operations = [
        migrations.CreateModel(
            name='SaleItem',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_on', models.DateTimeField(auto_now_add=True)),
                ('modified_on', models.DateTimeField(auto_now=True)),
                ('quantity', models.PositiveIntegerField(help_text='Number of items bought')),
                ('incoming_serial', models.CharField(blank=True, max_length=20, null=True)),
                ('outgoing_serial', models.CharField(blank=True, max_length=20, null=True)),
                ('listing', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='products.Listing', verbose_name='Product')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.RemoveField(
            model_name='sale',
            name='incoming_serial',
        ),
        migrations.RemoveField(
            model_name='sale',
            name='listing',
        ),
        migrations.RemoveField(
            model_name='sale',
            name='outgoing_serial',
        ),
        migrations.AddField(
            model_name='saleitem',
            name='sale',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='sale_items', to='sales.Sale'),
        ),
    ]
