import collections
import simplejson as json
import decimal
import os

from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db import IntegrityError, transaction
from django.db.models import Sum
from django.forms.formsets import formset_factory
from django.forms.models import inlineformset_factory
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404, redirect, render
from django.urls import reverse
from django.utils import timezone
from django.views.generic import DetailView, ListView
from rolepermissions.decorators import has_permission_decorator
from rolepermissions.mixins import HasPermissionsMixin, HasRoleMixin

from apps.accounts.utils import send_sms
from apps.consumers.models import Consumer
from apps.distribution.models import Retailer
from apps.inventory.models import CurrentStock, StockMovement
from apps.products.models import Listing, Brand, Cylinder
from apps.tenants.models import Tenant
from apps.orders.models import (
    Product, ProductItem, ProductItemUser, ProductMovement)

from apps.orders.choices import PRODUCT_STATUS, SALE_STATUS as PRODUCT_SALE_STATUS, WITH_STATUS
from apps.inventory.choices import MOVEMENT_TYPE, DIRECTION
from ..models import Sale, SaleItem
from ..choices import SALE_STATUS
from ..mixins import SaleRecipientDetailsMixin
from ..utils import get_voucher_number
from ..forms import SaleForm


class SalesView(HasRoleMixin, LoginRequiredMixin, ListView):
    model = Sale
    template_name = 'sales.html'
    context_object_name = 'sales'
    allowed_roles = ['retailer_admin']

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['display'] = 'My Sales'
        context['sales'] = Sale.objects.filter(seller=self.request.tenant).all()
        context['tenant'] = self.request.tenant

        return context


class NewSaleView(HasRoleMixin, LoginRequiredMixin, ListView):
    model = Sale
    form = SaleForm
    template_name = 'make_sale.html'
    context_object_name = 'sales'
    allowed_roles = ['retailer_admin']

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['display'] = 'New Sale'
        context['tenant'] = self.request.tenant
        context['form'] = self.form
        context['listings'] = Listing.objects.filter(tenant=self.request.tenant).all()
        return context

    def render_to_response(self, context, **response_kwargs):
        if len(context['listings']) is 0:
            messages.add_message(
                self.request, messages.ERROR, "You have no listings. Add them")
            return redirect("listing-create")

        if len(CurrentStock.objects.filter(tenant=self.request.tenant).all()) is 0:
            messages.add_message(
                self.request, messages.ERROR, "You have no stock. Place an order")
            return redirect("create_order")

        return super().render_to_response(context, **response_kwargs)

    def post(self, request, *args, **kwargs):
        data = request.POST
        if ProductItem.objects.filter(serial_no=data['outgoing_serial_no']).first() is None:
            messages.add_message(
                self.request, messages.ERROR, "This serial is not valid")
            return redirect("make_sale")

        if Listing.objects.filter(tenant=request.tenant, service=data['sale_type']).first() is None:
            messages.add_message(
                self.request, messages.ERROR, f'You have no listing for {data["sale_type"]}. Add it')
            return redirect("make_sale")

        phone_number = data['customer_phone_number']

        consumer = Consumer.objects.filter(phone=phone_number).first()
        if consumer is None:
            consumer = Consumer(
                phone=phone_number,
                name=request.POST['customer_name']
            )
            consumer.save()

        retailer = Retailer.objects.filter(retailer_tenant=request.tenant).first()
        product_movement = ProductMovement.objects.filter(product_item__serial_no=data['outgoing_serial_no'],\
                                                          destination=request.tenant).first()
        brand = product_movement.product_item.product.brand
        cylinder = product_movement.product_item.product.cylinder
        product = Product.objects.filter(brand=brand, cylinder=cylinder).first()
        listing = Listing.objects.filter(product=product, tenant=request.tenant, service=data['sale_type']).first()
        sale = Sale(
            consumer=consumer,
            seller=request.tenant,
            voucher_number=get_voucher_number(),
            seller_code=retailer.agent_number,
            sale_type=data['sale_type'],
        )
        sale.save()

        if data['sale_type'] == 'refill':
            sale_item = SaleItem(
                sale=sale,
                product=product,
                listing=listing,
                incoming_serial=data['incoming_serial_no'],
                outgoing_serial=data['outgoing_serial_no'],
            )

            # save the returned cylinder
            product_item = ProductItem(
                product=product,
                serial_no=data['incoming_serial_no'],
                sale_status=PRODUCT_SALE_STATUS.Not_Sold,
                with_status=WITH_STATUS.Retailer,
                product_status=PRODUCT_STATUS.Empty

            )
            product_item.save()
        else:
            sale_item = SaleItem(
                sale=sale,
                product=product,
                listing=listing,
                outgoing_serial=data['outgoing_serial_no'],
            )

        sale_item.save()
        return redirect(reverse('sale_preview', kwargs={'pk': sale.pk}))


class SalePreviewView(HasRoleMixin, LoginRequiredMixin, SaleRecipientDetailsMixin,  DetailView):
    model = Sale
    template_name = "sale_confirm.html"
    allowed_roles = ['retailer_admin']

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['sale_item'] = SaleItem.objects.filter(sale=kwargs['object']).first()
        return context

    def post(self, request, *args, **kwargs):
        tenant = request.tenant
        pk = request.POST['pk']
        sale = Sale.objects.filter(pk=pk).first()
        customer = Consumer.objects.filter(pk=sale.consumer.pk).first()
        sale.sale_status = SALE_STATUS.Completed
        sale.save()

        item_count = SaleItem.objects.filter(sale=sale).count()

        stock_movement = StockMovement(tenant=request.tenant, product=sale.get_product,
                                       movement_type=MOVEMENT_TYPE.Sales, direction=DIRECTION.Out,
                                       movement_date=sale.sale_date, no_of=item_count,
                                       customer=customer)
        stock_movement.save()

        sale_item = SaleItem.objects.filter(sale=sale).first()
        product_item = ProductItem.objects.filter(serial_no=sale.get_outgoing_serial).first()
        product_item.sale_status = PRODUCT_SALE_STATUS.Sold
        product_item.with_status = WITH_STATUS.Consumer
        product_item.save()

        brand = product_item.product.brand
        cylinder = product_item.product.cylinder

        product = f'{brand}-{cylinder}'
        serial_no = sale_item.outgoing_serial
        price = sale_item.listing.list_price

        if sale.sale_type == 'refill':
            message = f'{sale.voucher_number}. Refill was successful for {product} with {serial_no} for {sale_item.incoming_serial} @ Ksh {price} from {tenant}.\
             Thank you.'
        else:
            message = f'{sale.voucher_number}. Purchase was successful for {product} with {serial_no} @ Ksh {price} from {tenant}.\
             Thank you.'

        send_sms(phone_number=sale.consumer.phone, sms_body=message)
        messages.add_message(
            self.request, messages.SUCCESS, "Sale transaction completed successfully!")

        return redirect('my_sales')


def check_serial_no(request):
    serial_no = request.GET['outgoing_serial_no']
    product_item = ProductItem.objects.filter(serial_no=serial_no).first()
    product_movement = ProductMovement.objects.filter(destination=request.tenant, product_item=product_item).first()
    if product_movement is not None:
        response = {
            'status': 200,
            'serial_no': product_item.serial_no,
            'message': 'Serial has been found'
        }
    else:
        response = {
            'status': 400,
            'message': 'Serial Number does not exist'
        }
    return HttpResponse(json.dumps(response))
