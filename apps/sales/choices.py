from django.utils.translation import ugettext_lazy as _
from model_utils import Choices

PURCHASE = 'purchase'
REFILL = 'refill'

PENDING = 'pending'
COMPLETED = 'completed'


SALE_TYPE = Choices(
    (PURCHASE, 'Purchase', _('Purchase')),
    (REFILL, 'Refill', _('Refill')),
)


SALE_STATUS = Choices(
    (PENDING, 'Pending', _('Pending')),
    (COMPLETED, 'Completed', _('Completed')),
)
