
class SaleRecipientDetailsMixin(object):

    def get_context_data(self, **kwargs):
        order = self.object
        seller = order.seller

        retailer = seller.retailer
        context = super().get_context_data(**kwargs)
        context['seller_names'] = retailer.trading_name
        context['seller_phone'] = retailer.contact_phone
        context['seller_email'] = retailer.contact_email
        context['seller_county'] = retailer.sub_region.title
        context['seller_sub_county'] = retailer.sub_region.title
        return context
