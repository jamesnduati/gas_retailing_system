from django import forms
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _

from apps.orders.models import ProductItem
from .choices import SALE_TYPE


class SaleForm(forms.Form):
    customer_name = forms.CharField(
        max_length=100, label="Enter Customer Name", required=True)
    customer_phone_number = forms.CharField(
        max_length=32, label="Enter Customer Phone Number", required=True)
    sale_type = forms.ChoiceField(choices=SALE_TYPE, required=True)
    outgoing_serial_no = forms.CharField(
        max_length=32, label="Enter Issued Serial Number", required=True)
    incoming_serial_no = forms.CharField(
        max_length=32, label="Enter Returned Serial Number", required=False)

    def clean_outgoing_serial_no(self):
        outgoing_serial_no = self.cleaned_data['outgoing_serial_no']
        if outgoing_serial_no is None:
            raise ValidationError(_('This serial is not valid'))
        elif outgoing_serial_no and ProductItem.objects.get(serial_no=outgoing_serial_no) is None:
            raise ValidationError(_("This serial does not exist"))
        return outgoing_serial_no
