from django.conf.urls import url

from ..views import default

urlpatterns = [
    url(r'^new/$', default.NewSaleView.as_view(), name='make_sale'),
    url(r'^sales/$', default.SalesView.as_view(), name='my_sales'),
    url(r'^check_serial_no/$', default.check_serial_no, name='check_serial_no'),
    url(r'^(?P<pk>[0-9a-f-]+)/preview/$', default.SalePreviewView.as_view(), name="sale_preview"),
]
