from django.db import models
from django.db.models import Sum
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _

from apps.consumers.models import Consumer
from apps.core.models import BaseModel
from apps.core.utils import get_uuid
from apps.orders.models import ProductItem
from apps.products.models import Product, Listing
from apps.tenants.models import Tenant
from .choices import SALE_TYPE, SALE_STATUS


class Sale(BaseModel):
    """
    Sale made
    consumer
    sale type ( Refill or Purchase )
    incoming_serial
    outgoing serial
    cylinder size
    """
    consumer = models.ForeignKey(
        Consumer, on_delete=models.CASCADE, related_name='consumer', null=True, blank=True)
    seller = models.ForeignKey(
        Tenant, on_delete=models.CASCADE, related_name='seller', null=True, blank=True)
    voucher_number = models.CharField(max_length=30, unique=True, null=True, blank=True)
    sale_number = models.UUIDField(
        primary_key=True, default=get_uuid, editable=False)
    sale_date = models.DateTimeField(auto_now_add=True)
    seller_code = models.PositiveIntegerField(null=True,
                                              help_text='Agent number of Retailer',
                                              verbose_name=_(
                                                  'Agent\'s Number'),
                                              blank=True)
    sale_type = models.CharField(max_length=30,
                                       choices=SALE_TYPE, default=SALE_TYPE.Refill,
                                       verbose_name=_('Type of Sale'))
    sale_status = models.CharField(max_length=30,
                                        choices=SALE_STATUS, default=SALE_STATUS.Pending,
                                        verbose_name=_('Status of Sale'))

    @property
    def get_product(self):
        sale_item = SaleItem.objects.filter(sale=self.pk).first()
        return sale_item.product

    @property
    def get_cost(self):
        sale_item = SaleItem.objects.filter(sale=self.pk).first()
        return sale_item.listing.list_price

    @property
    def get_outgoing_serial(self):
        sale_item = SaleItem.objects.filter(sale=self.pk).first()
        return sale_item.outgoing_serial

    @property
    def get_incoming_serial(self):
        sale_item = SaleItem.objects.filter(sale=self.pk).first()
        return sale_item.incoming_serial

    @property
    def get_product_item_returned(self):
        product_item = ProductItem.objects.filter(serial_no=self.get_incoming_serial).first()
        return product_item


class SaleItem(BaseModel):
    """
    Sale Item made
    """
    sale = models.ForeignKey(
        Sale, on_delete=models.CASCADE, related_name='sale_item', null=True, blank=True)
    product = models.ForeignKey(
        Product, on_delete=models.CASCADE, verbose_name=_('Product'), null=True, blank=True)
    listing = models.ForeignKey(
        Listing, on_delete=models.CASCADE, verbose_name=_('Listing'), null=True, blank=True)
    incoming_serial = models.CharField(max_length=20, null=True, blank=True)
    outgoing_serial = models.CharField(max_length=20, null=True, blank=True)

    @property
    def cost(self):
        return self.listing.list_price * self.quantity

    def __str__(self):
        return f'{self.listing} - {self.listing.product}'
