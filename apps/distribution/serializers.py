import json
import random

from django.contrib.gis.geos import GEOSGeometry, Point
from django.core.exceptions import ObjectDoesNotExist
from django.core.mail import send_mail
from django.db import transaction
from phonenumbers import NumberParseException
from phonenumbers import parse as parse_phone
from rest_framework import serializers
from rolepermissions.roles import assign_role

from apps.accounts import utils as accountsutils
from apps.accounts.choices import EMPLOYEE
from apps.accounts.models import User
from apps.accounts.utils import Wallet
from apps.distribution import utils as retailerutils
from apps.employees.choices import EMPLOYMENT_STATUS, IDENTITY_TYPE
from apps.employees.models import Employee
from apps.tenants.choices import (DISTRIBUTOR, GAS_IMPORTER, RETAILER,
                                  TENANT_TYPE)
from apps.tenants.models import Tenant

from .models import Retailer


class RetailerSerializer(serializers.ModelSerializer):
    """Retailer Model Serializer. """
    trading_name = serializers.CharField()

    class Meta:
        model = Retailer
        fields = ('id', 'trading_name', 'sub_region', 'start_date', 'kra_pin_number',
                  'physical_address', 'identity_number', 'contact_phone',
                  'contact_email', 'lat', 'lon', 'position', 'gas_importer',
                  'agent_number')
        read_only_fields = ('gas_importer',)

    @transaction.atomic
    def create(self, validated_data):
        user = self.context['request'].user
        if user.profile_type == EMPLOYEE:
            employee = user.employee
            if employee.tenant.tenant_type == GAS_IMPORTER:
                try:
                    self.gas_importer = GAS_IMPORTER.objects.get(
                        distributor_tenant=employee.tenant)
                except GAS_IMPORTER.DoesNotExist:
                    raise serializers.ValidationError(
                        "Distributor does not exist")
        trading_name = validated_data.get('trading_name')
        tenant = Tenant.create_new(
            TENANT_TYPE.Retailer, trading_name)

        generated_password = retailerutils.get_random_digits(6)

        retaileruser = User.objects.create_user(
            username=validated_data['contact_phone'],
            phone=validated_data['contact_phone'],
            email=validated_data['contact_email'],
            password=generated_password,
            identity_number=validated_data['identity_number'],
            profile_type=EMPLOYEE,
            is_active=True,
            email_verified=True,
            phone_verified=True
        )

        assign_role(retaileruser, 'retailer')

        retailer = Retailer.objects.create(
            retailer_tenant=tenant,
            gas_importer=self.gas_importer,
            trading_name=validated_data['trading_name'],
            sub_region=validated_data['sub_region'],
            start_date=validated_data.get('start_date', None),
            identity_number=validated_data['identity_number'],
            kra_pin_number=validated_data['kra_pin_number'],
            physical_address=validated_data.get('physical_address', None),
            contact_phone=validated_data['contact_phone'],
            contact_email=validated_data['contact_email'],
        )
        retailer.save()

        user_id = retaileruser.id
        user = User.objects.get(id=user_id)

        Employee.objects.create(
            tenant=tenant,
            user=user,
            identity_number=validated_data['identity_number'],
            identity_type=IDENTITY_TYPE.NationalID,
            employment_status=EMPLOYMENT_STATUS.Active
        )

        sms_body = "Welcome to JamboPay Gas Retailing System. Here are your login credentials. Username :" + \
                   str(validated_data['contact_phone']) + " Password : " + \
                   str(generated_password) + " to log into the system."
        accountsutils.send_sms(validated_data['contact_phone'], sms_body)
        return retailer

    def validate_trading_name(self, trading_name):
        """
        Check whether the trading name already exists
        """
        if Retailer.objects.filter(trading_name=trading_name).exists():
            raise serializers.ValidationError(
                "Retailer with this trading name number already exists")
        return trading_name

    def validate_contact_email(self, contact_email):
        """
        Check whether the email already exists
        and not similar to jp_agent_email
        """
        if User.objects.filter(email=contact_email).exists():
            raise serializers.ValidationError(
                "User with this Email number already exists.")
        if contact_email == self.initial_data.get('jp_agent_email'):
            raise serializers.ValidationError(
                "contact_email should be different from jp_agent_email")
        return contact_email

    def validate_kra_pin_number(self, kra_pin_number):
        """
        Check whether the pin_number already exists
        """
        if Retailer.objects.filter(kra_pin_number=kra_pin_number).exists():
            raise serializers.ValidationError(
                "Retailer with this pin number already exists")
        return kra_pin_number

    def validate_identity_number(self, identity_number):
        """
        Check whether the identity_number already exists and if it is a number
        """
        if Retailer.objects.filter(identity_number=identity_number).exists():
            raise serializers.ValidationError(
                "User with this id_number already exists.")
        return identity_number

    def validate_contact_phone(self, contact_phone):
        """
        Check whether the phone_number already exists
        """
        if User.objects.filter(phone=contact_phone).exists():
            raise serializers.ValidationError(
                "User with this Phone number already exists.")
            try:
                parse_phone(contact_phone)
            except NumberParseException:
                raise serializers.ValidationError(
                    "Please enter a valid country code.")
        return contact_phone
