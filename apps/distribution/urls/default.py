from django.conf.urls import url

from apps.distribution.views.default import (RetailerDeleteView,
                                             RetailerUpdateView,
                                             RetailersView,
                                             LocateRetailer,
                                             RetailerData,
                                             load_seller,
                                             RetailerRegisterWizard)
from apps.distribution.views.reports import get_retailers, generate_pdf, generate_excel

urlpatterns = [
    url(r'^retailers/$', RetailersView.as_view(), name='retailers'),
    url(r'^retailers/(?P<pk>[0-9]+)/$', RetailerUpdateView.as_view(),
        name='retailer-update'),
    url(r'^retailers/(?P<pk>[0-9]+)/deactivate$',
        RetailerDeleteView.as_view(), name='retailer-deactivate'),
    url(r'^locate/retailers/$', LocateRetailer.as_view(), name="locate-retailer"),
    url(r'^points.retailer/$', RetailerData.as_view(), name="retailer-points"),
    url(r'^ajax/load-seller/$', load_seller, name='ajax_load_seller'),

    url(r'^reports/retailers/$', get_retailers, name='reports-retailers'),
    # generate pdf and excel
    url(r'^reports/generate/pdf/$', generate_pdf, name='distribution_generate_pdf'),
    url(r'^reports/distributors/generate/excel/$', generate_excel, name='distribution_generate_excel'),

]
