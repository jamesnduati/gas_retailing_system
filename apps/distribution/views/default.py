import datetime

from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db import transaction
from django.http import HttpResponseRedirect
from django.shortcuts import redirect, render
from django.urls import reverse_lazy
from django.views.generic import DeleteView, ListView, TemplateView, UpdateView
from djgeojson.views import GeoJSONLayerView
from formtools.wizard.views import SessionWizardView

from .. import forms
from ..mixins import CreateRetailerMerchantMixin
from ..models import Retailer


class RetailerRegisterWizard(CreateRetailerMerchantMixin, SessionWizardView):
    """Wizard View for retailer's account registration.

    Note:
        Step 1: Create a merchant and the retailer.
        Step 2: Create the agent and the user for the retailer.

    """
    @transaction.atomic
    def done(self, form, **kwargs):
        return redirect('retailers')


class RetailersView(LoginRequiredMixin, ListView):
    model = Retailer
    template_name = 'distribution/retailers.html'
    context_object_name = 'retailers'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['display'] = 'Retailers'
        return context

    def get_queryset(self):
        queryset = super().get_queryset()
        # queryset = queryset.filter(enabled=True)
        return queryset


class RetailerDeleteView(LoginRequiredMixin, DeleteView):
    model = Retailer
    template_name = 'distribution/retailer_delete.html'
    success_url = reverse_lazy('retailers')

    def delete(self, request, *args, **kwargs):
        """
        Calls the delete() method on the fetched object and then
        redirects to the success URL.
        """
        self.object = self.get_object()
        tenant = self.object.retailer_tenant
        tenant.enabled = False
        tenant.save()
        self.object.end_date = datetime.date.today().strftime('%Y-%m-%d')
        self.object.enabled = False
        self.object.save()
        return HttpResponseRedirect(self.get_success_url())

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['display'] = 'Confirm Delete'
        return context


class RetailerUpdateView(LoginRequiredMixin, UpdateView):
    model = Retailer
    template_name = 'distribution/retailer_update.html'
    form_class = forms.RetailerUpdateForm
    success_url = reverse_lazy('retailers')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['display'] = 'Update Retailer'
        return context


class RetailerData(LoginRequiredMixin, GeoJSONLayerView):
    model = Retailer
    properties = ('trading_name', 'agent_number')
    precision = 4
    simplify = 0.5

    def get_queryset(self):
        queryset = super().get_queryset()
        return queryset


class LocateRetailer(LoginRequiredMixin, TemplateView):
    model = Retailer
    template_name = 'location/location.html'

    def get_queryset(self):
        queryset = super().get_queryset()
        return queryset

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['display'] = 'Locate Nearest Retailer'
        context['seller_points'] = 'retailer-points'
        return context


@login_required
def load_seller(request):
    agent_number = request.GET.get('agent_number')
    if agent_number:
        """
        This are orders by consumers
        """

        retailer = Retailer.objects.filter(
            agent_number=agent_number, enabled=True).first()
        if retailer:
            seller = retailer
        else:
            seller = None
    else:
        seller = None
    return render(request, 'location/location_seller_details.html', {'seller': seller})
