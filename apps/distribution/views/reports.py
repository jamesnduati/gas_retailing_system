import xlsxwriter
import tempfile
from io import BytesIO
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponseRedirect, HttpResponse
from django.template.loader import render_to_string
from weasyprint import HTML
from django.shortcuts import render, reverse
from django.urls import reverse_lazy
from django.views.generic import (CreateView, DeleteView, DetailView, ListView,
                                  RedirectView, UpdateView, View)

from apps.tenants.models import Tenant
from apps.distribution.models import Retailer
from datetime import date
from config.settings.base import STATIC_ROOT, STATIC_URL, BASE_DIR
static_path = BASE_DIR + STATIC_URL


@login_required
def get_retailers(request):
    if request.method == 'GET':
        retailers = Retailer.objects.filter(enabled=True)
        return render(request, 'reports/retailers.html', {'retailers': retailers})
    else:
        min_value = request.POST['min-date']
        max_value = request.POST['max-date']
        retailers = Retailer.objects.filter(enabled=True)
        report_data = []
        for retailer in retailers:
            data = {}
            if retailer.created_on.strftime('%d-%m-%Y') >= min_value and retailer.created_on.strftime('%d-%m-%Y') <= max_value:
                data['name'] = retailer.retailer_tenant
                data['sub_region'] = retailer.sub_region
                data['active'] = retailer.retailer_tenant.enabled
                data['distributor'] = retailer.distributor
                data['date_joined'] = retailer.created_on
                data['phone'] = retailer.contact_phone
                data['email'] = retailer.contact_email
                report_data.append(data)
        return render(request, 'reports/retailers.html', {'retailers': report_data})


@login_required
def generate_pdf(request):
    min_value = request.GET['min_date']
    max_value = request.GET['max_date']
    d_type = request.GET['type']
    tenant = request.tenant
    report_data = []
    retailers = Retailer.objects.filter(enabled=True)
    for retailer in retailers:
        data = {}
        if retailer.created_on.strftime('%d-%m-%Y') >= min_value and retailer.created_on.strftime('%d-%m-%Y') <= max_value:
            data['name'] = retailer.retailer_tenant
            data['sub_region'] = retailer.sub_region
            data['active'] = retailer.retailer_tenant.enabled
            data['gas_importer'] = retailer.gas_importer
            data['date_joined'] = retailer.created_on
            data['phone'] = retailer.contact_phone
            data['email'] = retailer.contact_email
            report_data.append(data)
        else:
            data['name'] = retailer.retailer_tenant
            data['sub_region'] = retailer.sub_region
            data['active'] = retailer.retailer_tenant.enabled
            data['gas_importer'] = retailer.gas_importer
            data['date_joined'] = retailer.created_on
            data['phone'] = retailer.contact_phone
            data['email'] = retailer.contact_email
            report_data.append(data)
    return pdf(report_data, tenant, d_type)


@login_required
def generate_excel(request):
    min_value = request.GET['min_date']
    max_value = request.GET['max_date']
    d_type = request.GET['type']
    tenant = request.tenant
    report_data = []
    retailers = Retailer.objects.filter(enabled=True)
    for retailer in retailers:
        data = {}
        if retailer.created_on.strftime('%d-%m-%Y') >= min_value and retailer.created_on.strftime('%d-%m-%Y') <= max_value:
            data['name'] = str(retailer.retailer_tenant)
            data['sub_region'] = str(retailer.sub_region)
            data['active'] = str(retailer.retailer_tenant.enabled)
            data['gas_importer'] = str(retailer.gas_importer)
            data['date_joined'] = retailer.created_on.strftime('%d-%m-%Y')
            data['phone'] = retailer.contact_phone
            data['email'] = retailer.contact_email
            report_data.append(data)
        else:
            data['name'] = str(retailer.retailer_tenant)
            data['agent_number'] = retailer.agent_number
            data['sub_region'] = str(retailer.sub_region)
            data['active'] = str(retailer.retailer_tenant.enabled)
            data['gas_importer'] = str(retailer.gas_importer)
            data['date_joined'] = retailer.created_on.strftime('%d-%m-%Y')
            data['phone'] = retailer.contact_phone
            data['email'] = retailer.contact_email
            report_data.append(data)
    return excel(report_data, tenant, d_type)


def pdf(report_data, tenant, d_type):
    # Rendered
    html_string = render_to_string(
        'reports/distribution_reports.html', {
            'reports': report_data,
            'date': date.today(),
            'static': static_path,
            'logo': static_path+'images/logo.png',
            'tenant': tenant,
            'd_type': d_type
        }
    )
    html = HTML(string=html_string).write_pdf()
    # Creating http response
    response = HttpResponse(html, content_type='application/pdf')
    response['Content-Disposition'] = 'filename=distribution_report.pdf'
    return response


def excel(report_data, tenant, d_type):
    output = BytesIO()
    # Create a workbook and add a worksheet.
    workbook = xlsxwriter.Workbook(output, {'in_memory': True})
    worksheet = workbook.add_worksheet()

    # merge format for the cells
    merge_format = workbook.add_format(
                        {
                            'bold': 1,
                            'align': 'center',
                            'valign': 'vcenter'
                        }
                    )

    # merge logo cells
    # Merge 3 cells.
    if d_type == 'distributor':
        worksheet.merge_range('D1:E4','', merge_format)
    
    if d_type == 'retailer':
        worksheet.merge_range('D1:E4', '', merge_format)

    # setting the header with a logo
    image_width = 128.0
    image_height = 40.0

    cell_width = 64.0
    cell_height = 20.0

    x_scale = cell_width/image_width
    y_scale = cell_height/image_height

    logo = static_path+'images/logo.png'
    if d_type == 'distributor':
        worksheet.insert_image('D1', logo,
                            {'x_scale': x_scale, 'y_scale': y_scale}
                            )

    if d_type == 'retailer':
        worksheet.insert_image('D1', logo,
                               {'x_scale': x_scale, 'y_scale': y_scale}
                               )
    # Add a bold format to use to highlight cells.
    bold = workbook.add_format({'bold': True})

    # adding the header under the logo
    if d_type == 'distributor':
        worksheet.merge_range('D5:E5', '', merge_format)
        worksheet.write('D5', 'Distributors Report', bold)

    if d_type == 'retailer':
        worksheet.merge_range('D5:E5', '', merge_format)
        worksheet.write('D5', 'Retailers Report', bold)

    worksheet.merge_range('A1:B4', '', merge_format)
    worksheet.write(
        'A1', 'Printed by: '+str(tenant)+'\nDate: '+str(date.today()))

    if d_type == 'distributor':
        worksheet.merge_range('F1:H4', '', merge_format)
        worksheet.write('F1', 'National Oil Corporation of Kenya \nKAWI house – South C Redcross Road, \nOff Popo Road \nTelephone: +254-20-695 2000')

    if d_type == 'retailer':
            worksheet.merge_range('F1:H4', '', merge_format)
            worksheet.write('F1', 'National Oil Corporation of Kenya \nKAWI house – South C Redcross Road, \nOff Popo Road \nTelephone: +254-20-695 2000')

    # Write some data headers.
    worksheet.write('A6', 'Name', bold)
    worksheet.write('B6', 'Retailer Code ', bold)
    worksheet.write('C6', 'County', bold)
    worksheet.write('D6', 'Status', bold)
    worksheet.write('E6', 'Distributor', bold)
    worksheet.write('F6', 'Date Joined', bold)
    worksheet.write('G6', 'Email', bold)
    worksheet.write('H6', 'Phone', bold)

    # Start from the first cell below the headers.
    row = 6
    col = 0

    # Iterate over the data and write it out row by row.
    rows = list(report_data[0].keys())
    for i, row in enumerate(report_data):
        i += 6
        for j, col in enumerate(rows):
            worksheet.write(i, j, row[col])

    workbook.close()
    output.seek(0)
    # response
    filename = 'consumers-'+str(date.today())+'.xls'
    response = HttpResponse(output.read(), content_type="application/ms-excel")
    response['Content-Disposition'] = "attachment; filename="+filename
    return response
