import json

from django.db import models
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _

from apps.core.models import BaseModel, Location, Region, SubRegion
from apps.core.utils import NullableEmailField
from apps.tenants.models import Tenant
from apps.tenants.choices import TENANT_TYPE


class GasImporter(BaseModel):
    """
    GasImporters registered in the system
    """
    gas_importer_tenant = models.OneToOneField(
        Tenant, on_delete=models.CASCADE, related_name='gas_importer')
    trading_name = models.CharField(
        max_length=50, help_text='Business Name')
    contact_email = NullableEmailField(
        _('contact e-mail address'), blank=True, null=True, default=None, unique=True,
        help_text='Contact Email Address')
    contact_phone = models.CharField(
        _('phone number'), max_length=16, null=True,
        blank=True, default=None, help_text='Contact Phone Number'
    )
    identity_number = models.CharField(max_length=20, blank=False)
    agent_number = models.IntegerField(
        unique=True, null=True, blank=True, help_text='Six digit agent number')

    def __str__(self):
        return '{}'.format(self.gas_importer_tenant)


class RetailerManager(models.Manager):
    def create_retailer_merchant(self, business_name, gas_importer, **kwargs):
        retailer_tenant = Tenant.create_new(
            tenant_type=TENANT_TYPE.Retailer, title=business_name)
        retailer = Retailer.objects.create(
            gas_importer=gas_importer,
            retailer_tenant=retailer_tenant,
            trading_name=business_name,
            kra_pin_number=kwargs['kra_pin_number'],
            contact_email=kwargs['email'],
            contact_phone=kwargs['phone'],
            identity_number=kwargs['identity_number'],
            physical_address=kwargs.get('physical_address'),
            sub_region=kwargs['sub_region'],
            agent_number=kwargs['agent_number']
        )
        return retailer


class Retailer(BaseModel):
    """
    Retailers registered in the system

    """
    gas_importer = models.ForeignKey(
        GasImporter, on_delete=models.CASCADE, related_name='gas_importer_retailer')
    retailer_tenant = models.OneToOneField(
        Tenant, on_delete=models.CASCADE, null=True, related_name='retailer')
    sub_region = models.ForeignKey(SubRegion, on_delete=models.CASCADE)
    trading_name = models.CharField(
        max_length=100, help_text='Name of the Retailer')
    start_date = models.DateField(null=True, blank=True)
    end_date = models.DateField(null=True, blank=True)
    agent_number = models.IntegerField(
        unique=True, null=True, blank=True, help_text='Six digit agent number')
    identity_number = models.CharField(max_length=20, blank=False)
    kra_pin_number = models.CharField(max_length=30)
    physical_address = models.CharField(max_length=50, null=True, blank=True)
    contact_phone = models.CharField(max_length=50)
    contact_email = models.CharField(max_length=50, unique=True)
    enabled = models.BooleanField(default=True)

    objects = RetailerManager()

    def __str__(self):
        return '{}'.format(self.retailer_tenant)

    def get_absolute_url(self):
        """
        Returns the URL of the detail page for that retailer.
        """
        return reverse(
            'retailer-update', kwargs={'pk': self.pk}
        )
