import logging

from django.conf import settings
from django.contrib import messages
from django.core.exceptions import ValidationError
from django.db import transaction
from django.utils.translation import ugettext_lazy as _
from formtools.wizard.forms import ManagementForm
from rolepermissions.roles import assign_role

from apps.accounts.choices import PROFILE_TYPES
from apps.accounts.models import User
from apps.accounts.utils import Wallet, send_sms
from apps.core.messages import first_time_password
from apps.employees.choices import EMPLOYMENT_STATUS, IDENTITY_TYPE
from apps.employees.models import Employee

from .models import Retailer

logger = logging.getLogger('django')

# TODO: Combine these two classes for better code reuse


class CreateRetailerMerchantMixin(object):
    def post(self, *args, **kwargs):
        """
        This method handles POST requests.

        The wizard will render either the current step (if form validation
        wasn't successful), the next step (if the current step was stored
        successful) or the done view (if no more steps are available)
        """
        # Look for a wizard_goto_step element in the posted data which
        # contains a valid step name. If one was found, render the requested
        # form. (This makes stepping back a lot easier).
        wizard_goto_step = self.request.POST.get('wizard_goto_step', None)
        if wizard_goto_step and wizard_goto_step in self.get_form_list():
            return self.render_goto_step(wizard_goto_step)

        # Check if form was refreshed
        management_form = ManagementForm(self.request.POST, prefix=self.prefix)
        if not management_form.is_valid():
            raise ValidationError(
                _('ManagementForm data is missing or has been tampered.'),
                code='missing_management_form',
            )

        form_current_step = management_form.cleaned_data['current_step']
        if (form_current_step != self.steps.current and
                self.storage.current_step is not None):
            # form refreshed, change current step
            self.storage.current_step = form_current_step

        # get the form for the current step
        form = self.get_form(data=self.request.POST,
                             files=self.request.FILES)
        # and try to validate
        if form.is_valid():
            # if the form is valid, store the cleaned data and files.
            self.storage.set_step_data(
                self.steps.current, self.process_step(form))
            self.storage.set_step_files(
                self.steps.current, self.process_step_files(form))
            # check if the current step is the last step

            # no more steps, render done view
            business_name = form.cleaned_data['business_name']
            phone = form.cleaned_data['phone']
            phone = '254' + phone[-9:]
            kra_pin_number = form.cleaned_data['kra_pin_number']
            email = form.cleaned_data['email']
            identity_number = form.cleaned_data['identity_number']
            password = form.cleaned_data['password']
            postal_address = form.cleaned_data.get('postal_address', None)
            physical_address = form.cleaned_data.get(
                'physical_address', None)
            sub_region = form.cleaned_data['sub_region']

            with transaction.atomic():
                try:
                    retailer = Retailer.objects.create_retailer_merchant(
                        business_name=business_name,
                        gas_importer=self.request.tenant.gas_importer,
                        kra_pin_number=kra_pin_number,
                        email=email,
                        phone=phone,
                        identity_number=identity_number,
                        physical_address=physical_address,
                        sub_region=sub_region,
                    )
                    logger.info(f'Retailer created==>{retailer}')
                except Exception as e:
                    if hasattr(e, 'message'):
                        message = e.message
                    else:
                        message = e
                    logger.error(
                        f'Retailer creation failed==>{message}')
                    messages.add_message(
                        self.request, messages.ERROR, message)
                    return self.render(form)
            return self.render_next_step(form)
        return self.render(form)
