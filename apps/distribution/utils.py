import random

from .models import Retailer
from django.core.exceptions import ValidationError
from apps.core.utils import get_random_digits


def get_agent_number():
    agent_number = int(get_random_digits(7))
    if Retailer.objects.filter(enabled=True, agent_number=agent_number).exists():
        raise ValidationError('Agent number already exists.')
    return agent_number
