from django import forms
from django.conf import settings
from django.forms import ModelForm, SelectDateWidget
from django.forms.widgets import Select
from django.utils.translation import ugettext_lazy as _

from apps.accounts import messages
from apps.accounts.models import User
from apps.core.models import Region, SubRegion

from .models import Retailer


class RetailerUpdateForm(ModelForm):

    class Meta:
        model = Retailer
        fields = (
            'trading_name',
            'kra_pin_number',
            'identity_number',
            'contact_phone',
            'contact_email',
            'physical_address',
            'sub_region',
            'start_date',
            'end_date'
        )
        widgets = {
            'end_date': forms.DateInput(format=('%d-%m-%Y'),
                                        attrs={'id': 'endDate',
                                               'placeholder': 'Select a date'}),
            'start_date': forms.DateInput(format=('%d-%m-%Y'),
                                          attrs={'id': 'startDate',
                                                 'placeholder': 'Select a date'})
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        instance = getattr(self, 'instance', None)
        if instance and instance.id:
            self.fields['contact_email'].widget.attrs['readonly'] = True
            self.fields['contact_phone'].widget.attrs['readonly'] = True
