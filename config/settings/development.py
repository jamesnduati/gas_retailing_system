"""
Development specific settings.
"""

from .base import *

# Database
# https://docs.djangoproject.com/en/1.8/ref/settings/#databases
DEBUG = True

DATABASES = {
    'default': {
        'ENGINE': 'django.contrib.gis.db.backends.postgis',
        'NAME': os.getenv('DB_NAME'),
        'USER': os.getenv('DB_USER'),
        'PASSWORD': os.getenv('DB_PASSWORD'),
        'HOST': '127.0.0.1',
        'PORT': os.getenv('PORT'),
    }
}

# EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
EMAIL_BACKEND = "django.core.mail.backends.smtp.EmailBackend"
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_PORT = 587
EMAIL_HOST_USER = 'njugunanduati'
EMAIL_HOST_PASSWORD = 'jwiZZY@_1987'
EMAIL_USE_TLS = True
DEFAULT_FROM_EMAIL = 'no-reply@gasyetu.co.ke'




