"""nock URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls import handler403, handler404, handler500, include, url
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.auth.decorators import login_required
from django.contrib.auth import views as auth_views
from django.views.generic import TemplateView
from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework import permissions

from apps.core.views.default import handler403, handler404, handler500, IndexPage, get_index

# from . import api

# URLs that can be seen by Swagger after one has authenticated
# public_apis = [
#     url(r'^api/v1/', include(api)),
# ]

# Remove docs from production
# schema_view = get_schema_view(
#     openapi.Info(
#         title="GAS YETU API DOCS",
#         default_version='v1',
#         description="GAS YETU APIs",
#         terms_of_service="https://www.google.com/policies/terms/",
#         contact=openapi.Contact(email="group.rnd@jambopay.com"),
#     ),
#     permission_classes=(permissions.AllowAny,),
#     public=False,
#     patterns=public_apis,
# )

# docs = [
#     url(r'^docs/$', login_required(schema_view.with_ui('swagger',
#                                                        cache_timeout=None)), name='schema-swagger-ui'),
# ]


# urlpatterns = public_apis + docs + [
urlpatterns = [
    # url(r'^$', IndexPage.as_view(), name='home'),
    url(r'^$', get_index, name='home'),
    url(r'^account/', include('apps.accounts.urls.default', namespace='account')),
    url(r'^account/', include('allauth.urls')),
    url(r'^distribution/', include('apps.distribution.urls.default')),
    url(r'^consumers/', include('apps.consumers.urls.default')),
    url(r'^core/', include('apps.core.urls')),
    url(r'^inventory/', include('apps.inventory.urls.default')),
    url(r'^products/', include('apps.products.urls.default')),
    url(r'^sales/', include('apps.sales.urls.default')),
    url(r'^orders/', include('apps.orders.urls.default')),
    url(r'^admin/', admin.site.urls),
    url(r'^invitations/', include('invitations.urls', namespace='invitations')),
    url(r'^accounts/login/$', auth_views.LoginView,
        {'template_name': 'admin/login.html'}),
    url(r'^accounts/logout/$', auth_views.LogoutView),
]

handler403 = handler403
handler404 = handler404
handler500 = handler500

admin.site.site_header = settings.ADMIN_SITE_HEADER
admin.site.index_title = settings.ADMIN_SITE_INDEX_TITLE
admin.site.site_title = settings.ADMIN_SITE_SITE_TITLE
