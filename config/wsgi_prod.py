import os

import django
from django.core.wsgi import get_wsgi_application
from twisted.application import internet, service
from twisted.internet import reactor
from twisted.python.log import FileLogObserver, ILogObserver
from twisted.python.logfile import DailyLogFile
from twisted.web import server, wsgi
from whitenoise.django import DjangoWhiteNoise



PORT = 8000

django.setup()
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "config.settings")
djapp = get_wsgi_application()
djapp = DjangoWhiteNoise(djapp)

# Twisted Application Framework setup:
application = service.Application('twisted-django')

root = wsgi.WSGIResource(reactor, reactor.getThreadPool(), djapp)
logfile = DailyLogFile("nockgui.log","/logs")
application.setComponent(ILogObserver, FileLogObserver(logfile).emit)
# Serve it up:
main_site = server.Site(root)
internet.TCPServer(PORT, main_site).setServiceParent(application)
reactor.suggestThreadPoolSize(100)
